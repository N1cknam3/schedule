<?php
	
App::uses('CakeEmail', 'Network/Email');
	
App::uses('AppController', 'Controller');	
	
class MobileController extends AppController {
	
	const ERR_CODE_INPUT_DATA_MISSED = 'ERR_CODE_INPUT_DATA_MISSED';
// 	const ERR_CODE_INPUT_DATA_WRONG = 'ERR_CODE_INPUT_DATA_WRONG';
	
	
	public function beforeRender(){

	}
	
	/*
		Отправка списка факультетов
		
		@return json массив записей факультета в формате:
			{"id" = faculty_id, "name" = "имя_факультета"}
			
	*/
	public function get_list_faculties() {
		$this->autoRender = false;
		$result = $this->getElementsFromModel('all', null, 'Faculty', array('order' => array('id')));
		$result = $this->redunantModelName($result, 'Faculty');
		return Json::json_encode($result);
	}
	
	/*
		Отправка списка направлений подготовки определенного факультета
		
		@input:
			$faculty_id - id факультета, список направлений которого необходимо получить
		
		@return json многомерный массив записей направлений факультета бакалавров (B), магистров(M), специалистов(S) в формате:
				[B = array(..), M = array(..), S = array(..)],
			в котором каждый подмассив состоит из записей формата:
				{"id" = speciality_id, "full_name" = "название_направления (название_профиля_направления)" }
			
	*/
	public function get_list_specialities($faculty_id = null) {
		$this->autoRender = false;
		
		if ($faculty_id !== null) {
		
			$result_bachelors = $this->redunantModelName(
				$this->getElementsFromModel('all', array('faculty_id' => $faculty_id, 'type' => 0), 'Speciality', array('fields' => array('id', 'full_name'), 'order' => array('id'))),
				'Speciality');
				
			$result_masters = $this->redunantModelName(
				$this->getElementsFromModel('all', array('faculty_id' => $faculty_id, 'type' => 1), 'Speciality', array('fields' => array('id', 'full_name'), 'order' => array('id'))),
				'Speciality');
				
			$result_specialists = $this->redunantModelName(
				$this->getElementsFromModel('all', array('faculty_id' => $faculty_id, 'type' => 2), 'Speciality', array('fields' => array('id', 'full_name'), 'order' => array('id'))),
				'Speciality');
				
			return Json::json_encode(array('B' => $result_bachelors, 'M' => $result_masters, 'S' => $result_specialists));
		
		} else {
			return Json::json_encode(array('ERROR' => self::ERR_CODE_INPUT_DATA_MISSED));
		}
	}
	
	/*
		Отправка списка групп определенного направления подготовки
		
		@input:
			$speciality_id - id направления подготовки, список групп которого необходимо получить
			$course - номер курса (от 1 до 6)
		
		@return json массив записей групп направления подготовкив формате:
			{"id" = group_id, "name" = "название_группы" }
		
	*/
	public function get_list_groups($speciality_id = null, $course = null) {
		$this->autoRender = false;
		
		if ($speciality_id !== null && $course !== null) {
			
			$result = $this->getElementsFromModel('all', array('speciality_id' => $speciality_id, 'course' => $course), 'Group', array('fields' => array('id', 'name'), 'order' => array('id')));
			$result = $this->redunantModelName($result, 'Group');
			
			return Json::json_encode($result);
			
		} else {
			return Json::json_encode(array('ERROR' => self::ERR_CODE_INPUT_DATA_MISSED));
		}
	}
	
	/*
		Отправка расписания всех подгрупп данного направления подготовки и курса
		
		@input:
			$speciality_id - id направления подготовки (из БД),
			$course - номер курса (от 1 до 6)
			
		@return json, содержащий следующие варианты:
			* ERROR = ERROR_DESCRIPTION - если возникла ошибка
			* TABLE = null - если нет данных
			* TABLE - многомерный массив расписания следующего формата:
				[day_id][ring_id][group_id | "all"]["up"|"down"]["Subject" = "название_предмета", "Teacher" = "имя_преподавателя"]
			  RINGS - массив "звонков", который используется в этом расписании, следующего формата:
			  	{"id" = ring_id, "start" = "время_начала", "end" = "время_окончания"}
			  	
	*/
	public function get_list_schedule($speciality_id = null, $course = null) {
		$this->autoRender = false;
		
		if ($speciality_id !== null && $course !== null) {
		
			$groups = $this->get_Groups($speciality_id, $course);	//	метод в AppController
			if (count($groups) != 0)
			{
				//	Берем id этих групп в отдельный массив
				$groups_id_array = $this->transform_groupsArray($groups);	//	метод в AppController
				$result = $this->find_schedule($groups_id_array);
				return Json::json_encode($result);
			} else {
				return Json::json_encode(array('TABLE' => null));
			}
			
		} else {
			return Json::json_encode(array('ERROR' => self::ERR_CODE_INPUT_DATA_MISSED));
		}
	}
	
	/*
		Отправка расписания необходимой подгруппы
		
		@input:
			$group_id - id группы (из БД)
			
		@return json, содержащий следующие варианты:
			* ERROR = ERROR_DESCRIPTION - если возникла ошибка
			* TABLE = null - если нет данных
			* TABLE - многомерный массив расписания следующего формата:
				[day_id][ring_id][group_id | "all"]["up"|"down"]["Subject" = "название_предмета", "Teacher" = "имя_преподавателя"]
			  RINGS - массив "звонков", который используется в этом расписании, следующего формата:
			  	{"id" = ring_id, "start" = "время_начала", "end" = "время_окончания"}
			  	
	*/
	public function get_list_schedule_group($group_id = null) {
		$this->autoRender = false;
		
		if ($group_id !== null) {
		
			$groups = array($group_id);
			$result = $this->find_schedule($groups);
			return Json::json_encode($result);
			
		} else {
			return Json::json_encode(array('ERROR' => self::ERR_CODE_INPUT_DATA_MISSED));
		}
	}
	
	//	По заданному массиву id групп находит их расписание
	private function find_schedule($groups_id_array) {
		
		//	Ищем все занятия этих групп
		$lessons = $this->getLessonsIn(implode(',', $groups_id_array));	//	метод в AppController
			
		//	Сразу подставляем нужные данные в занятия
		$lessons = $this->load_SubjectsTeachers($lessons);	//	метод в AppController
		
		//	Сортировка массива занятий и приведение к виду:
		//	[day_id][ring_id][group_id] - занятия
		$table = $this->MakeTableArray($lessons, $groups_id_array);	//	метод в AppController
		$table = $this->compact_table($table);	//	сокращение передаваемых данных о занятиях
		
		//	Достаем массив звонков для дальнейшего вывода таблицы
		//	Достаем массив звонков для дальнейшего вывода таблицы
		$rings = $this->get_compactRingsArray();
	
		/*
		//	Достаем массив дней недели для дальнейшего вывода таблицы
			$this->loadModel('Day');
			$days = $this->Day->find('list');
		*/
			
		$rings = $this->extract_used_rings($table, $rings);	//	сокращение передаваемых данных о звонках
		
		return array('TABLE' => $table, 'RINGS' => $rings/* , 'DAYS' => $days */);
		
	}
	
	//	Убирает лишние данные в таблице расписания, полученного функцией MakeTableArray($lessons, $groups_id_array) для сокращения передаваемых данных
	private function compact_table($data) {
		
		foreach($data as $k_day => &$v_day) {
			//	[day_id]
			foreach($v_day as $k_ring => &$v_ring) {
				//	[ring_id]
				foreach($v_ring as $k_group => &$v_group) {
					//	[group_id/all]
					foreach($v_group as $k_type => &$v_type) {
						//	[up/down]
						
						//	Убираем пустые up/down
						if (empty($v_type)) {
							unset($v_group[$k_type]);
						} else {
							//	Сокращаем уровень массива Lesson
							$v_group[$k_type] = $v_group[$k_type]['Lesson'];
							
							//	Убираем ненужные поля информации занятий и упрощаем данные о названии и учителе
							foreach($v_type as $k_lesson => &$v_lesson) {
								switch($k_lesson) {
									case "id":
									case "ring_id":
									case "day_id":
									case "group_id":
									case "type":
										unset($v_type[$k_lesson]);
										break;
									case "Subject":
									case "Teacher":
										if (empty($v_lesson))
											$v_type[$k_lesson] = null;
										else
											$v_type[$k_lesson] = $v_lesson['name'];
								}
							}
						}
					}
					if (empty($v_group))
						unset($v_ring[$k_group]);
				}
			}
		}

		return $data;
	}
	
	//	Извлечение только используемых звонков из таблицы расписаний
	private function extract_used_rings($table, $rings) {
		
		$rings_ready = array();
		
		foreach($table as $k_day => &$v_day) {
			//	[day_id]
			foreach($v_day as $k_ring => &$v_ring) {
				//	[ring_id]
				foreach($rings as $id => $ring) {
					if ($ring['id'] == $k_ring) {
						array_push($rings_ready, $ring);
						unset($rings[$id]);
						break;
					}
				}
			}
		}
		
		return $rings_ready;
		
	}
	
}	

/*
	Свой класс для кодирования строк, содержащих русские символы, в JSON
*/
class Json{
  static public $_code;

  static public function json_encode($data){
    Json::$_code=0;
    return preg_replace_callback('/\\\\u([0-9a-f]{4})/i',
      function($val){
        $val=hexdec($val[1]);
          if(Json::$_code){
            $val=((Json::$_code&0x3FF)<<10)+($val&0x3FF)+0x10000;
            Json::$_code=0;
          }elseif($val>=0xD800&&$val<0xE000){
            Json::$_code=$val;
            return '';
          }
          return html_entity_decode(sprintf('&#x%x;', $val), ENT_NOQUOTES, 'utf-8');
      }, json_encode($data)
    );
  }
}

?>