<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	protected function getLastYear(){
		//задаем глобальные переменные для отображения года
		$this->loadModel('Year');			
		$year = $this->Year->find('first', array('order' => array('id'=>'desc')));
		return $year;
	}
	
	protected function redirectRoot() {
		$this->redirect(array('controller' => 'users', 'action' => 'login'));
	}
	
	protected function redirectConnectionError() {
		$this->redirect(array('controller' => 'students', 'action' => 'login'));
	}
	
	protected function getSpecialities($type = null, $findOption = null, $facultet_id = null, $show_dividers = false) {
		//	type - тип специальности для вывода: 0 - бакалавр, 1 - специалитет, 2 - магистратура (по умолчанию - все)
		//	findOption - тип вывода записей: all, list .. (по умолчанию - all)
		//	facultet_id - id факультета (по умолчанию берет из текущей сессии)
		//	show_dividers - добавление к результатам поиска разделителя типов специальностей (по умолчанию - false)
			//	уместно только для select box'ов
			
		$specialities = null;
		
		$this->loadModel('User');
		$this->loadModel('Speciality');
		
		if ($facultet_id == null) {
			$users = $this->User->find('all', array('conditions' => array('login' => $this->Session->read('user'))));
			$facultet_id = $users[0]['User']['facultet_id'];
		}
		
		if ($findOption == null)
			$findOption = "all";
		
		if ($type != null) {
			$specialities = $this->Speciality->find($findOption, array('conditions' => array('type' => $type, 'faculty_id' => $facultet_id), 'order' => array('type' => 'asc', 'code'=>'asc')));
		} else {
			$specialities = array();
			for ($i = 0; $i < 3; $i++) {
				$find_res = $this->Speciality->find($findOption, array('conditions' => array('type' => $i, 'faculty_id' => $facultet_id), 'order' => array('type' => 'asc', 'code'=>'asc')));
				if (!empty($find_res))
					$specialities[$this->get_specialityName($i)] = $find_res;
			}
// 			$specialities = $this->Speciality->find($findOption, array('conditions' => array('faculty_id' => $facultet_id), 'order' => array('type' => 'asc', 'code'=>'asc')));
		}
		
		return $specialities;
	}
	
	//	Возвращает название типа специальности
	private function get_specialityName($type) {
		if ($type === 0) {
			$name = "Бакалавриат";
		} else if ($type === 1) {
			$name = "Магистратура";
		} else if ($type === 2) {
			$name = "Специалитет";
		} else {
			$name = "Иное";
		}
		return $name;
	}
	
	protected function methodException() {
		throw new MethodNotAllowedException(__('Запрашиваемая страница недоступна'));
	}
	
	//	Загузка информации о группах
	protected function get_Groups($speciality_id, $course, $findType = 'all') {
		$this->loadModel('Group');	
						
		$options = array(
			'conditions' => array(
				'Group.course' => $course,
				'Group.speciality_id' => $speciality_id,
				),
			'order' => array('id'=>'asc')	//	Тут можно прописать сортировку групп специальности
		);
		return $this->Group->find($findType, $options);
	}
	
	//	Преобразование массива групп
	protected function transform_groupsArray($groups) {
		$groups_id_array = array();
		foreach ($groups as $group) {
			array_push($groups_id_array, $group['Group']['id']);
		}
		return $groups_id_array;
	}
	
	//	Преобразование массива звонков
	protected function get_transformedRingsArray() {
		$this->loadModel('Ring');
		$rings = $this->Ring->find('all');
		$rings = Set::combine($rings, '{n}.Ring.id', '{n}');	//	преобразование ключей в id
		return $rings;
	}
	
	//	Получение компактного массива звонков
	protected function get_compactRingsArray() {
		$this->loadModel('Ring');
		$rings = $this->Ring->find('all');
		$rings = $this->redunantModelName($rings, "Ring");
		return $rings;
	}
	
	//	Очистка уровня массива - имени модели
	protected function redunantModelName($data, $modelName) {
		return Set::extract('/'.$modelName.'/.', $data);
	}
	
	//	Возвращает массив предметов, находящихся в массиве $groups_id (in ($groups_id))
	//		Правило: первая id группы из group_id доджна принадлежать списку групп
	protected function getLessonsIn($groups_id, $another_condition = null) {
		$this->loadModel("Lesson");
		if (!$another_condition)
			return $this->Lesson->query('select * from lessons as Lesson where group_id in('.$groups_id.')');
		else
			return $this->Lesson->query('select * from lessons as Lesson where (group_id in('.$groups_id.')) and ('.$another_condition.')');
	}
	
	//	Подгружает необходимые данные в занятия вместо id учителя и предмата
	protected function load_SubjectsTeachers($lessons)
	{
		$this->loadModel('Subject');
		$this->loadModel('Teacher');
		
		foreach ($lessons as $key => $lesson) {
			
			$subj = $this->Subject->find('first', array('conditions' => array('Subject.' . $this->Subject->primaryKey => $lesson['Lesson']['subject_id'])));
			$lessons[$key]['Lesson']['Subject'] = $subj['Subject'];
			if ($lesson['Lesson']['teacher_id'] !== null) {
				$teach = $this->Teacher->find('first', array('conditions' => array('Teacher.' . $this->Teacher->primaryKey => $lesson['Lesson']['teacher_id'])));
				$lessons[$key]['Lesson']['Teacher'] = $teach['Teacher'];
			} else {
				$lessons[$key]['Lesson']['Teacher'] = array();
			}
			
			unset($lessons[$key]['Lesson']['subject_id']);
			unset($lessons[$key]['Lesson']['teacher_id']);
			
		}
		
		return $lessons;
	}
	
	//	Преобразует массив занятий в массив, готовый для вывода
	protected function MakeTableArray($lessons, $neededGroupsID) {
		
		$days = array();
		
		//	Создаем пустую матрицу таблицы
		foreach ($lessons as $lesson)
		{
			foreach ($neededGroupsID as $group_id)
			{
				$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']][$group_id]['up'] = array();
				$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']][$group_id]['down'] = array();
			}
		}
		
		//	Сортируем сначала все занятия групп
		foreach ($neededGroupsID as $group_id)	//	Проход по id групп в отдельности
		{
			foreach ($lessons as $key => $lesson)	//	Проход по всем занятиям с удалением тех, которые мы вставляем
			{
				if ($group_id == $lesson['Lesson']['group_id'])	//	Назначение занятия в случае, когда совпадают group_id занятия и id группы
				{
					//	Случай, когда в записываемой ячейке уже находятся данные: $days[day_id][ring_id][group_id]
					//	означает, что одно из этих данных - по числителю, другое - по знаменателю
					//	Необходимо их объединить!
					if ($lesson['Lesson']['type'] == "0")
						$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']][$group_id]['up'] = $lesson;
					else if ($lesson['Lesson']['type'] == "1")
						$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']][$group_id]['down'] = $lesson;
					
					unset($lessons[$key]);
				}
			}
		}
		
		//	Теперь сортируем записи для всех групп, оставляя только их
		foreach ($lessons as $key => $lesson)
		{
			//	Случай, когда в записываемой ячейке уже находятся данные: $days[day_id][ring_id][group_id]
			//	означает, что одно из этих данных - по числителю, другое - по знаменателю
			//	Необходимо их объединить!
			if ( !array_key_exists("all", $days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']]) ) {
					
				//	Стираем все остальные записи групп
				$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']] = array();
				//	Создаем новые массивы для общих записей
				$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']]['all']['up'] = array();
				$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']]['all']['down'] = array();
			
			}
			
			if ($lesson['Lesson']['type'] == "0")
				$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']]['all']['up'] = $lesson;
			else if ($lesson['Lesson']['type'] == "1")
				$days[$lesson['Lesson']['day_id']][$lesson['Lesson']['ring_id']]['all']['down'] = $lesson;
			
			unset($lessons[$key]);
		}
		
		ksort($days);
		//	[day_id][ring_id][group_id] - занятия
		foreach ($days as $key => $day) {
			ksort($day);	//	Сортировка звонков по id
			$days[$key] = $day;
		}
		
		return $days;
	}
	
	//	Удаление элемента из модели
	protected function deleteElementFromModel($elem_id, $modelName) {
		$this->loadModel($modelName);
		$this->$modelName->id = $elem_id;
		if (!$this->$modelName->exists()) {
			throw new NotFoundException(__('Не найден '.$modelName.' с ID='.$elem_id));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->$modelName->delete()) {
			return true;
		} else {
			//return false;
			throw new InternalErrorException('Невозможно удалить запись с ID='.$elem_id.' из "'.$modelName.'"!'); // 500 error
		}
	}
	
	//	Создание нового элемента в модели $modelName с данными $data
	protected function createElementToModel($data, $modelName) {
		//	Примечание: $data не обязательно должно быть приведено к виду "array($modelName => array(..))"
		
		$this->loadModel($modelName);
		$this->$modelName->create();
		if (!array_key_exists($modelName, $data)) {
			$data = array(
				$modelName => $data
			);
		}
		if ($this->$modelName->save($data)) {
			return $this->$modelName->id;
		} else {
			//return false;
			throw new InternalErrorException('Невозможно сохранить данные в "'.$modelName.'"!'); // 500 error
		}
	}
	
	//	Обновление данных элемента в модели $modelName данными $data
	protected function updateElementToModel($elem_id, $data, $modelName) {
		//	Примечание: $data не обязательно должно быть приведено к виду "array($modelName => array(..))"
		
		$this->loadModel($modelName);
		$this->$modelName->id = $elem_id;
		if (!$this->$modelName->exists()) {
			throw new NotFoundException(__('Не найден '.$modelName.' с ID='.$elem_id));
		}
		if (!array_key_exists($modelName, $data)) {
			$data = array(
				$modelName => $data
			);
		}
		if ($this->$modelName->save($data)) {
			return true;
		} else {
			//return false;
			throw new InternalErrorException('Невозможно обновить запись с ID='.$elem_id.' в "'.$modelName.'"!'); // 500 error
		}
	}
	
	protected function getElementsFromModel($findType, $conditions, $modelName, $other_options = null) {
		$this->loadModel($modelName);
		$options = array(
			'conditions' => $conditions
		);
		if ($other_options)
			$options += $other_options;
		return $this->$modelName->find($findType, $options);
	}
	
	//	Вывод список SQL запросов модели
	protected function getSQLlog($model)
	{
		$this->loadModel($model);
		$log = $this->$model->getDataSource()->getLog(false, false);
		debug($log);
	}
	
}
