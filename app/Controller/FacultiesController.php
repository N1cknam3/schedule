<?php
	
App::uses('AppController', 'Controller');	
	
class FacultiesController extends AppController {
	
	const ERR_ALL_FIELDS = 'Все поля обязательны для заполнения!';
	const ERR_FIELD_CODE = 'Необходимо заполнить поле "Код"';
	const ERR_FIELD_NAME = 'Необходимо заполнить поле "Имя"';
	const ERR_FIELD_PROFILE = 'Необходимо заполнить поле "Профиль"';
	const ERR_SAVE = 'Сохранение не удалось. Попробуйте еще раз';
	
	public $name = 'Faculties';
	public $helpers = array('Html', 'Session', 'Widgets');
	public $uses = array();	
	
	public $currentPage;
	public $openButton;
	
	// проверка на авторизацию
	public function is_login() {
		$user = $this->Session->read('user');
		
        if (!empty($user)) {
            return true;
        }		
        else {
        	return false;
        }
	}
	
	//	Получаем ID факультета текущего пользователя, если он в системе, иначе - NULL
	private function getCurrentFacultyID(){
		if ($this->is_login()) {
			$this->loadModel('User');
			$this->loadModel('Faculty');
			$user = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));
			$faculty = $this->Faculty->find('first', array('conditions' => array('id' => $user['User']['facultet_id'])));
			return $faculty['Faculty']['id'];
		} else {
			return null;
		}
	}
	
	//задает название заголовка, добавляя в конец название факультета
	public function setTitleName($text = null, $facultyName = true) {
		if ($facultyName)
		{
			$this->loadModel('User');
			$this->loadModel('Faculty');
			$user = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));
			$faculty = $this->Faculty->find('first', array('conditions' => array('id' => $user['User']['facultet_id'])));
			
			$this->set('title_for_layout', $text . ' ' . $faculty['Faculty']['name']);
		}
		else
		{
			$this->set('title_for_layout', $text);
		}
	}
	
	//задает необходимые переменные перед отрисовкой страницы
	public function beforeRender(){
		
		$this->set('jsVars', $this->_jsVars);
		
		$this->set('dropdownIcon1Class', 'icon-chevron-right');
		
		//задаем глобальные переменные для отображения кнопки выбранного раздела
		if ($this->currentPage == '1') {
			$this->set('mainClass', 'open');
		} else if ($this->currentPage == '2') {
			if ($this->openButton == true)
			{
				$this->set('dropdownButton1Class', 'subdrop');
				$this->set('dropdownList1Style', 'display: block; visibility: visible;');
				$this->set('dropdownIcon1Class', 'icon-chevron-down');
			} else {
				$this->set('dropdownButton1Class', '');
				$this->set('dropdownList1Style', 'display: none; visibility: visible;');
			}
		} else if ($this->currentPage == '3') {
			$this->set('chairsClass', 'open');
		} else if ($this->currentPage == '4') {
			$this->set('timeTableClass', 'open');
		}
		
		//задаем глобальные переменные для отображения года
		$year = $this->getLastYear();
		$this->set('year', $year['Year']['name']);
	}
	
	public function beforeFilter() {
		if (!$this->is_login()) {
			$this->redirectRoot();
		}	
	}
	
	// кабинет администратора факультета
	public function admin() {
		
		$this->redirect("timeTable");
		
		$this->layout = 'admin';
		
		$this->setTitleName('Расписание факультета');		
		$this->currentPage = '1';
		
	}
	
	public function specialities($type = null) {
		
		$this->layout = 'admin';		
		//назначение кнопки 2 "Специальности" статуса активной
		$this->currentPage = '2';
		//назначение списку раскрывающейся кнопки статуса раскрытой
		$this->openButton = true;		
				
		$specialities = $this->getSpecialities($type);
				
		if ($type == 0)
		{
			$this->setTitleName('Направления подготовки бакалавров факультета');
		}
		else if ($type == 1)
		{
			$this->setTitleName('Направления подготовки магистров факультета');
		}
		else if ($type == 2)
		{
			$this->setTitleName('Направления подготовки специалистов факультета');
		}
		else
		{
			//	Ветка ошибки, но стоит предусмотреть
			$this->setTitleName('Специальности');
			$specialities = $this->getSpecialities();
		}
		
		$this->set('type', $type);
		$this->set('specialitiesArray', $specialities);
		$this->set('textToAddButton', "Добавить специальность");
		$this->set('actionToAddButton', "speciality_add_show");
		
	}
	
	//::::Динамические функции для удаления-редактирования:::://
	
	//	Добавление новой специальности
	public function speciality_ajax_add() {
		$this->layout = 'ajax';
		
		//необходимо сперва вывести все, что есть по специальностям этого факультета
		$this->loadModel('User');
		$this->loadModel('Speciality');
		$this->loadModel('Faculty');
		
		$users = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));		
		$type = $this->request->data['Faculty']['type'];
		$specialities = $this->Speciality->find('all', array('conditions' => array('type' => $type, 'faculty_id' => $users['User']['facultet_id']), 'order' => array('code'=>'asc')));		
		
		$this->set('type', $type);
		$this->set('specialitiesArray', $specialities);
		
		if (!empty($this->request->data)) {						
			
			$was_errors = false;
			
			if (empty($this->request->data['Faculty']['code']))
			{
				$this->set("error_addSpecialityCode", 'true');
				$was_errors = true;
			}
			
			if (empty($this->request->data['Faculty']['name']))
			{
				$this->set("error_addSpecialityName", 'true');
				$was_errors = true;
			}
			
			if (empty($this->request->data['Faculty']['profile']))
			{
				$this->set("error_addSpecialityProfile", 'true');
				$was_errors = true;
			}
			
			if ($was_errors) {
				$this->set("hidden_message_add", self::ERR_ALL_FIELDS);
				return;
			}		
			
			$this->set("hidden_message_add", 'ok');	
			
			//сохранить полученные данные в БД							
			$faculty = $this->Faculty->find('first', array('conditions' => array('id' => $users['User']['facultet_id'])));							
						
			$data = array(
				'Speciality' => array(
					'faculty_id' => $faculty['Faculty']['id'],
					'code' => $this->request->data['Faculty']['code'],
					'name' => $this->request->data['Faculty']['name'],
					'profile' => $this->request->data['Faculty']['profile'],
					'type' => $type
				)
			);
			
			//сохраняем запись
			$this->Speciality->create();
			if ($this->Speciality->save($data)) {
				//$this->Session->setFlash(__('Сохранение прошло успешно'));
			} else {
				$this->set("hidden_message_add", self::ERR_SAVE);
				return;
			}
				
			$specialities = $this->Speciality->find('all', array('conditions' => array('type' => $type, 'faculty_id' => $users['User']['facultet_id']), 'order' => array('code'=>'asc')));
			
			$this->set('type', $type);
			$this->set('specialitiesArray', $specialities);
			
			$this->set("hidden_message_add", 'ok');
			$this->set('textToAddButton', "Добавить специальность");
			$this->set('actionToAddButton', "speciality_add_show");
			
		}
	}
	
	//	Изменение специальности
	public function speciality_ajax_edit() {
		$this->layout = 'ajax';
		
		//необходимо сперва вывести все, что есть по специальностям этого факультета
		$this->loadModel('User');
		$this->loadModel('Speciality');
		$this->loadModel('Faculty');
		
		$users = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));
		$type = $this->request->data['Faculty']['type'];
		$specialities = $this->Speciality->find('all', array('conditions' => array('type' => $type, 'faculty_id' => $users['User']['facultet_id']), 'order' => array('code'=>'asc')));		
		
		$this->set('type', $type);
		$this->set('specialitiesArray', $specialities);
		
		if (!empty($this->request->data)) {						
			
			$was_errors = false;
			
			if (empty($this->request->data['Faculty']['code']))
			{
				$this->set("error_editSpecialityCode", 'true');
				$was_errors = true;
			}
			
			if (empty($this->request->data['Faculty']['name']))
			{
				$this->set("error_editSpecialityName", 'true');
				$was_errors = true;
			}
			
			if (empty($this->request->data['Faculty']['profile']))
			{
				$this->set("error_editSpecialityProfile", 'true');
				$was_errors = true;
			}
			
			if ($was_errors) {
				$this->set("hidden_message_edit", self::ERR_ALL_FIELDS);
				return;
			}
			
			$spec_id = $this->request->data['Faculty']['id'];

			if (!$this->Speciality->exists($spec_id)) {
				throw new NotFoundException(__('Неизвестный ID='.$spec_id));
			}
			$this->Speciality->id = $spec_id;
			if ($this->Speciality->save($this->request->data['Faculty'])) {
				//$this->Session->setFlash(__('Сохранение прошло успешно'));
			} else {
				$this->set("hidden_message_edit", self::ERR_SAVE);
				return;
			}

			$specialities = $this->Speciality->find('all', array('conditions' => array('type' => $type, 'faculty_id' => $users['User']['facultet_id']), 'order' => array('code'=>'asc')));
			
			$this->set('type', $type);
			$this->set('specialitiesArray', $specialities);
			
			$this->set("hidden_message_add", 'ok');
			$this->set('textToAddButton', "Добавить специальность");
			$this->set('actionToAddButton', "speciality_add_show");
		}

	}
	
	//	Удаление специальности
	public function speciality_ajax_delete($elemId_to_delete = null, $elemType_to_delete = null) {
		$this->layout = 'ajax';
		
		//сперва удаляем выбранную запись
		if ($elemId_to_delete == null)	
			$elem_id = $this->request->data['Hidden']['elementIdToDelete'];
		else
			$elem_id = $elemId_to_delete;
		
		if ($elem_id != null) {
			$this->loadModel('Speciality');
			$this->Speciality->id = $elem_id;
			if (!$this->Speciality->exists()) {
				throw new NotFoundException(__('Не найдена специальность с ID='.$elem_id));
			}
			if (!$this->Speciality->delete()) {
				echo 'Специальность не удалена. Попробуйте еще раз.';
			}
		}
		
		//затем необходимо вывести все, что есть по специальностям этого факультета
		$this->loadModel('User');
		$this->loadModel('Speciality');
		$this->loadModel('Faculty');
		
		$users = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));		
		if ($elemType_to_delete == null)
			$type = $this->request->data['Hidden']['elementTypeToDelete'];
		else
			$type = $elemType_to_delete;
		$specialities = $this->Speciality->find('all', array('conditions' => array('type' => $type, 'faculty_id' => $users['User']['facultet_id']), 'order' => array('code'=>'asc')));		
		
		$this->set('type', $type);
		$this->set('specialitiesArray', $specialities);
		$this->set('textToAddButton', "Добавить специальность");
		$this->set('actionToAddButton', "speciality_add_show");
	}
	
	//-----------КАФЕДРЫ--------------//
	
	public function chairs() {
		
		$this->layout = 'admin';
		
		//назначение кнопки 3 "Кафедры" статуса активной
		$this->currentPage = '3';
		
		//необходимо сперва вывести все, что есть по кафедрам этого факультета
		$this->loadModel('User');
		$this->loadModel('Chair');
		$this->loadModel('Faculty');
		
		$users = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));
		$chairs = $this->Chair->find('all', array('conditions' => array('faculty_id' => $users['User']['facultet_id']), 'order' => array('name'=>'asc')));		
		
		$this->set('chairsArray', $chairs);
		
		$this->set('chairsArray', $chairs);
		
		$this->setTitleName('Кафедры факультета');
		$this->set('textToAddButton', "Добавить кафедру");
		$this->set('actionToAddButton', "chair_add_show");
	}
	
	//	Добавление новой кафедры
	public function chair_ajax_add() {
		$this->layout = 'ajax';
		$this->set('actionToAddButton', "chair_add_show");
		$this->set('textToAddButton', "Добавить кафедру");
		
		//необходимо сперва вывести все, что есть по кафедрам этого факультета
		$this->loadModel('User');
		$this->loadModel('Chair');
		$this->loadModel('Faculty');
		
		$users = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));
		$chairs = $this->Chair->find('all', array('conditions' => array('faculty_id' => $users['User']['facultet_id']), 'order' => array('name'=>'asc')));		
		
		$this->set('chairsArray', $chairs);
		
		if (!empty($this->request->data)) {						
			
			$was_errors = false;
			
			if (empty($this->request->data['Chair']['name']))
			{
				$this->set("error_addChairName", 'true');
				$was_errors = true;
			}	
			
			if ($was_errors) {
				$this->set("hidden_message_chair_add", self::ERR_ALL_FIELDS);
				return;
			}	
			
			$this->set("hidden_message_chair_add", 'ok');	
			
			//сохранить полученные данные в БД							
			$faculty = $this->Faculty->find('first', array('conditions' => array('id' => $users['User']['facultet_id'])));							
						
			$data = array(
				'Chair' => array(
					'faculty_id' => $faculty['Faculty']['id'],
					'name' => $this->request->data['Chair']['name']
				)
			);
			
			//сохраняем запись
			$this->Chair->create();
			if ($this->Chair->save($data)) {
				//$this->Session->setFlash(__('Сохранение прошло успешно'));
			} else {
				$this->set("hidden_message_chair_add", self::ERR_SAVE);
				return;
			}
				
			$chairs = $this->Chair->find('all', array('conditions' => array('faculty_id' => $users['User']['facultet_id']), 'order' => array('name'=>'asc')));

			$this->set('chairsArray', $chairs);
			
			$this->set("hidden_message_chair_add", 'ok');
			
		}
	}
	
	//	Изменение кафедры
	public function chair_ajax_edit() {
		$this->layout = 'ajax';
		
		//необходимо сперва вывести все, что есть по кафедрам этого факультета
		$this->loadModel('User');
		$this->loadModel('Chair');
		$this->loadModel('Faculty');
		
		$users = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));
		$chairs = $this->Chair->find('all', array('conditions' => array('faculty_id' => $users['User']['facultet_id']), 'order' => array('name'=>'asc')));		
		
		$this->set('chairsArray', $chairs);
		$this->set('textToAddButton', "Добавить кафедру");
		$this->set('actionToAddButton', "chair_add_show");
		
		if (!empty($this->request->data)) {						
			
			$was_errors = false;
			
			$was_errors = false;
			
			if (empty($this->request->data['Chair']['name']))
			{
				$this->set("error_editChairName", 'true');
				$was_errors = true;
			}	
			
			if ($was_errors) {
				$this->set("hidden_message_chair_edit", self::ERR_ALL_FIELDS);
				return;
			}	
			
			$this->set("hidden_message_chair_edit", 'ok');
			
			$chair_id = $this->request->data['Chair']['id'];

			if (!$this->Chair->exists($chair_id)) {
				throw new NotFoundException(__('Неизвестный ID='.$chair_id));
			}
			$this->Chair->id = $chair_id;
			if ($this->Chair->save($this->request->data['Chair'])) {
				//$this->Session->setFlash(__('Сохранение прошло успешно'));
			} else {
				$this->set("hidden_message_chair_edit", self::ERR_SAVE);
				return;
			}

			$chairs = $this->Chair->find('all', array('conditions' => array('faculty_id' => $users['User']['facultet_id']), 'order' => array('name'=>'asc')));	

			$this->set('chairsArray', $chairs);
		}

	}
	
	//	Удаление кафедры
	public function chair_ajax_delete($elemId_to_delete = null) {
		$this->layout = 'ajax';
		
		//сперва удаляем выбранную запись
		$elem_id = $elemId_to_delete;
		
		if ($elem_id != null) {
			$this->loadModel('Chair');
			$this->Chair->id = $elem_id;
			if (!$this->Chair->exists()) {
				throw new NotFoundException(__('Не найдена кафедра с ID='.$elem_id));
			}
			if (!$this->Chair->delete()) {
				echo 'Кафедра не удалена. Попробуйте еще раз.';
			}
		}
		
		$this->set('actionToAddButton', "chair_add_show");
		$this->set('textToAddButton', "Добавить кафедру");
		
		//затем необходимо вывести все, что есть по кафедрам этого факультета
		$this->loadModel('User');
		$this->loadModel('Chair');
		$this->loadModel('Faculty');
		
		$users = $this->User->find('first', array('conditions' => array('login' => $this->Session->read('user'))));
		$chairs = $this->Chair->find('all', array('conditions' => array('faculty_id' => $users['User']['facultet_id']), 'order' => array('name'=>'asc')));		
		
		$this->set('chairsArray', $chairs);
		
		$this->set('textToAddButton', "Добавить кафедру");
		$this->set('actionToAddButton', "chair_add_show");
	}
	
//%$&$%&$%^%$&%$^##$^#$%%#$$#%^#%$#$%^#$%$#^%$#$^#$^%#^@%&#^@$%$^#@%$&*%!@$^%@^&!$#@&^$*%!@$@!^!//
//(^%#*(_^$_)@!*^$*^%*^^#%&^#@*&$#@&($*%@^#$%*@&#$#^@(*%@!&^#*%$#$&($@^*!&%$#%^&*&^%$#@#$%^$%^@$//
//#$%^&*&^%$%^&^*&%$&*^%$&*%$$%&*(^%&#$^&*()^*&%^(*^^&$%(#$%^$%^&$%#$%$^@%$(*@^*&!%$$#%%^&*$*@^$//
//-------------------------------------РАБОТА С РАСПИСАНИЕМ-------------------------------------//
	
	public function timeTable() {
		$this->layout = 'admin';
		//назначение кнопки 2 "Специальности" статуса активной
		$this->currentPage = '4';
		$this->setTitleName('Расписание факультета');
	}
	
	public function schedule_step1_showSpecialitySelectbox(){
		$this->layout = 'ajax';
		
		$specialities = $this->getSpecialities(null, 'list', null, true);
		$this->set('specialitiesArray', $specialities);
	}
	
	public function schedule_step2_showTable(){
		$this->layout = 'ajax';
		
		if( $this->request->is('ajax') ) {
			
			//	1.
			//	Получаем id специальности через ajax и выбранный элемент selectbox'а
				$speciality_id = $this->request->data('speciality_id_toSend'); 		
				
			//	Загрузка информации о специальности
				$this->loadModel('Speciality');	
						
				$options = array('conditions' => array('Speciality.' . $this->Speciality->primaryKey => $speciality_id));
			$this->set('speciality', $this->Speciality->find('first', $options));
				unset($options);
			
			//	2.
			//	Получаем курс через ajax и выбранный элемент selectbox'а
				$course = $this->request->data('course_toSend'); 
			
			//	Загрузка информации о группах
				$groups = $this->get_Groups($speciality_id, $course);	//	метод в AppController
			$this->set('groups', $groups);
			
			//	3.
			//	Сбор необходимых данных из БД
			
			$this->loadModel('Lesson');
			
			$table = null;
			
			//	Проверка на существование групп этой специальности
			if (count($groups) != 0)
			{
					//	Берем id этих групп в отдельный массив
					$groups_id_array = $this->transform_groupsArray($groups);	//	метод в AppController
					
					//	Ищем все занятия этих групп
					$lessons = $this->getLessonsIn(implode(',', $groups_id_array));	//	метод в AppController
						
					//	Сразу подставляем нужные данные в занятия
					$lessons = $this->load_SubjectsTeachers($lessons);	//	метод в AppController
					
					//	Сортировка массива занятий и приведение к виду:
					//	[day_id][ring_id][group_id] - занятия
					$table = $this->MakeTableArray($lessons, $groups_id_array);	//	метод в AppController
					
					//	Достаем массив звонков для дальнейшего вывода таблицы
					$rings = $this->get_transformedRingsArray();
				$this->set('rings', $rings);
					
					//	Достаем массив дней недели для дальнейшего вывода таблицы
					$this->loadModel('Day');
					$days = $this->Day->find('all');
				$this->set('days', $days);
				
			}
			
			$this->set('table', $table);
			
		} else {
			$this->methodException();
		}
	}
		
	//	AJAX автоподстановки предмета
	public function ajax_getSubjects() {
		
		$this->layout = 'ajax';
		
		if( $this->request->is('ajax') ) {
			
			//	Получение передаваемых данных
			$keyword = $this->request->data('keyword');
			
			//	Первыми выдаются варианты, созданные этим факультетомы
			$faculty_id = $this->getCurrentFacultyID();
			
			if (!empty($keyword)) {
				$this->loadModel("Subject");
				
// 				$subjects = $this->Subject->query("select * from subjects as Subject where name like '".$keyword."%' order by name limit 5");
				
				$subjects = $this->Subject->query("select * from subjects as Subject where (( name like '".$keyword."%') and (faculty_id = '".$faculty_id."')) order by name limit 5");
				$subjects += $this->Subject->query("select * from subjects as Subject where (( name like '".$keyword."%') and (faculty_id IS NULL)) order by name limit 5");
				$subjects += $this->Subject->query("select * from subjects as Subject where (( name like '".$keyword."%') and (faculty_id <> '".$faculty_id."')) order by name limit 5");
				$subjects = array_slice($subjects, 0, 5);
				
				$this->set("subjects", $subjects);
			}
			
		}
	}
	
	//	AJAX автоподстановки преподавателя
	public function ajax_getTeachers() {
		
		$this->layout = 'ajax';
		
		if( $this->request->is('ajax') ) {
			
			//	Получение передаваемых данных
			$keyword = $this->request->data('keyword');
			
			//	Первыми выдаются варианты, созданные этим факультетомы
			$faculty_id = $this->getCurrentFacultyID();
			
			if (!empty($keyword)) {
				$this->loadModel("Teacher");
				
// 				$teachers = $this->Teacher->query("select * from teachers as Teacher where name like '".$keyword."%'  order by name limit 5");
				
				$teachers = $this->Teacher->query("select * from teachers as Teacher where (( name like '".$keyword."%') and (faculty_id = '".$faculty_id."')) order by name limit 5");
				$teachers += $this->Teacher->query("select * from teachers as Teacher where (( name like '".$keyword."%') and (faculty_id IS NULL)) order by name limit 5");
				$teachers += $this->Teacher->query("select * from teachers as Teacher where (( name like '".$keyword."%') and (faculty_id <> '".$faculty_id."')) order by name limit 5");
				$teachers = array_slice($teachers, 0, 5);
				
				$this->set("teachers", $teachers);
			}
			
		}
	}
	
	//	AJAX сохранения автоподстановки предмета при завершении ввода
	public function ajax_autocomplete_subject_save() {
		$this->autoRender = false;
		if( $this->request->is('ajax') ) {
			//	Получение передаваемых данных
			$text = $this->request->data('text');
			if ($text) {
				$subject = $this->getAndUpdateDictionary($text, 'Subject', $this->getCurrentFacultyID());
			}
		} else {
			$this->methodException();
		}
	}
	
	//	AJAX сохранения автоподстановки преподавателя при завершении ввода
	public function ajax_autocomplete_teacher_save() {
		$this->autoRender = false;
		if( $this->request->is('ajax') ) {
			//	Получение передаваемых данных
			$text = $this->request->data('text');
			if ($text) {
				$teacher = $this->getAndUpdateDictionary($text, 'Teacher', $this->getCurrentFacultyID());
			}
		} else {
			$this->methodException();
		}
	}
	
	//	Сохранение данных таблицы расписания
	public function set_schedule_table() {
		//	Возникает при нажатии кнопки сохранения таблицы
		
		//	Отключение необходимости View
		$this->autoRender = false;
		
		//	Получение данных из таблицы
		$data = $this->request->data;
		
		//	Вытаскиваем массив групп
		$groups = $data['Group'];
		unset($data['Group']);
		
		//	Сохранение данных о группах
		foreach($groups as $group_id => $group_data) {
			
			$this->updateElementToModel($group_id, $group_data, 'Group');
			
		}
		
		//	Преобразование данных
		foreach($data as $d_key_old => $d_array) {
			//	Дни
			$d_key_new = str_replace("d-", "", $d_key_old);
			$data[$d_key_new] = $data[$d_key_old];
			unset($data[$d_key_old]);
			
			foreach ($d_array as $r_key_old => $r_array) {
				//	Звонки
				$r_key_new = str_replace("r-", "", $r_key_old);
				$data[$d_key_new][$r_key_new] = $data[$d_key_new][$r_key_old];
				unset($data[$d_key_new][$r_key_old]);
				
				foreach ($r_array as $g_key_old => $g_array) {
					//	Группы
					$g_key_new = str_replace("g-", "", $g_key_old);
					if ($g_key_new != "no") {
						$data[$d_key_new][$r_key_new][$g_key_new] = $data[$d_key_new][$r_key_new][$g_key_old];
					}
					unset($data[$d_key_new][$r_key_new][$g_key_old]);
				}
				//	Проверка на случай отсутствия занятий для звонка
				if (empty($data[$d_key_new][$r_key_new]))
					unset($data[$d_key_new][$r_key_new]);
			}
		}
		
		//	Составление списка групп для поиска
		$groups_id = "";
		foreach($groups as $g_key => $g_array) {
			$groups_id .= $g_key.",";
		}
		$groups_id = substr_replace($groups_id, "", strlen($groups_id) - 1);
		
		//	Ищем все занятия этих групп
		$lessons_groups = $this->getLessonsIn($groups_id);
		
		//	после работы следующих вложенных циклов в массиве $lessons_groups
		//	останутся те записи, которые мы не обновляли,
		//	а значит их следует удалить
		
		//	Сохранение и обновление всех записей
		foreach($data as $d_key => $d_array) {
			//	Дни
			foreach ($d_array as $r_key => $r_array) {
				//	Звонки
				foreach ($r_array as $g_key => $g_array) {
					//	Группы
					
					if ($g_key == "all") {
						//	Общий предмет, $group_id = "№группы1,№группы2,№группы3.."
						$this->saveLessons($d_key, $r_key, $groups_id, $g_array, $lessons_groups);

					} else {
						//	Много предметов, $group_id = "№группы"
						$this->saveLessons($d_key, $r_key, $g_key, $g_array, $lessons_groups);
						
					}
				}
			}
		}
		
		//	удаляем все записи, которые не изменились в ходе работы
		foreach($lessons_groups as $lesson_group) {
			$this->deleteElementFromModel($lesson_group['Lesson'], 'Lesson');
		}
		
		/*
		echo "Удалены:";
		debug($lessons_groups);
		echo "<br>";
		echo "<br>";
		*/
		
	}
	
	//	Сохранение и обновление занятий, переданных через $group_lessons_array
	private function saveLessons($day_id, $ring_id, $group_id, $group_lessons_array, &$lessons_list) {
		//	$day_id, $ring_id, $group_id - поля записи таблицы 'Lesson', которые необходимо сохранить
		//	$group_lessons_array - массив занятий группы (с вложенными массивами "up" и/или "down")
		//	$lessons_list - массив занятий, содержащий записи, которые программа еще не обновила
		//		этот массив передается ПО ССЫЛКЕ не случайно, что обусловлено изменением его значения,
		//		так как в дальнейшем этот массив проверяется
		
		if (!empty($group_lessons_array)) {
			foreach($group_lessons_array as $type => $lesson) {
			//	Типы
			
				if ($type == "up")
					$type_new = "0";
				else if ($type == "down")
					$type_new = "1";
				
				if ($lesson['subject'] != null) {
					$subject = $this->getAndUpdateDictionary($lesson['subject'], 'Subject', $this->getCurrentFacultyID());
					$subject_id = $subject['Subject']['id'];
				}
				$teacher_id = null;
				if (array_key_exists("name", $lesson)) {
					if ($lesson['name'] != null) {
						$teacher = $this->getAndUpdateDictionary($lesson['name'], 'Teacher', $this->getCurrentFacultyID());
						$teacher_id = $teacher['Teacher']['id'];
					}
				}
				
				$data_save = array(
					'Lesson' => array(
						'ring_id' => $ring_id,
						'day_id' => $day_id,
						'subject_id' => $subject_id,
						'teacher_id' => $teacher_id,
						'group_id' => $group_id,
						'type' => $type_new
					)
				);
				
				/*
				echo "Before";
				debug($lessons_groups);
				echo "After";
				debug($data_save);
				echo "<br>";
				echo "<br>";
				*/
				
				//	Изменяя элемент из списка занятий звонка этого дня ($lessons_groups)
				//	мы его удаляем из списка этих занятий
				//	для того, чтобы потом все неизмененные занятия удалить
				foreach($lessons_list as $les_key => $lesson_group) {
					if ($lesson_group['Lesson']['id'] == $lesson['id']) {
						unset($lessons_list[$les_key]);
						break;
					}
				}
				
				if ($lesson['id'] == "") {
					//	Создалась новая запись этой группы
					//	сохраняем её
					$this->createElementToModel($data_save, 'Lesson');
					
				} else {
					//	Возможно изменились данные существующей записи
					//	необходимо обновить поля
					$this->updateElementToModel($lesson['id'], $data_save, 'Lesson');
				}
			}
		}
		
	}
	
	//	Если нет предмета, то он добавляется в словарь предметов
	//	Возвращается предмет, у которого 'name' = $name,
	//	либо false, если сохранение прошло неудачно
	private function getAndUpdateDictionary($name, $modelName, $faculty_id = null) {
		$this->loadModel($modelName);
		$options = array('conditions' => array($modelName.'.name' => $name));
		$result = $this->$modelName->find('first', $options);
		if (empty($result)) {
			if ($this->createElementToModel(array('name' => $name, 'faculty_id' => $faculty_id), $modelName)) {
				return $this->getAndUpdateDictionary($name, $modelName);
			} else {
				return null;
			}
		} else {
			return $result;
		}
	}
	
	//	если $pre_ring_id = null, то значит, что вставляется первая запись в день
	public function ajax_getRing() {
		
		$this->layout = 'ajax';
		
		if( $this->request->is('ajax') ) {
			
			$day_id = $this->request->data('day_id');
			$insert_ring_id = $this->request->data('nextTime');
			$pre_ring_id = $this->request->data('prevTime');
			$position = $this->request->data('position');
		
			//	Получение информации о необходимым звонке
			$this->loadModel("Ring");
			$options = array('conditions' => array('Ring.' . $this->Ring->primaryKey => $insert_ring_id));
			$ring = $this->Ring->find("first", $options);
			
			//	Получение органов управления для этого звонка
			$type = "";
			if ($pre_ring_id != null) {
			
				if ($pre_ring_id - $insert_ring_id > 0) {
					//	Вставка ДО
					
					//	Если есть запись с меньшим id, то возможна еще одна вставка до
					if ($this->Ring->exists($insert_ring_id - 1)) {
						$type = 1;
					} else {
						$type = 0;
					}
				} else {
					//	Вставка ПОСЛЕ
					
					//	Если есть запись с большим id, то возможна еще одна вставка после
					if ($this->Ring->exists($insert_ring_id + 1)) {
						$type = 3;
					} else {
						$type = 0;
					}
				}
			
			} else {
				
				//Три варианта:
				// 1) + - (вставка возможна только ДО)
				// 2) + - + (вставка возможна и ДО и ПОСЛЕ)
				// 3) - + (вставка возможна только ПОСЛЕ)
				
				if ( ($this->Ring->exists($insert_ring_id - 1) ) && ($this->Ring->exists($insert_ring_id + 1)) ) {
					
					$type = 2;
					
				} else {
					
					if ($this->Ring->exists($insert_ring_id + 1)) {	// - +
						$type = 3;
					} else if ($this->Ring->exists($insert_ring_id - 1)) {	// + -
						$type = 1;
					} else {
						$type = 0;	//	Экстренный вариант на случай единственной записи для звонка
					}
				}
				
			}
			
			$this->set("day_id", $day_id);
			$this->set("ring", $ring);
			$this->set("type", $type);
			$this->set("position", $position);
			
			$speciality_id = $this->request->data('speciality_id_toSend');
			$course = $this->request->data('course_toSend');
			
			//	Загрузка информации о группах
				$this->loadModel('Group');	
						
				$options = array(
					'conditions' => array(
						'Group.course' => $course,
						'Group.speciality_id' => $speciality_id,
						),
					'order' => array('id'=>'asc')	//	Тут можно прописать сортировку групп специальности
				);
				$groups = $this->Group->find('all', $options);
			$this->set('groups', $groups);
		
		}
		
	}
	
	//	Вывод блока "Выходной"
	public function ajax_makeDayOff() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
			
			$this->set("day_id", $this->request->data('day_id'));
			$this->set("groups_count", $this->request->data('groups_count'));
				
		}
	}
	
	//	Добавление группы
	public function ajax_addGroup() {
		//	Отключение необходимости View у этого действия
		$this->autoRender = false;
		if( $this->request->is('ajax') ) {
			//	Загрузка данных
			$course = $this->request->data('course_toSend');
			$speciality_id = $this->request->data('speciality_id_toSend');
			//	Создание нового элемента
			$this->createElementToModel(array('course' => $course, 'speciality_id' => $speciality_id), 'Group');
		} else {
			$this->methodException();
		}
	}
	
	//	Удаление группы
	public function ajax_delGroup() {
		//$this->layout = 'ajax';
		//	Отключение необходимости View у этого действия
		$this->autoRender = false;
		if( $this->request->is('ajax') ) {
			//	Загрузка данных
			$group_id = $this->request->data('group_id_toSend');
			//	Поиск занятий, связанных только с этой группой
			$this->loadModel('Lesson');
			$lessons_to_delete = $this->Lesson->find('all', array(
				'conditions' => array(
					'group_id' => $group_id
				)
			));
			//	Удаление занятий, связанных только с этой группой
			if (!empty($lessons_to_delete)) {
				foreach($lessons_to_delete as $lesson) {
					$this->deleteElementFromModel($lesson['Lesson']['id'], 'Lesson');
				}
			}
			//	Поиск занятий, где упоминается эта группа
			$lessons_to_change = $this->Lesson->query("select * from lessons as Lesson where group_id like '%".$group_id."%'");
			//	Удаление упоминания об этой группе в каждом занятии
			if (!empty($lessons_to_change)) {
				foreach($lessons_to_change as $lesson) {
					$str_group_id = $lesson['Lesson']['group_id'];
					$lesson['Lesson']['group_id'] = str_replace(array($group_id.",", ",".$group_id), "", $str_group_id);
					$this->updateElementToModel($lesson['Lesson']['id'], $lesson, 'Lesson');
				}
			}
			//	Удаление самой группы
			$this->deleteElementFromModel($group_id, 'Group');
		} else {
			$this->methodException();
		}
	}

//-------------------------------------РАБОТА С РАСПИСАНИЕМ-------------------------------------//
//%$&$%&$%^%$&%$^##$^#$%%#$$#%^#%$#$%^#$%$#^%$#$^#$^%#^@%&#^@$%$^#@%$&*%!@$^%@^&!$#@&^$*%!@$@!^!//
//(^%#*(_^$_)@!*^$*^%*^^#%&^#@*&$#@&($*%@^#$%*@&#$#^@(*%@!&^#*%$#$&($@^*!&%$#%^&*&^%$#@#$%^$%^@$//
//#$%^&*&^%$%^&^*&%$&*^%$&*%$$%&*(^%&#$^&*()^*&%^(*^^&$%(#$%^$%^&$%#$%$^@%$(*@^*&!%$$#%%^&*$*@^$//

}

?>