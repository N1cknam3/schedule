<?php
	
App::uses('CakeEmail', 'Network/Email');
	
App::uses('AppController', 'Controller');	
	
class UsersController extends AppController {
	
	const ERR_ALL_FIELDS = 'Все поля обязательны для заполнения!';
	const ERR_PASS = "Пароль введен неверно!";
	const ERR_EXIST_USER = "Пользователь с таким e-mail не существует!";
	const ERR_FIELD = "Введите e-mail факультета и повторите попытку!";
	const ERR_E_MAIL = "Некорректно введен email!";
	const SUCCESS_REMINDER = "Письмо с паролем отправлено Вам на почту!";

	public $name = 'Users';
	public $helpers = array('Html', 'Session');
	public $uses = array();	
	
	// разлогинивание пользователя
	public function logout() {
		$this->Session->write('user', '');
		$this->redirectRoot();
	}			
	
	// форма авторизации
	public function login() {
		if ($this->Session->read('user') != "") {
			$this->redirect(array('controller' => 'faculties', 'action' => 'admin'));
		}
		else {
			$this->set('title_for_layout', 'Авторизация');	
		}
	}
	
	//проверка корректности email
	public function check_email($email) { 
		if(!eregi("^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+\.[a-zA-Z]{2,4}$", $email)) {
			 	return false;	
		} 		
		else {
			return true;
		}
	}	
	
	// механизм авторизации
	public function ajax_login() {
		$this->layout = 'ajax';
		
		if (!empty($this->request->data)) {
			
			$was_error_in_login = false;
			
			if (empty($this->request->data['User']['login'])) {
				$was_error_in_login = true;
				$this->set('error_enterLogin', 'true');
			}
			if (empty($this->request->data['User']['pass'])) {
				$was_error_in_login = true;
				$this->set('error_enterPass', 'true');
			} 
			
			if ($was_error_in_login) {
				$this->set('message', self::ERR_ALL_FIELDS);
			} else {
				if ($this->check_email($this->request->data['User']['login'])) {
					$this->loadModel('User');
					
					$users = $this->User->find('all', array('conditions' => array('login' => $this->request->data['User']['login'], 'pass' => $this->request->data['User']['pass'])));
					
					if (count($users) == 0) {
						$this->set("message", self::ERR_PASS);
						$this->set('error_enterPass', 'true');
					}
					else {
						$this->Session->write('user', $this->request->data['User']['login']);
						$this->set("message", 'ok');
						$this->set("url", Router::fullbaseUrl().Router::url(array('controller' => 'faculties', 'action' => 'admin')));
					}					
				}
				else {
					$this->set("message", self::ERR_E_MAIL);
					$this->set('error_enterLogin', 'true');
				}
			}
		}
	}
	
	//ф-я напоминания пароля
	public function reminder_pass() {
		$this->layout = 'ajax';
		
		if (!empty($this->request->data)) {
			if ($this->request->data['User']['login'] == ''){
				$this->set("message", self::ERR_FIELD);
			}
			else {
				if ($this->check_email($this->request->data['User']['login'])) {
					$this->loadModel('User');
					
					$users = $this->User->find('all', array('conditions' => array('login' => $this->request->data['User']['login'])));
					
					if (count($users) == 0) {
						$this->set("message", self::ERR_EXIST_USER);
					}	
					else {				
						$email = new CakeEmail();
						$email->config('gmail');
						$email->viewVars(array('value' => 'Ваш пароль: '.$users[0]['User']['pass']));
						$email->template('im_excited')
							->emailFormat('html')
							->to($this->request->data['User']['login'])
							->from('fourbases.ru@gmail.com')
							->subject('Курский государственный университет. Расписание')
							->send();
							
						$this->set("message", self::SUCCESS_REMINDER);
					}					
				}
				else {
					$this->set("message", self::ERR_E_MAIL);
				}			
			}
		}
		
	}
	
}	

?>