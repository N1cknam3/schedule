<?php
	
App::uses('CakeEmail', 'Network/Email');
	
App::uses('AppController', 'Controller');	
	
class StudentsController extends AppController {
	
	//	egserhgbg
	
	const ERR_ALL_FIELDS = 'Все поля обязательны для заполнения!';
	const ERR_PASS = "Пароль введен неверно!";
	const ERR_USERPASS = "Проверьте данные и повторите попытку";
	const ERR_EXIST_USER = "Пользователь с таким e-mail не существует!";
	const ERR_FIELD = "Введите e-mail факультета и повторите попытку!";
	const ERR_E_MAIL = "Некорректно введен email!";
	const SUCCESS_REMINDER = "Письмо с паролем отправлено Вам на почту!";
	const ERR_EXIST_GUEST = "Такой пользователь уже зарегистрирован!";
	const ERR_PASS_COMPARE = "Введенные пароли не совпадают!";
	const ERR_CAPTCH = "Неправильный ответ на вопрос";
	
	//	NOTE: в сессии поле "student" хранит id студента
	
	//	Тип первоначальной недели (0 - числитель, 1 - знаменатель)
	const START_WEEK = 1;

	public $name = 'Students';
	public $helpers = array('Html', 'Session', 'StudentCabinet');
	public $uses = array();	
	
	public $components = array(
    'MathCaptcha' => array(
        'setting' => 'value',
        'setting2' => 2));
	
	function captcha()  {
	    $this->autoRender = false;
	    $this->layout='ajax';
	    $this->Captcha->create();
	}
	
	// выбор факультета для вывода специальностей
	public function choose_faculty() {
		$this->layout = 'ajax';
		
		$this->loadModel('Faculty');
		$this->loadModel('Speciality');
		
		$faculty = $this->Faculty->find('first', array('conditions'=>array('id'=>$this->request->data['Faculty']['name'])));
		
		$specialities = $this->Speciality->find('all', array('conditions'=>array('faculty_id'=>$faculty['Faculty']['id'])), array('order'=>array('name'=>'asc')));
		$options = array();
		foreach($specialities as $spec) {
			$options[$spec['Speciality']['id']] = $spec['Speciality']['name'].'.'.$spec['Speciality']['profile'];
		}
		
		$this->set('message', $options);
	}
	
	// разлогинивание пользователя
	public function logout() {
		$this->Session->write('student', '');
		$this->redirect('/');
	}
	
	private function is_login() {
		if ($this->Session->read('student') == "")
			return false;
		else
			return true;
	}
	
	// форма авторизации
	public function login($param = null) {
		if ($this->is_login()) {
			$this->redirect(array('action' => 'cabinet'));
		} else {
			if (empty($param)) {
				$this->layout = 'student';
				$this->set('title_for_layout', 'Расписание');	
			}
			else {
				$this->layout = 'ajax';
			}	
			
			$this->set('faculties', $this->getFacultiesList());
			
			//	TODO получение типа дня недели
			/*
$days_count = $this->getStudyWeekIndex();
				$this->set('weekType', $days_count);
*/
		}
	}
	
	protected function getFacultiesList() {
		$this->loadModel('Faculty');
		return $this->Faculty->find('list', array('order'=>array('name'=>'asc')));
	}
	
	//	DOING: удалить, так как пока нигде не используется
	//	Подсчет кол-ва дней, прошедших с начала учебного года до сегодняшнего дня
	protected function getStudyDaysCount() {
		
		$now = time();
		
		$curr_month = date('m');
		$curr_year = date('Y');
		
		if ($curr_month < 9) {
			$study_start = ($curr_year - 1) . "-09-01";
		} else {
			$study_start = $curr_year . "-09-01";
		}
		$datediff = $now - strtotime($study_start);
		
		return floor($datediff/(60*60*24));
	}
	
	//	Возвращает тип текущей недели (0 - четная, 1 - нечетная)
	protected function getStydeWeekType() {
		$week_diff = $this->getStudyWeekIndex();
		$weekType = ($week_diff + self::START_WEEK) % 2;
		return $weekType;
	}
	
	//	Вывод номера текущей недели, начиная с учебного года до сегодняшнего дня
	protected function getStudyWeekIndex() {
		
		$curr_month = date('m');
		$curr_year = date('Y');
		
		//	Получение даты начала текущего учебного года
		$study_start = $curr_year;
		if ($curr_month < 9) {
			$study_start--;
		}
		$study_start .= "-09-01";
		
		//	Получение номера недели начала учебного года (с начала календарного года)
		$date1 = new DateTime($study_start);
		$studyStartWeekNumber_fromYearStart = $date1->format("W");
		
		//	Получение номера текущей недели (с начала календарного года)
		$now = date('Y-m-d');
		$date2 = new DateTime($now);
		$currWeekNumber_fromYearStart = $date2->format("W");
		
		//	Подсчет кол-ва изменения недель (переходов воскр-понед)
		$week_diff = $currWeekNumber_fromYearStart - $studyStartWeekNumber_fromYearStart;
		
		return $week_diff;
	}
	
	//	----------------------------------------------------------------------------------------------------
	//	----------------------------------------------------------------------------------------------------
	//	----------------------------------------------------------------------------------------------------
	
	//	Выбор специальности
	public function select_step2_showSpecialitiesSelectBox() {
		$this->layout = 'ajax';
		
		$specialities = $this->getSpecialities(null, 'list', $this->request->data('faculty_id'), true);
		$this->set('specialitiesArray', $specialities);
	}
	
	//	Выбор курса
	public function select_step3_showCourseSelectBox() {
		$this->layout = 'ajax';
	}
	
	//	Вывод таблицы
	public function select_step4_showTable() {
		$this->layout = 'ajax';
		
		if( $this->request->is('ajax') ) {
		
				$faculty_id = $this->request->data('faculty_id');
				$speciality_id = $this->request->data('speciality_id');
				$course = $this->request->data('course');
				
			$this->set('faculty_id', $faculty_id);
			$this->set('speciality_id', $speciality_id);
			$this->set('course', $course);
				
				//	Загрузка информации о специальности
				$this->loadModel('Speciality');
				$options = array('conditions' => array('Speciality.' . $this->Speciality->primaryKey => $speciality_id));
			$this->set('speciality', $this->Speciality->find('first', $options));
				unset($options);
				
				$groups = $this->get_Groups($speciality_id, $course);
			
			$this->set('groups', $groups);
			
				$table = null;
				
				//	Проверка на существование групп этой специальности
				if (count($groups) != 0)
				{
						//	Берем id этих групп в отдельный массив
						$groups_id_array = $this->transform_groupsArray($groups);	//	метод в AppController
						
						//	Ищем все занятия этих групп
						$lessons = $this->getLessonsIn(implode(',', $groups_id_array));	//	метод в AppController
							
						//	Сразу подставляем нужные данные в занятия
						$lessons = $this->load_SubjectsTeachers($lessons);	//	метод в AppController
						
						//	Сортировка массива занятий и приведение к виду:
						//	[day_id][ring_id][group_id] - занятия
						$table = $this->MakeTableArray($lessons, $groups_id_array);	//	метод в AppController
						
						//	Достаем массив звонков для дальнейшего вывода таблицы
						//	Достаем массив звонков для дальнейшего вывода таблицы
						$rings = $this->get_transformedRingsArray();
					$this->set('rings', $rings);
					
						//	Достаем массив дней недели для дальнейшего вывода таблицы
						$this->loadModel('Day');
						$days = $this->Day->find('all');
					$this->set('days', $days);
					
						//	TODO: вывод числителя/знаменателя
						
				}
			
			$this->set('table', $table);
			
		} else {
			$this->methodException();
		}
	}
	
	// регистрация нового пользователя для просмотра конкретного расписания и получения уведомлений
	public function registration() {
		$this->layout = 'student';
		$this->set('title_for_layout', 'Регистрация');
		
		$this->set('captcha', $this->MathCaptcha->getCaptcha());
	}
	
	//регистрация нового пользователя при помощи ajax
	public function reg_ajax() {
		$this->layout = 'ajax';
		
		if (!empty($this->request->data)) {
			$this->loadModel('Guest');
			
			$this->set('error_name', 'false');
			$this->set('error_last_name', 'false');
			$this->set('error_email', 'false');
			$this->set('error_pass', 'false');
			$this->set('error_pass2', 'false');	
			$this->set('error_captcha', 'false');		
			
			if ((empty($this->request->data['Guest']['name'])) || (empty($this->request->data['Guest']['last_name'])) || (empty($this->request->data['Guest']['email'])) || (empty($this->request->data['Guest']['pass'])) || (empty($this->request->data['Guest']['pass2']))) {
				$this->set('message', self::ERR_ALL_FIELDS);
				
				if (empty($this->request->data['Guest']['name'])) {
					$this->set('error_name', 'true');
				}
				
				if (empty($this->request->data['Guest']['last_name'])) {
					$this->set('error_last_name', 'true');
				}
				
				if (empty($this->request->data['Guest']['email'])) {
					$this->set('error_email', 'true');
				}	
				
				if (empty($this->request->data['Guest']['pass'])) {
					$this->set('error_pass', 'true');
				}
				
				if (empty($this->request->data['Guest']['pass2'])) {
					$this->set('error_pass2', 'true');
				}										
			}
			else {
				if (!$this->check_email($this->request->data['Guest']['email'])) {
					$this->set('message', self::ERR_E_MAIL);
					$this->set('error_email', 'true');
				}
				else {
					$guests = $this->Guest->find('all', array('conditions' => array('email' => $this->request->data['Guest']['email'])));
					
					if (count($guests) > 0) {
						$this->set("message", self::ERR_EXIST_GUEST);
						$this->set('error_email', 'true');
					}
					else {
						if ($this->request->data['Guest']['pass'] != $this->request->data['Guest']['pass2']) {
							$this->set("message", self::ERR_PASS_COMPARE);
							$this->set('error_pass', 'true');
							$this->set('error_pass2', 'true');
						}
						else {
							if ($this->MathCaptcha->validate($this->request->data['Guest']['captcha'])) {
								$this->set('message', 'yes');
								//	Сохраняем данные в БД
								$new_guest_id = $this->createElementToModel($this->request->data['Guest'], "Guest");
								//	Отправляем письмо с логином и паролем регистрации на почту
								//	Входим в систему с введенным логином
								$this->login_to_system($new_guest_id);
								//	Создаем единственное пустое расписание для этого пользователя
								$this->create_schedule($new_guest_id);
								//	Перенаправляем пользователя в кабинет для того, чтобы настроить необходимые параметры
								//	Перенаправление с помощью js после
								$this->set("url", Router::fullbaseUrl().Router::url(array('controller' => 'students', 'action' => 'cabinet')));
							}
							else {
								$this->set('message', self::ERR_CAPTCH);
								$this->set('error_captcha', 'true');
							}
						}
					}
				}
			}
		}
	}
	
	public function create_schedule($guest_id) {
		$data = array(
			'guest_id' => $guest_id
		);
		return $this->createElementToModel($data, 'GuestSchedule');
	}
	
	private function login_to_system($id) {
		$this->Session->write('student', $id);
	}
	
	//проверка корректности email
	public function check_email($email) { 
		if(!eregi("^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+\.[a-zA-Z]{2,4}$", $email)) {
			 	return false;	
		} 		
		else {
			return true;
		}
	}	
	
	// механизм авторизации
	public function ajax_login() {
		$this->layout = 'ajax';
		
		if (!empty($this->request->data)) {
			
			$was_error_in_login = false;
			
			if (empty($this->request->data['User']['login'])) {
				$was_error_in_login = true;
				$this->set('error_enterLogin', 'true');
			}
			if (empty($this->request->data['User']['pass'])) {
				$was_error_in_login = true;
				$this->set('error_enterPass', 'true');
			} 
			
			if ($was_error_in_login) {
				$this->set('message', self::ERR_ALL_FIELDS);
			} else {
				if ($this->check_email($this->request->data['User']['login'])) {
					$this->loadModel('Guest');
					
					$users = $this->Guest->find('first', array('conditions' => array('email' => $this->request->data['User']['login'], 'pass' => $this->request->data['User']['pass'])));
					
					if (count($users) == 0) {
						$this->set("message", self::ERR_USERPASS);
						$this->set('error_enterLogin', 'true');
						$this->set('error_enterPass', 'true');
					}
					else {
						$this->login_to_system($users['Guest']['id']);
						$this->set("message", 'yes');
						$this->set("url", Router::fullbaseUrl().Router::url(array('controller' => 'students', 'action' => 'cabinet')));
					}					
				}
				else {
					$this->set("message", self::ERR_E_MAIL);
					$this->set('error_enterLogin', 'true');
				}
			}
		}
	}
	
	//ф-я напоминания пароля
	public function reminder_pass() {
		$this->layout = 'ajax';
		
		if (!empty($this->request->data)) {
			if ($this->request->data['User']['login'] == ''){
				$this->set("message", self::ERR_FIELD);
			}
			else {
				if ($this->check_email($this->request->data['User']['login'])) {
					$this->loadModel('User');
					
					$users = $this->User->find('all', array('conditions' => array('login' => $this->request->data['User']['login'])));
					
					if (count($users) == 0) {
						$this->set("message", self::ERR_EXIST_USER);
					}	
					else {				
						$email = new CakeEmail();
						$email->config('gmail');
						$email->viewVars(array('value' => 'Ваш пароль: '.$users[0]['User']['pass']));
						$email->template('im_excited')
							->emailFormat('html')
							->to($this->request->data['User']['login'])
							->from('fourbases.ru@gmail.com')
							->subject('Курский государственный университет. Расписание')
							->send();
							
						$this->set("message", self::SUCCESS_REMINDER);
					}					
				}
				else {
					$this->set("message", self::ERR_E_MAIL);
				}			
			}
		}
	}
	
	const VK_CLIENT_ID = "5172261";
	const VK_CLIENT_SECRET = "SIS5hsNZ4QoMVTaF5H9J";
	const VK_REDIRECT_URI = "http://schedule.kursksu.ru/students/vk_login";
	const VK_REDIRECT_URI_LOCAL = "http://localhost/schedule_work/students/vk_login";
	const VK_REDIRECT_URI_SERVER = "http://schedule.kursksu.ru/students/vk_login";
	const VK_AUTHORIZE_URI = "http://oauth.vk.com/authorize";
	const VK_TOKEN_URI = "https://oauth.vk.com/access_token";
	
	private function vk_get_href() {
		$vk_params = array(
			'client_id'     => self::VK_CLIENT_ID,
			'redirect_uri'  => self::VK_REDIRECT_URI,
			'scope' => 'email',
			'response_type' => 'code'
		);
		return self::VK_AUTHORIZE_URI . '?' . urldecode(http_build_query($vk_params));
	}
	
	public function vk_login() {
		$url = $this->params['url'];
		if (!empty($url)) {
			
			if (isset($url['code'])) {
				
				$vk_params = array(
			        'client_id' => self::VK_CLIENT_ID,
			        'client_secret' => self::VK_CLIENT_SECRET,
			        'code' => $url['code'],
			        'redirect_uri' => self::VK_REDIRECT_URI
			    );
			 
				$content = @file_get_contents(self::VK_TOKEN_URI . '?' . urldecode(http_build_query($vk_params)));
			    if ($content === false) {
				    $this->redirectConnectionError();
			    } else {
				    $vk_token = json_decode($content, true);
				    
				    if (isset($vk_token['access_token'])) {
					    
						$vk_params = array(
						    'uids'         => $vk_token['user_id'],
						    'fields'       => 'uid,first_name,last_name',
						    'access_token' => $vk_token['access_token']
						);
						
						$content = @file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($vk_params)));
						
						if ($content === false) {
							$this->redirectConnectionError();
						} else {
							$vk_userInfo = json_decode($content, true);
							
							//	Предотвращение создания пользователей с одним email
							if (isset($vk_token['email'])) {
								$users = $this->getElementsFromModel('all', array('email' => $vk_token['email']), 'Guest');
								if (count($users) === 0) {
									//	Создаем нового пользователя
									$this->save_vk_user($vk_token, $vk_userInfo['response'][0]);
								} else {
									//	Обновляем существующего пользователя
									$this->updateElementToModel($users[0]['Guest']['id'], array('vk_id' => $vk_token['user_id']), 'Guest');
								}
							} else {
								$this->save_vk_user($vk_token, $vk_userInfo['response'][0]);
							}
							
							//	Получаем пользователя
							$saved_user = $this->getElementsFromModel('first', array('vk_id' => $vk_token['user_id']), 'Guest');
							//	Авторизация под этим пользователем
								//	TODO: опять же стоит делать авторизацию для разных случаев: через email или вк
							$this->login_to_system($saved_user['Guest']['id']);
							$this->redirect(array('controller' => 'students', 'action' => 'cabinet'));
						}
					} else {
						$this->redirectConnectionError();
					}
				}
				
			} else {
				$this->redirectConnectionError();
			}
			
		} else {
			$this->redirectConnectionError();
		}
		//	TODO: реализовать перенаправление на страницу ошибки
		exit;
	}
	
	private function exists_vk_user($id) {
		$result = $this->getElementsFromModel('all', array('vk_id' => $id), 'Guest');
		return empty($result);
	}
	
	private function save_vk_user($vk_response_account, $vk_response_user_info) {
		$vk_id = $vk_response_account['user_id'];
		$email = null;
		if (isset($vk_response_account['email'])) {
			$email = $vk_response_account['email'];
		}
		$first_name = $vk_response_user_info['first_name'];
		$last_name = $vk_response_user_info['last_name'];
		$this->createElementToModel(array(
			'name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'vk_id' => $vk_id
		), 'Guest');
	}
	
	const FB_CLIENT_ID = "784376215007928";
	const FB_CLIENT_SECRET = "57fec3ba894220f8299ec5a5c38744fb";
	const FB_REDIRECT_URI = "http://schedule.kursksu.ru/students/fb_login";
	const FB_REDIRECT_URI_LOCAL = "http://localhost/schedule_work/students/fb_login";
	const FB_REDIRECT_URI_SERVER = "http://schedule.kursksu.ru/students/fb_login";
	const FB_AUTHORIZE_URI = "https://www.facebook.com/dialog/oauth";
	const FB_TOKEN_URI = "https://graph.facebook.com/oauth/access_token";
	
	public function fb_get_href() {
		$params = array(
		    'client_id'     => self::FB_CLIENT_ID,
		    'redirect_uri'  => self::FB_REDIRECT_URI,
		    'scope'         => 'email',
		    'response_type' => 'code'
		);
		return self::FB_AUTHORIZE_URI . '?' . urldecode(http_build_query($params));
	}
	
	public function fb_login() {
		$url = $this->params['url'];
		if (!empty($url)) {
			
			if (isset($url['code'])) {
				
				$params = array(
			        'client_id' => self::FB_CLIENT_ID,
			        'client_secret' => self::FB_CLIENT_SECRET,
			        'code' => $url['code'],
			        'redirect_uri' => self::FB_REDIRECT_URI
			    );
			
			    $content = @file_get_contents(self::FB_TOKEN_URI . '?' . urldecode(http_build_query($params)));
			    if ($content === false) {
				    $this->redirectConnectionError();
			    } else {
				    
				    $token = null;
				    parse_str($content, $token);
				    
				    if (isset($token['access_token'])) {
					    
						$params = array(
						    'access_token' => $token['access_token'],
						    'fields' => 'id,first_name,last_name,email'
						);
						
						$content = @file_get_contents('https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params)));
						
						if ($content === false) {
							$this->redirectConnectionError();
						} else {
							
							$userInfo = json_decode($content, true);
							
							//	Предотвращение создания пользователей с одним email
							if (isset($userInfo['email'])) {
								$users = $this->getElementsFromModel('all', array('email' => $userInfo['email']), 'Guest');
								if (count($users) === 0) {
									//	Создаем нового пользователя
									$this->save_fb_user($userInfo);
								} else {
									//	Обновляем существующего пользователя
									$this->updateElementToModel($users[0]['Guest']['id'], array('fb_id' => $userInfo['id']), 'Guest');
								}
							} else {
								$this->save_fb_user($userInfo);
							}
							
							//	Получаем пользователя
							$saved_user = $this->getElementsFromModel('first', array('fb_id' => $userInfo['id']), 'Guest');
							//	Авторизация под этим пользователем
								//	TODO: опять же стоит делать авторизацию для разных случаев: через email или вк
							$this->login_to_system($saved_user['Guest']['id']);
							$this->redirect(array('controller' => 'students', 'action' => 'cabinet'));
						}
						
					} else {
						$this->redirectConnectionError();
					}
					
				};
				
			} else {
				$this->redirectConnectionError();
			}
			
		} else {
			$this->redirectConnectionError();
		}
		//	TODO: реализовать перенаправление на страницу ошибки
	}
	
	private function exists_fb_user($id) {
		$result = $this->getElementsFromModel('all', array('fb_id' => $id), 'Guest');
		return empty($result);
	}
	
	private function save_fb_user($userInfo) {
		$fb_id = $userInfo['id'];
		$email = null;
		if (isset($userInfo['email'])) {
			$email = $userInfo['email'];
		}
		$first_name = $userInfo['first_name'];
		$last_name = $userInfo['last_name'];
		$this->createElementToModel(array(
			'name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'fb_id' => $fb_id
		), 'Guest');
	}
	
	//	#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$	//
	//	#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$	//
	//	#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$	//
	//---------------КАБИНЕТ СТУДЕНТА-----------------//
	
	//задает необходимые переменные перед отрисовкой страницы
	public function beforeRender(){
		$year = $this->getLastYear();
		$this->set('year', $year['Year']['name']);
			
		$this->set('vk_href', $this->vk_get_href());
		$this->set('fb_href', $this->fb_get_href());
		
		$this->set('neededToShowVideo', false);
	}
	
	private function getCurrentUser() {
		$id = $this->Session->read('student');
		return $this->getElementsFromModel("first", array('id' => $id), "Guest");
	}
	
	private function getCurrentSchedules() {
		$user = $this->getCurrentUser();
		return $this->getElementsFromModel('all', array('guest_id' => $user['Guest']['id']), 'GuestSchedule');
	}
	
	//	Кабинет студента-пользователя
	public function cabinet() {
		if (!$this->is_login()) {
			$this->logout();
		} else {
			$this->layout = 'student-cabinet';
			$this->set('title_for_layout', "Личный кабинет");
			
			$user = $this->getCurrentUser();
			
			if ($user && !empty($user)) {
				$this->set('userName', $user['Guest']['last_name'] . " " . $user['Guest']['name']);
			}
			
			$schedules = $this->getElementsFromModel('all', array('guest_id' => $user['Guest']['id']), 'GuestSchedule');
			$schedules = $this->schedules_addNeededSelectBoxInfo($schedules);
			
			$this->set('schedules', $schedules);
			
			$this->set('faculties', $this->getFacultiesList());
			
			$this->schedule_setReadyList();
		}
	}
	
	//	Добавление всех необходимых данных для selectBox'ов расписаний
	private function schedules_addNeededSelectBoxInfo($rawSchedules) {
		if (!empty($rawSchedules)) {
			foreach($rawSchedules as $key => $schedule) {
				
				$groupsList = null;
				$specialitiesList = null;
				
				if (($schedule['GuestSchedule']['course'] !== null) && ($schedule['GuestSchedule']['speciality_id'] !== null)) {
					$groupsList = $this->getElementsFromModel(
						'list',
						array(
							'speciality_id' => $schedule['GuestSchedule']['speciality_id'],
							'course' => $schedule['GuestSchedule']['course']
						),
						'Group'
					);
				}
				
				if ($schedule['GuestSchedule']['faculty_id'] !== null) {
					$specialitiesList = $this->getSpecialities(null, 'list', $schedule['GuestSchedule']['faculty_id'], false);
				}
				
				$schedule['GuestSchedule']['is_ready'] = $this->schedule_isReady($schedule);
				$schedule['Speciality'] = $specialitiesList;
				$schedule['Group'] = $groupsList;
				$rawSchedules[$key] = $schedule;
			}
		}
		
		return $rawSchedules;
	}
	
	private function schedule_isReady($schedule) {
		return ($schedule['GuestSchedule']['faculty_id'] && $schedule['GuestSchedule']['speciality_id'] && $schedule['GuestSchedule']['course']);
	}
	
	//	Удаление расписания студента
	public function ajax_cabinet_delete_schedule() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
			$is_delete = $this->request->data('is_delete');
			$guest_schedule_id = $this->request->data('guest_schedule_id');
			
			if ($is_delete) {	
				//	Действительно удаляем
				$this->deleteElementFromModel($guest_schedule_id, 'GuestSchedule');
			} else {
				//	Сбрасываем расписание
				$this->updateElementToModel($guest_schedule_id, $this->schedules_getEmptySchedule(), 'GuestSchedule');
			}
		} else {
			$this->methodException();
		}
	}
	
	private function schedules_getEmptySchedule() {
		return array(
			'faculty_id' => null,
			'speciality_id' => null,
			'course' => null,
			'group_id' => null,
			'is_primary' => 1,
		);
	}
	
	//	Добавление нового расписания студента
	public function ajax_cabinet_add_schedule() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
			$this->set('faculties', $this->getFacultiesList());
			$user = $this->getCurrentUser();
			$new_schedule_id = $this->create_schedule($user["Guest"]["id"]);
			$new_schedule = $this->getElementsFromModel("all", array("id" => $new_schedule_id), "GuestSchedule");
			$new_schedule = $this->schedules_addNeededSelectBoxInfo($new_schedule);
			$this->set('schedules', $new_schedule);
		} else {
			$this->methodException();
		}
	}
	
	//	Добавление выбора специальности
	public function ajax_cabinet_add_schedule_speciality() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
			$faculty_id = $this->request->data('faculty_id');
			$specialities = $this->getSpecialities(null, 'list', $this->request->data('faculty_id'), true);
			$this->set('specialities', $specialities);
			
/*
			//	Обновление информации
			$schedule_id = $this->request->data('schedule_id');
			$this->updateElementToModel($schedule_id, array('faculty_id' => $faculty_id), "GuestSchedule");
*/
		} else {
			$this->methodException();
		}
	}
	
	//	Добавление выбора курса
	public function ajax_cabinet_add_schedule_course() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
/*
			$speciality_id = $this->request->data('speciality_id');
			
			//	Обновление информации
			$schedule_id = $this->request->data('schedule_id');
			$this->updateElementToModel($schedule_id, array('speciality_id' => $speciality_id), "GuestSchedule");
*/
		} else {
			$this->methodException();
		}
	}
	
	//	Добавление выбора группы
	public function ajax_cabinet_add_schedule_group() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
			$speciality_id = $this->request->data('speciality_id');
			$course = $this->request->data('course');
			$groups = $this->get_Groups($speciality_id, $course, 'list');
			$this->set('groups', $groups);
		} else {
			$this->methodException();
		}
	}
	
	//	Обновление информации о расписании
	public function ajax_cabinet_schedule_update() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
			$faculty_id = $this->request->data('faculty_id');
			$speciality_id = $this->request->data('speciality_id');
			$course = $this->request->data('course');
			$group_id = $this->request->data('group_id');
			
			$faculty_id = ($faculty_id) ? $faculty_id : null;
			$speciality_id = ($speciality_id) ? $speciality_id : null;
			$course = ($course) ? $course : null;
			$group_id = (($group_id) && ($group_id > 0)) ? $group_id : null;
			
			//	Обновление информации в БД
			$schedule_id = $this->request->data('schedule_id');
			$this->updateElementToModel($schedule_id, array('faculty_id' => $faculty_id, 'speciality_id' => $speciality_id, 'course' => $course, 'group_id' => $group_id), "GuestSchedule");
		} else {
			$this->methodException();
		}
	}
	
	public function ajax_cabinet_schedule_check() {
		$this->layout = 'ajax';
		$this->autoRender = false;
		if( $this->request->is('ajax') ) {
			$schedule_id = $this->request->data('schedule_id');
			$this->setScheduleAsPrimary($schedule_id);
		} else {
			$this->methodException();
		}
	}
	
	private function setScheduleAsPrimary($schedule_id) {
		//	сброс всех расписаний
		$schedules = $this->getCurrentSchedules();
		if (!empty($schedules)) {
			foreach($schedules as $schedule) {
				$this->updateElementToModel($schedule['GuestSchedule']['id'], array('is_primary' => 0), "GuestSchedule");
			}
		}
		//	установка выбранного в качестве основного
		$this->updateElementToModel($schedule_id, array('is_primary' => 1), "GuestSchedule");
	}
	
	public function ajax_cabinet_schedule_getStatus() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
			$is_ready = $this->request->data('is_ready');
			$this->set("is_ready", $is_ready);
		} else {
			$this->methodException();
		}
	}
	
	protected function schedule_setReadyList() {
		
		$ready_schedules = $this->schedules_getReady();
		$this->set('test', $ready_schedules);
		
		$tables_data = array();
		
		foreach($ready_schedules as $ready_schedule) {
			
			$speciality_id = $ready_schedule['GuestSchedule']['speciality_id'];
			$course = $ready_schedule['GuestSchedule']['course'];
			$group_id = $ready_schedule['GuestSchedule']['group_id'];
			
			$groups = array();
			if (!$group_id) {
				$groups = $this->get_Groups($speciality_id, $course);
				
			} else {
				$groups = $this->getElementsFromModel('all', array('id' => $group_id), 'Group');
			}
			$table = null;
			
			//	Проверка на существование групп этой специальности
			if (count($groups) != 0)
			{
					//	Берем id этих групп в отдельный массив
					$groups_id_array = $this->transform_groupsArray($groups);	//	метод в AppController
					
					//	Ищем все занятия этих групп
					$lessons = $this->getLessonsIn(implode(',', $groups_id_array));	//	метод в AppController
						
					//	Сразу подставляем нужные данные в занятия
					$lessons = $this->load_SubjectsTeachers($lessons);	//	метод в AppController
					
					//	Сортировка массива занятий и приведение к виду:
					//	[day_id][ring_id][group_id] - занятия
					$table = $this->MakeTableArray($lessons, $groups_id_array);	//	метод в AppController
					
			}
			
			$speciality = $this->getElementsFromModel('first', array('id' => $speciality_id), 'Speciality');
			
			$speciality_type = null;
			switch($speciality['Speciality']['type']) {
				case '0':
					$speciality_type = 'Бакалавриат';
					break;
				case '1':
					$speciality_type = 'Магистратура';
					break;
				case '2':
					$speciality_type = 'Специалитет';
					break;
			};
			
			$group_name = null;
			if ($group_id) {
				$group_name = $groups[0]['Group']['name'];
				if (!$group_name)
					$group_name = 'без названия';
			}
			
			$curr_data = array(
				'table' => $table,
				'groups' => $groups,
				'speciality' => $speciality,
				'speciality_type' => $speciality_type,
				'course' => $course,
				'is_primary' => $ready_schedule['GuestSchedule']['is_primary'],
				'group_name' => $group_name
			);
			array_push($tables_data, $curr_data);
	
		}
		
		$this->set('tables_data', $tables_data);
		
			//	Достаем массив звонков для дальнейшего вывода таблицы
			$rings = $this->get_transformedRingsArray();
		$this->set('rings', $rings);
		
			//	Достаем массив дней недели для дальнейшего вывода таблицы
			$this->loadModel('Day');
			$days = $this->Day->find('all');
		$this->set('days', $days);
		
	}
	
	public function ajax_cabinet_schedule_getReadyList() {
		$this->layout = 'ajax';
		if( $this->request->is('ajax') ) {
			
			$this->schedule_setReadyList();
			
		} else {
			$this->methodException();
		}
	}
	
	private function schedules_getReady() {
		$user = $this->getCurrentUser();
		
		$this->loadModel('GuestSchedule');
		$options = array(
			'conditions' => array(
				'guest_id' => $user['Guest']['id'],
				'not' => array(
					'faculty_id' => null,
					'speciality_id' => null,
					'course' => null
				)
			),
			'order' => array(
				'is_primary' => 'desc'
			)
		);
		return $this->GuestSchedule->find('all', $options);
	}
	
	//-----------конец: КАБИНЕТ СТУДЕНТА--------------//
	//	#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$	//
	//	#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$	//
	//	#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$#$%^&&(*&^$#%$%^^(*&*^$%&%%#%#*&$	//
	
}	

?>