var message_animation_effect = 'flash';
var input_animation_effect = 'shake';
	
	function animateElement1(elementName, animationName) {
		$(elementName).removeClass(animationName + " animated");
			
			$(function() {
				setTimeout(function() {
					$(elementName).addClass(animationName + " animated");
				}, 0);
			});
			
		$(elementName).on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {$(elementName).removeClass(animationName + " animated")});
	}	
	
$(document).ready(function(){
	$("#bg-overlay").height($(document).height());
});

//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	РАБОТА СО СПИСКОМ РАСПИСАНИЙ

$(document).on('click', ".speciality-add", function() {
	
	var button = $(this);
	$.ajax({
        type:"POST",

        url:"../students/ajax_cabinet_add_schedule",
        
        data:{
	        
	    },
        
        success : function(data) {
	        
	        //	Добавление строки расписания
	        button.closest(".schedule-student-row-footer").before(data);
	        //	Если есть скрытые элементы удаления, то их необходимо показать
	        button.closest(".widget-content").find(".speciality-controls").show();

        },
        error : function() {
           alert("Bad AJAX request!");
        }
    });
	
});

$(document).on('click', ".speciality-minus", function() {
	
	var button = $(this);
	var row = new NewSchedule.scheduleRow(button);
	var isDelete = 1;
	
	//	Если расписание последнее, то сбрасываем его, не удаляя
	if (NewSchedule.scheduleRow.isOne()) {
		isDelete = 0;
	}
    
    $.ajax({
        type:"POST",

        url:"../students/ajax_cabinet_delete_schedule",
        
        data:{
	        guest_schedule_id: row.id,
	        is_delete: isDelete
	    },
        
        success : function(data) {
			
			var was_checked = row.buttonCheck.isChecked();
			
			if (isDelete) {
				//	Удаляем строку расписания
				row.body.remove();
			} else {
				//	Очищаем строку расписания
				NewSchedule_clear(button);
			}
			
			if (was_checked) {
				NewSchedule.scheduleRow.getFirst().check();
			}
			
			NewSchedule.scheduleRow.getSchedulesList();

        },
        error : function() {
           alert("Bad AJAX request!");
        }
    });
	
});

$(document).on('click', ".speciality-check", function() {
	var button = $(this);
	
	var row = new NewSchedule.scheduleRow(button);
	var rowFirst = NewSchedule.scheduleRow.getFirst();
	
	var checked = row.buttonCheck.isChecked();
	
	//	Если мы нажимаем первое расписание и оно уже активно, то нет смысла переактивировать его снова
	if (row.equal(rowFirst) && checked) {
		return;
	}
	
	//	Деактивируем все нажатые кнопки
	$.each(NewSchedule.scheduleRow.getAll(), function(){
		this.buttonCheck.uncheck();
	});
	
	//	Активируем эту кнопку при активации, либо первую при деактивации
	if (!checked) {
		row.check();
	} else {
		rowFirst.check();
	}
	
});

//-------------------------
//	КЛАССЫ
/*
	======================================
	Пространство имен элементов расписания
	======================================
*/
function NewSchedule() { };

/*
	================================================
	КЛАСС - родительский объект элементов расписания
	================================================
*/
NewSchedule.object = function() { 
	this.body = null;
	this.equal = function(compared_object) {
		return this.body[0].isEqualNode(compared_object.body[0]);
	};
};

/*
	===================================
	КЛАСС - кнопка активации расписания
	===================================
*/
NewSchedule.buttonCheck = function(init) {
	
	//	Наследование
	NewSchedule.object.call(this);
	
	this.body = init;
	this.icon = this.body.find(".fa").first();

	this.isChecked = function() {
		return this.icon.hasClass(NewSchedule.buttonCheck.name_checked);
	};
	
	//	Активация кнопки
	this.check = function() {
		this.icon.removeClass(NewSchedule.buttonCheck.name_unchecked).addClass(NewSchedule.buttonCheck.name_checked).css('color', NewSchedule.buttonCheck.color_checked);
	};
	
	//	Деактивация кнопки
	this.uncheck = function() {
		this.icon.removeClass(NewSchedule.buttonCheck.name_checked).addClass(NewSchedule.buttonCheck.name_unchecked).css('color', NewSchedule.buttonCheck.color_unchecked);
	};
};
NewSchedule.buttonCheck.className = "speciality-check";
NewSchedule.buttonCheck.name_checked = "fa-star";
NewSchedule.buttonCheck.name_unchecked = "fa-star-o";
NewSchedule.buttonCheck.color_checked = "#D74E48";
NewSchedule.buttonCheck.color_unchecked = "#A5A5A5";

/*
	======================
	КЛАСС - Иконка статуса
	======================
*/
NewSchedule.status = function(init) {
	
	//	Наследование
	NewSchedule.object.call(this);
	
	this.body = init;
	this.prevStatus = init;
	
	this.setLoad = function() {
		this.prevStatus = this.body.html();
		this.body.find('.fa').removeAttr('class').attr('class', 'fa fa-spinner fa-pulse').css('color', 'grey').attr('title', 'Загрузка данных');
	};
	this.setReady = function() {
		this.body.html(this.prevStatus);
	};
};
NewSchedule.status.className = "schedule-ready";
/*
	==================
	    Константы
	==================
*/
NewSchedule.id = function(){};
NewSchedule.id.className = "guest-schedule-id";
NewSchedule.faculty_id = function(){};
NewSchedule.faculty_id.className = "guest-schedule-faculty_id";
NewSchedule.speciality_id = function(){};
NewSchedule.speciality_id.className = "guest-schedule-speciality_id";
NewSchedule.course = function(){};
NewSchedule.course.className = "guest-schedule-course";
NewSchedule.group_id = function(){};
NewSchedule.group_id.className = "guest-schedule-group_id";
NewSchedule.is_ready = function(){};
NewSchedule.is_ready.className = "schedule-ready";

/*
	==================
	КЛАСС - расписание
	==================
*/
NewSchedule.scheduleRow = function(init) {
	
	//	Наследование
	NewSchedule.object.call(this);
	
	this.body = null;
	
	//	Возможность задать с помощью вложенного элемента
	if (init.hasClass(NewSchedule.scheduleRow.className))
		this.body = init;
	else
		this.body = init.closest("."+NewSchedule.scheduleRow.className);
	
	this.buttonCheck = new NewSchedule.buttonCheck(this.body.find("."+NewSchedule.buttonCheck.className));
	this.id = this.body.find("."+NewSchedule.id.className).val();
	this.faculty_id = this.body.find("."+NewSchedule.faculty_id.className).val();
	this.speciality_id = this.body.find("."+NewSchedule.speciality_id.className).val();
	this.course = this.body.find("."+NewSchedule.course.className).val();
	this.group_id = this.body.find("."+NewSchedule.group_id.className).val();
	
	this.status = new NewSchedule.status(this.body.find("."+NewSchedule.status.className));
	
	//	Отметка расписания как основного
	this.check = function() {
		var row = this;
		$.ajax({
	        type:"POST",
	
	        url:"../students/ajax_cabinet_schedule_check",
	        
	        data:{
		        schedule_id: row.id
		    },
	        
	        success : function(data) {
		        
		    	row.buttonCheck.check();
		    	NewSchedule.scheduleRow.getSchedulesList();
	
	        },
	        error : function() {
	           alert("Bad AJAX request!");
	        }
	    });
	};
	
	//	Деактивировать
	this.deactivate = function() {
		var inputs = this.body.find('select');
		inputs.each(function(){
			$(this).attr('disabled', 'true');
		});
		this.body.find('.'+NewSchedule.is_ready.className);
		this.status.setLoad();
	};
	//	Активировать
	this.activate = function() {
		this.body.find('select').removeAttr('disabled');
// 		this.status.setReady();
		this.setStatus();
		NewSchedule.scheduleRow.getSchedulesList();
	};
	//	Проверка статуса
	this.isReady = function(){
		return ((this.body.find("."+NewSchedule.faculty_id.className).val() > 0) && (this.body.find("."+NewSchedule.speciality_id.className).val() > 0) && (this.body.find("."+NewSchedule.course.className).val() > 0));
	};
	this.setStatus = function() {
		var row = this;
		var ready = this.isReady();
		ready = (ready) ? 1 : 0;
		$.ajax({
	        type:"POST",
	
	        url:"../students/ajax_cabinet_schedule_getStatus",
	        
	        data:{
		        is_ready: ready
		    },
	        
	        success : function(data) {
		        //	Обновление
		    	row.status.body.replaceWith(data);
	        },
	        error : function() {
	           alert("Bad AJAX request!");
	        }
	    });
	};
};
NewSchedule.scheduleRow.className = "schedule-student-row";
//	Получение первого расписания
NewSchedule.scheduleRow.getFirst = function() {
	return new NewSchedule.scheduleRow($(document).find("."+NewSchedule.widget.className).find("."+NewSchedule.scheduleRow.className).first());
};
//	Получение всех расписаний
NewSchedule.scheduleRow.getAll = function() {
	var bodies = NewSchedule.scheduleRow.getAllBodies();
	var mass_res = [];
	bodies.each(function(){
		mass_res.push(new NewSchedule.scheduleRow($(this)));
	});
	return mass_res;
};
//	Получение jquery-объектов всех расписаний (всех body)
NewSchedule.scheduleRow.getAllBodies = function() {
	return $(document).find("."+NewSchedule.widget.className).find("."+NewSchedule.scheduleRow.className);
};
//	Проверка на последнее расписание
NewSchedule.scheduleRow.isOne = function() {
	return (NewSchedule.scheduleRow.getAllBodies().length === 1);
};
NewSchedule.scheduleRow.getSchedulesList = function() {
	//	Обновление списка расписаний
	
	$.ajax({
        type:"POST",

        url:"../students/ajax_cabinet_schedule_getReadyList",
        
        success : function(data) {
	        //	Обновление
	    	$(document).find('.student-schedule-list').html(data);
        },
        
        error : function() {
           alert("Bad AJAX request! can't get schedule list.");
        }
    });
};

/*
	=================================
	КЛАСС - контейнер всех расписаний
	=================================
*/
NewSchedule.widget = function(init) {
	
	//	Наследование
	NewSchedule.object.call(this);
	
	this.body = null;
	
	//	Возможность задать с помощью вложенного элемента
	if (initRow.hasClass(NewSchedule.schedules.className))
		this.body = init;
	else
		this.body = init.closest("."+NewSchedule.schedules.className);
};
NewSchedule.widget.className = "widget-content";

//	конец: КЛАССЫ
//-------------------------

function NewSchedule_add_speciality(changed_facultySelect){
	
	var row = new NewSchedule.scheduleRow($(changed_facultySelect));
	
	$.ajax({
        type:"POST",

        url:"../students/ajax_cabinet_add_schedule_speciality",
        
        data:{
	        faculty_id: row.faculty_id
	    },
	    
	    beforeSend: function() {
		    row.deactivate();
	    },
        
        success : function(data) {
	        
	        //	Очистка
			NewSchedule_clear(changed_facultySelect, 'f');
			NewSchedule_save(changed_facultySelect);
	        
	        //	Корень этой строки расписания
			var content_box = $(changed_facultySelect).closest(".schedule-student-row").find(".schedule-student-speciality-box");
			content_box.html(data);
			
// 			row.activate();

        },
        error : function() {
           alert("Bad AJAX request!");
//            row.activate();
        }
    });
}

function NewSchedule_add_course(changed_specialitySelect){
	
	var row = new NewSchedule.scheduleRow($(changed_specialitySelect));
	
	$.ajax({
        type:"POST",

        url:"../students/ajax_cabinet_add_schedule_course",
        
        data:{
	        
	    },
	    
	    beforeSend: function() {
		    row.deactivate();
	    },
        
        success : function(data) {
	        
	        //	Очистка
			NewSchedule_clear(changed_specialitySelect, 's');
			NewSchedule_save(changed_specialitySelect);
	        
	        //	Корень этой строки расписания
			var content_box = $(changed_specialitySelect).closest(".schedule-student-row").find(".schedule-student-course-box");
			content_box.html(data);
			
// 			row.activate();

        },
        error : function() {
           alert("Bad AJAX request!");
//            row.activate();
        }
    });
}

function NewSchedule_add_group(changed_courseSelect, speciality_id, course){
	
	var row = new NewSchedule.scheduleRow($(changed_courseSelect));
	
	$.ajax({
        type:"POST",

        url:"../students/ajax_cabinet_add_schedule_group",
        
        data:{
	        // schedule_id: row.id,
	        speciality_id: row.speciality_id,
	        course: row.course
	    },
	    
	    beforeSend: function() {
		    row.deactivate();
	    },
        
        success : function(data) {
	        
	        //	Очистка
			NewSchedule_clear(changed_courseSelect, 'c');
			NewSchedule_save(changed_courseSelect);
	        
	        //	Корень этой строки расписания
			var content_box = $(changed_courseSelect).closest(".schedule-student-row").find(".schedule-student-group-box");
			content_box.html(data);
			
// 			row.activate();

        },
        error : function() {
           alert("Bad AJAX request!");
//            row.activate();
        }
    });
}

function NewSchedule_clear(changedSelect, type) {
	/*
		type = f => меняется факультет
		type = s => меняется специальность
		type = c => меняется курс
	*/
	
	var row = new NewSchedule.scheduleRow($(changedSelect));
	//	Отдельный случай для удаления последнего расписания (не передается параметр type)
	type = (type) ? type : null;
	
	switch (type) {
		case null:
			row.body.find("."+NewSchedule.faculty_id.className).val('-1');
		case 'f':
			row.body.find(".schedule-student-speciality-box").html('');
		case 's':
			row.body.find(".schedule-student-course-box").html('');
		case 'c':
			row.body.find(".schedule-student-group-box").html('');
		default:
			break;
	};
}

function NewSchedule_updateGroups(changedSelect) {
	var row = new NewSchedule.scheduleRow($(changedSelect));
	row.deactivate();
	NewSchedule_save(changedSelect);
	
}

function NewSchedule_save(changedSelect) {
	
	var row = new NewSchedule.scheduleRow($(changedSelect));
	
	f_sel = (row.faculty_id) ? row.faculty_id : 0;
	s_sel = (row.speciality_id) ? row.speciality_id : 0;
	c_sel = (row.course) ? row.course : 0;
	g_sel = (row.group_id) ? row.group_id : 0;
	
	$.ajax({
        type:"POST",

        url:"../students/ajax_cabinet_schedule_update",
        
        data:{
	        schedule_id: row.id,
	        faculty_id: f_sel,
	        speciality_id: s_sel,
	        course: c_sel,
	        group_id: g_sel
	    },
        
        success : function(data) {
			row.activate();
        },
        error : function() {
           alert("Bad AJAX request!");
        }
    });
}

//	конец: РАБОТА СО СПИСКОМ РАСПИСАНИЙ
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@

//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	РАБОТА С ВКЛАДКАМИ

$(document).on('click', ".title-button", function() {
	
	//	Получение номера текущей активной вкладки
	var activeButton = $(document).find('.title-button.active').first();
	var actID = substrTillSpace(activeButton.attr('class'), "student-cabinet-tab_");
	
	var buttons = $(document).find('.title-button');
	//	Убираем текущую активную кнопку
	buttons.each(function(){
		$(this).removeClass("active");
	});
	//	Делаем эту активной
	$(this).addClass("active");
	
	//	Получение номера вкладки
	var nextID = substrTillSpace($(this).attr('class'), "student-cabinet-tab_");
	
	//	Переход к контенту этой вкладки
	$(document).find('.student-cabinet-content_'+actID).hide();
	$(document).find('.student-cabinet-content_'+nextID).show();
});

function substrTillSpace(target, string) {
	var start = target.indexOf(string) + string.length;
	var str = target.substr(start);
	var end = str.indexOf(" ");
	return str.slice(0, end);
}

function loadReadySchedules() {
	$.ajax({
        type:"POST",

        url:"../students/ajax_cabinet_schedule_getReadyList",
        
        data:{
	        
	    },
        
        success : function(data) {

        },
        
        error : function() {
           alert("Bad AJAX request!");
        }
    });
}

/*
$(document).on('click', ".title-button", function() {
	var id =  $(this).attr('class');
	id = id.split('-');
	$("#menu-container .content").hide();
	$("#menu-container #menu-"+id[1]).addClass("animated fadeInDown").show();
	return false;
});
*/
/*
$(document).on('click', ".schedule-table-list-row", function() {
	var table = $(this).next();
	
	if (table.is(':visible')) {
		
		table.find('.schedule-table-list-row-content').slideToggle(700);
		
		$(function() {
		 	setTimeout(function() {
		 		table.css('display', 'none');
			}, 700);
		});	
	} else {
		table.css('display', 'table-row');
		table.find('.schedule-table-list-row-content').slideToggle(500);
	}
});
*/

$(document).on('click', ".schedule-list", function() {
	var table = $(this).closest(".schedule-list-container").find(".schedule-list-content");
	table.slideToggle(500);
});

function setHeiHeight() {
    $('#hei').css({
        height: $(window).height() + 'px'
    });
}
setHeiHeight(); // устанавливаем высоту окна при первой загрузке страницы
$(window).resize( setHeiHeight ); // обновляем при изменении размеров окна

//	конец: РАБОТА С ВКЛАДКАМИ
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@
//	#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@#$^&%*(&(^%$#&(^(%*%^$%^*&^%$*#$%(^&%$&#%^@%^&^(*%$#@