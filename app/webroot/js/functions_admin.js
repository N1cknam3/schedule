//------------------------------------------------------------------------------------------------------------------------------------

	//************************************************************************//
	//*************МЕТОДЫ ОПРЕДЕЛЕНИЯ И НАЗНАЧЕНИЯ НАЖАТЫХ КНОПОК*************//
	
	function dropDownMenu() {
		
		if ($('#btn-main').hasClass("open"))
		{
			$('#btn-main').removeClass("open");		
			$('#btn-main').addClass("");
			
			$('#nav').css('display', 'none');
		}
		else
		{
			$('#btn-main').addClass("open");
			
			$('#nav').css('display', 'block');
		}
	}
	
	function dropdownButton() {
		if ($('#dropdownButton1').hasClass("subdrop"))
		{
			$('#dropdownButton1').removeClass("subdrop");		
			$('#dropdownButton1').addClass("");
			
			$('#dropdownIcon1').removeClass("icon-chevron-down");
			$('#dropdownIcon1').addClass("icon-chevron-right");
			$('#dropdownList11').slideUp();
		}
		else
		{
			$('#dropdownButton1').addClass("subdrop");		
			
			$('#dropdownIcon1').removeClass("icon-chevron-right");
			$('#dropdownIcon1').addClass("icon-chevron-down");
			$('#dropdownList11').slideDown();
		}
	}
	
	//*************МЕТОДЫ ОПРЕДЕЛЕНИЯ И НАЗНАЧЕНИЯ НАЖАТЫХ КНОПОК*************//
	//************************************************************************//

//------------------------------------------------------------------------------------------------------------------------------------

	//********************************************************//
	//*************ФОРМА ДОБАВЛЕНИЯ СПЕЦИАЛЬНОСТИ*************//
	
	$(document).click(function(e)
	{    
	    var subject2 = $("#myModalAdd");     
	    var container = $(document).find("body");
	    
	    if ((subject2.is(':visible')) && (container).hasClass('admin'))
	    {
	        if(!subject2.has(e.target).length)
	        {
	            closeAddForm();            
	        }
	    }   
	});
	
	function speciality_add_show(typeOfSpeciality) {
		$('.modal-title').text('Добавление специальности');	
		
		$('#addSpecialityCode').val('');
		$('#addSpecialityName').val('');
		$('#addSpecialityProfile').val('');
		$('#addSpecialityType').val(typeOfSpeciality);
		
		showAddForm();
	}
	
	function ajax_speciality_add_check() {
		
		//показываем блок статуса добавления
		$('#status_add').css('display', 'block');
		//назначаем тексту сообщения об ошибке соответствующий текст, передаваемый через скрытое поле		
		$('#message_add').text($('#hidden_message_add').text());
		
		if ($('#hidden_message_add').text() !== 'ok') {			
			animateElement('#message_add', message_animation_effect);				
			$('#message_add').css('display', 'inline-block');
			
/*
			if ($('#hidden_where_was_error').text() == 'addSpecialityCode') {
				animateElement('#addSpecialityCode', 'shake');
			} else if ($('#hidden_where_was_error').text() == 'addSpecialityName') {
				animateElement('#addSpecialityName', 'shake');
			} else if ($('#hidden_where_was_error').text() == 'addSpecialityProfile') {
				animateElement('#addSpecialityProfile', 'shake');
			}
*/
			if ($('#hidden_error_addSpecialityCode').text() === 'true') {
				animateElement('#addSpecialityCode', input_animation_effect);
			}
			if ($('#hidden_error_addSpecialityName').text() === 'true') {
				animateElement('#addSpecialityName', input_animation_effect);
			}
			if ($('#hidden_error_addSpecialityProfile').text() === 'true') {
				animateElement('#addSpecialityProfile', input_animation_effect);
			}
			
		} else {
			$('#status_add').css('display', 'none');
			$('#message_add').css('display', 'none');
			closeAddForm();
		}
	}
	
	function closeAddForm() {
		animateElement('#SpecialityFormAdd', 'fadeOutUp');
		animateElement('#myModalAdd', 'fadeOut');
		$('#myModalAdd').slideUp();
		
		$('#status_add').css('display', 'none');
		$('#message_add').css('display', 'none');
	}
	
	function showAddForm() {
		$('#myModalAdd').slideDown();
		animateElement('#myModalAdd', 'fadeIn');
		animateElement('#SpecialityFormAdd', 'fadeInDown');	
	}
	
	//*************ФОРМА ДОБАВЛЕНИЯ СПЕЦИАЛЬНОСТИ*************//
	//********************************************************//

//------------------------------------------------------------------------------------------------------------------------------------

	//*******************************************************//
	//*************ФОРМА ИЗМЕНЕНИЯ СПЕЦИАЛЬНОСТИ*************//

	$(document).click(function(e)
	{
		var subject1 = $("#myModalEdit"); 	
		var container = $(document).find("body");
	    
	    if ((subject1.is(':visible')) && (container).hasClass('admin'))
	    {
	        if(!subject1.has(e.target).length)
	        {
	            closeEditForm();	            
	        }
	    }  
	});
	
	function speciality_edit_show(elem_id, typeOfSpeciality) {	
		$('.modal-title').text('Изменение специальности');
		
		$('#editSpecialityCode').val($('#code-'+elem_id).text());
		$('#editSpecialityName').val($('#name-'+elem_id).text());
		$('#editSpecialityProfile').val($('#profile-'+elem_id).text());
		$('#FacultyId').val(elem_id);
		$('#editSpecialityType').val(typeOfSpeciality);
		
		showEditForm();
	}
	
	function ajax_speciality_edit_check() {
		
		//показываем блок статуса добавления
		$('#status_edit').css('display', 'block');
		//назначаем тексту сообщения об ошибке соответствующий текст, передаваемый через скрытое поле		
		$('#message_edit').text($('#hidden_message_edit').text());
		
		if ($('#hidden_message_edit').text() !== 'ok') {			
			animateElement('#message_edit', message_animation_effect);	
			$('#message_edit').css('display', 'inline-block');
			
/*			//вариант для одной движущейся ячейки воода
			if ($('#hidden_where_was_error').text() == 'editSpecialityCode') {
				animateElement('#editSpecialityCode', 'shake');
			} else if ($('#hidden_where_was_error').text() == 'editSpecialityName') {
				animateElement('#editSpecialityName', 'shake');
			} else if ($('#hidden_where_was_error').text() == 'editSpecialityProfile') {
				animateElement('#editSpecialityProfile', 'shake');
			}
*/
			if ($('#hidden_error_editSpecialityCode').text() === 'true') {
				animateElement('#editSpecialityCode', input_animation_effect);
			}
			if ($('#hidden_error_editSpecialityName').text() === 'true') {
				animateElement('#editSpecialityName', input_animation_effect);
			}
			if ($('#hidden_error_editSpecialityProfile').text() === 'true') {
				animateElement('#editSpecialityProfile', input_animation_effect);
			}
			
		} else {
			$('#status_edit').css('display', 'none');
			$('#message_edit').css('display', 'none');
			closeEditForm();
		}
	}
	
	function closeEditForm() {
		animateElement('#SpecialityFormEdit', 'fadeOutUp');
		animateElement('#myModalEdit', 'fadeOut');
		$('#myModalEdit').slideUp();
		
		$('#status_edit').css('display', 'none');
		$('#message_edit').css('display', 'none');
	}
	
	function showEditForm() {
		$('#myModalEdit').slideDown();
		animateElement('#myModalEdit', 'fadeIn');
		animateElement('#SpecialityFormEdit', 'fadeInDown');	
	}
	
	//*************ФОРМА ИЗМЕНЕНИЯ СПЕЦИАЛЬНОСТИ*************//
	//*******************************************************//
	
//------------------------------------------------------------------------------------------------------------------------------------

	//****************************************//
	//*************ФОРМА УДАЛЕНИЯ*************//
	
	function confirmDelete(text) {
		text = typeof text !== 'undefined' ? " "+text : "";	// (условие) ? (значение_если_да) : (значение_если_нет)
		
		if (confirm("Вы точно хотите удалить" + text + "?")) {
			return true;
		} else {
			return false;
		}
	}
	
	//*************ФОРМА УДАЛЕНИЯ*************//
	//****************************************//
	
//------------------------------------------------------------------------------------------------------------------------------------

	//**************************************************//
	//*************ФОРМА ДОБАВЛЕНИЯ КАФЕДРЫ*************//
	
	$(document).click(function(e)
	{    
	    var subject4 = $("#myModalChairAdd");     
	    var container = $(document).find("body");
	    
	    if ((subject4.is(':visible')) && (container).hasClass('admin'))
	    {
	        if(!subject4.has(e.target).length)
	        {
	            closeChairAddForm();            
	        }
	    }   
	});
	
	function chair_add_show(typeOfSpeciality) {
		$('.modal-title').text('Добавление кафедры');	
		
		$('#addChairName').val('');
		
		showChairAddForm();
	}
	
	function ajax_chair_add_check() {
		
		//показываем блок статуса добавления
		$('#status_chair_add').css('display', 'block');
		//назначаем тексту сообщения об ошибке соответствующий текст, передаваемый через скрытое поле		
		$('#message_chair_add').text($('#hidden_message_chair_add').text());
		
		if ($('#hidden_message_chair_add').text() !== 'ok') {			
			animateElement('#message_chair_add', message_animation_effect);				
			$('#message_chair_add').css('display', 'inline-block');
			
			if ($('#hidden_error_addChairName').text() === 'true') {
				animateElement('#addChairName', input_animation_effect);
			}
			
		} else {
			$('#status_chair_add').css('display', 'none');
			$('#message_chair_add').css('display', 'none');
			closeChairAddForm();
		}
	}
	
	function closeChairAddForm() {
		animateElement('#ChairAddForm', 'fadeOutUp');
		animateElement('#myModalChairAdd', 'fadeOut');
		$('#myModalChairAdd').slideUp();
		
		$('#status_chair_add').css('display', 'none');
		$('#message_chair_add').css('display', 'none');
	}
	
	function showChairAddForm() {
		$('#myModalChairAdd').slideDown();
		animateElement('#myModalChairAdd', 'fadeIn');
		animateElement('#ChairAddForm', 'fadeInDown');	
	}
	
	//*************ФОРМА ДОБАВЛЕНИЯ КАФЕДРЫ*************//
	//**************************************************//
	
//------------------------------------------------------------------------------------------------------------------------------------

	//*************************************************//
	//*************ФОРМА ИЗМЕНЕНИЯ КАФЕДРЫ*************//

	$(document).click(function(e)
	{
		var subject5 = $("#myModalChairEdit"); 	
		var container = $(document).find("body");
	    
	    if ((subject5.is(':visible')) && (container).hasClass('admin'))
	    {
	        if(!subject5.has(e.target).length)
	        {
	            closeChairEditForm();	            
	        }
	    }  
	});
	
	function chair_edit_show(elem_id) {	
		$('.modal-title').text('Изменение кафедры');
		
		$('#editChairName').val($('#name-'+elem_id).text());
		$('#ChairId').val(elem_id);
		
		showChairEditForm();
	}
	
	function ajax_chair_edit_check() {
		
		//показываем блок статуса добавления
		$('#status_chair_edit').css('display', 'block');
		//назначаем тексту сообщения об ошибке соответствующий текст, передаваемый через скрытое поле		
		$('#message_chair_edit').text($('#hidden_message_chair_edit').text());
		
		if ($('#hidden_message_chair_edit').text() !== 'ok') {			
			animateElement('#message_chair_edit', message_animation_effect);	
			$('#message_chair_edit').css('display', 'inline-block');

			if ($('#hidden_error_editChairName').text() === 'true') {
				animateElement('#editChairName', input_animation_effect);
			}
			
		} else {
			$('#status_chair_edit').css('display', 'none');
			$('#message_chair_edit').css('display', 'none');
			closeChairEditForm();
		}
	}
	
	function closeChairEditForm() {
		animateElement('#ChairEditForm', 'fadeOutUp');
		animateElement('#myModalChairEdit', 'fadeOut');
		$('#myModalChairEdit').slideUp();
		
		$('#status_chair_edit').css('display', 'none');
		$('#message_chair_edit').css('display', 'none');
	}
	
	function showChairEditForm() {
		$('#myModalChairEdit').slideDown();
		animateElement('#myModalChairEdit', 'fadeIn');
		animateElement('#ChairEditForm', 'fadeInDown');	
	}
	
	//*************ФОРМА ИЗМЕНЕНИЯ КАФЕДРЫ*************//
	//*************************************************//
	
//------------------------------------------------------------------------------------------------------------------------------------

	//**********************************************//
	//*************РАБОТА С РАСПИСАНИЕМ*************//
	
	//	--=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	//	АВТОПОДСТАНОВКА
	$(document).on( "keyup", 'input.active', function() {
		
		var action = "timeTable";	//	по-умолчанию перезагрузка страницы расписания
		
		if ($(this).hasClass("subject")) {
			action = "ajax_getSubjects";
		} else if ($(this).hasClass("name")) {
			action = "ajax_getTeachers";
		}

		$.ajax({
			type: "POST",
			url: "../faculties/"+action,
			
			data: {
				keyword: $(this).val()
			},
			
			beforeSend: function(){
				//	Анимация загрузки
				//$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
			},
			
			success: function(data){
				var box = $("input.active").parent().find("div.suggestion-box");
				box.show();
				box.html(data);
				$("input.active").css("background","#FFF");
			}
		});

	});
	
	//	выбор подходящего элемента из списка автоподстановки
	$(document).on( "mousedown", 'li.suggestion_box_item', function() {
		//	li <- ul <- div.suggestion-box <- div.subject -> input.subject
		$(this).parent().parent().parent().find("input").val( $(this).html() );
		
		$(this).parent().parent().parent().find("span").text( $(this).html() );
		//	li <- ul <- div.suggestion-box
		$(this).parent().parent().hide();
	});
	//	конец: АВТОПОДСТАНОВКА
	//	--=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	//	Очистка и скрытие области таблицы
	function schedule_ajax_reload_clearAll() {
		//$('#ajax_reload_step1_ChooseSpeciality').text('');
		$('#ajax_reload_step2_GetTable').text('');
		schedule_ajax_hideTable();
	}
	
	//	Выборка и вывод необходимых данных через AJAX при изменении специальности
	//	Отображение таблицы выбранного курса и специальности
	function speciality_Selected() {
		
		$.ajax({
	        type:"POST",
	
	        url:"../faculties/schedule_step2_showTable",
	        
	        beforeSend: function(){
		        //	Анимация загрузки
				//$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
			},
	        
	        data:{
		        speciality_id_toSend: $('#speciality_SelectBox').val(),
		        course_toSend: $('#course_SelectBox').val()
		    },
	        
	        success : function(data) {
	           
	           //	UPDATE TARGET
	           $('#ajax_reload_step2_GetTable').html(data);
	           
	           //	Показ таблицы
	           schedule_ajax_showTable();
	           
	           $(".schedule-table-button").width($("table.schedule-table").width() - 48);
	
	        },
	        error : function(data) {
	           alert("Bad AJAX request!");
	        }
	    });
    
	}
	
	//	Отображение таблицы занятий
	function schedule_ajax_showTable() {	
		$('#schedule_ajax_table').css('display', 'block');
	}
	
	//	Скрытие таблицы занятий
	function schedule_ajax_hideTable() {	
		$('#schedule_ajax_table').css('display', 'none');
	}
	
	//	Javascript вызов сохранения данных таблицы
	function saveTable() {
		
		var form = $(document).find("#ScheduleTableForm");
			
		$.ajax({
	        type:"POST",
	
	        url: "../faculties/set_schedule_table",
	        
	        beforeSend: function(){
		        //	Анимация загрузки
				//$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
			},
	        
	        data: $(form).serialize(),
	        
	        /*
			xhr: function(){
				
		        var xhr = $.ajaxSettings.xhr(); // получаем объект XMLHttpRequest
		        xhr.upload.addEventListener('progress', function(evt){ // добавляем обработчик события progress (onprogress)
		          if(evt.lengthComputable) { // если известно количество байт
		            // высчитываем процент загруженного
		            var percentComplete = Math.ceil(evt.loaded / evt.total);
		            // устанавливаем значение в атрибут value тега <progress>
		            // и это же значение альтернативным текстом для браузеров, не поддерживающих <progress>
		            //progressBar.val(percentComplete).text('Загружено ' + percentComplete + '%');
		          }
		        }, false);
		        return xhr;
		        
	    	},
			*/
	        
	        success : function(data) {
	        	
	        	$("#table-result").text( data );
	        	alert("Сохранено");
	        	
	        },
	        
	        error : function() {
	        	alert("Bad AJAX request!");
	        }
	    });
		
	}
	
	//ТРАНСФОРМАЦИЯ ТЕКСТ -> ПОЛЕ ВВОДА
	$(document).on( "click", 'span.subject, span.name, span.group-name', function() {
		
		//Скрываем текст предмета
		$(this).css('display', 'none');
		
		//Поиск поля ввода
		var input = $(this).next();	//получение формы ввода
		
		//Проверка на ввод
		//Заметки: строки ниже должно быть идентичны строкам, присвоенным по умолчанию в
		//WidgetHelper::ScheduleTableCellSubject, WidgetHelper::ScheduleTableCellTeacherName и WidgetHelper::ScheduleTableCellGroupName соответственно
		if ((this.innerHTML === "Нет предмета") || (this.innerHTML === "Нет преподавателя") || (this.innerHTML === "Название группы")) {
			
		} else {
			//Присваиваем новое значение полю ввода
			input.val(this.innerHTML);	//присвоение значения текста
		}
		
		//Показываем поле ввода
		input.css('display', 'block');	//отображение формы ввода
		
		//Выделяем текст в поле ввода
		var strLength = input.val().length * 2;	//умножение в два раза из-за особенностей браузера Opera
		input.focus();
		input[0].setSelectionRange(0, strLength);	//выделение всего текста формы ввода
		
		//Присваиваем класс активности этому полю ввода для распознавания покидания курсором поля ввода (см далее)
		input.addClass("active");

	});
	
	//ТРАНСФОРМАЦИЯ ПОЛЕ ВВОДА -> ТЕКСТ
		//	проверка на введенное значение
	$(document).on( "blur", 'input.active', function() {
		
		//Получение текста предмета, соответствующего полю ввода
		var text = $(this).prev();	//получение текста
		
		//	изменение стиля текста на полупрозрачный (заранее)
		text.addClass("unactive");
		
		//Проверка на правильность ввода
		if (($(this).hasClass("subject")) && ($(this).val() === '')) {
			//	НЕ ВВЕДЕНО НАЗВАНИЕ ПРЕДМЕТА
			
			//	скрываем поле имени преподавателя и назначаем ему пустые значения
			hideTeacherName($(this));
			//	сброс названия предмета
			text.text("Нет предмета");
			
			//	очистка поля "name" предмета в значение по-умолчанию ("пусто")
			$(this).attr("name", changeGroupNum($(this).attr("name"), "no"));
			
			//	Изменение поля "name" id занятия
			var lessonID = $(this).closest("td").find("input.id");
			lessonID.attr("name", changeGroupNum(lessonID.attr("name"), "no"));
			
			//	изменение стиля текста названия предмета
			addUnactivePadding($(this).parent());
			
		} else if (($(this).hasClass("name")) && ($(this).val() === '')) {
			//	НЕ ВВЕДЕНО ИМЯ ПРЕПОДАВАТЕЛЯ
			
			//	сброс имени преподавателя
			text.text("Нет преподавателя");
			
		} else if (($(this).hasClass("group-name")) && ($(this).val() === '')) {
			//	НЕ ВВЕДЕНО НАЗВАНИЕ ГРУППЫ
			
			//	сброс имени преподавателя
			text.text("Название группы");
			
		} else {
			//	ВВЕДЕНО ПРАВИЛЬНО
						
			//	Присваиваем новое значение тексту
			text.text($(this).val());
			//	изменение стиля текста на обычный
			removeUnactivePadding($(this).parent());
			text.removeClass("unactive");
			
			//	Сохранение введенного значения в БД
			//	для того, чтобы при дальнейшем вводе выводился этот вариант
			if ($(this).hasClass("name"))
				saveTeacherName($(this).val());
			else if ($(this).hasClass("subject"))
				saveSubjectName($(this).val());
			
			//	особые действия для поля предмета
			if ($(this).hasClass("subject")) {
				
				if ($(this).hasClass("global")) {
					//	NO -> ALL
					
					//	Изменение поля "name" input'у названия предмета
					$(this).closest("td").find("input.subject").attr("name", changeGroupNum($(this).attr("name"), "all"));
					//	Изменение поля "name" input'у имени преподавателя
					var inputName = $(this).closest("td").find("input.name");
					inputName.attr("name", changeGroupNum(inputName.attr("name"), "all"));
					//	Изменение поля "name" id занятия
					var lessonID = $(this).closest("td").find("input.id");
					lessonID.attr("name", changeGroupNum(lessonID.attr("name"), "all"));
					
				} else {
					//	NO -> №группы
					
					//	Получение id соответствующей группы из строки заголовка групп
					var groupColIndex = $(this).closest('td.subject')[0].cellIndex - 2; //	Чтобы индексация шла с 1
					var neededGroupId = $(document).find("table.schedule-table").find("th.subject")[groupColIndex].getAttribute("group");
					//	Изменение поля "name" input'у названия предмета
					$(this).closest("td").find("input.subject").attr("name", changeGroupNum($(this).attr("name"), neededGroupId));
					//	Изменение поля "name" input'у имени преподавателя
					var inputName = $(this).closest("td").find("input.name");
					inputName.attr("name", changeGroupNum(inputName.attr("name"), neededGroupId));
					//	Изменение поля "name" id занятия
					var lessonID = $(this).closest("td").find("input.id");
					lessonID.attr("name", changeGroupNum(lessonID.attr("name"), neededGroupId));
					
				}
				//	Появление поля имени преподавателя и установка его значений и полей	
				showTeacherName($(this));
			}
		}
		//	конец: Проверка на правильность ввода
		
		//Скрываем поле ввода
		$(this).css('display', 'none');
		
		//	Скрываем меню автозаполнения
		var box = $(this).parent().find(".suggestion-box");
		//box.text("");
		box.hide();
		
		//Показываем текст предмета
		text.css('display', 'inline');	//отображение текста
		
		//Очищаем класс активности этого поля ввода
		$(this).removeClass("active");

	});
	
	function saveSubjectName(text) {
		$.ajax({
	        type:"POST",
	
	        url: "../faculties/ajax_autocomplete_subject_save",
	        
	        data: { text: text }
	    });
	}
	
	function saveTeacherName(text) {
		$.ajax({
	        type:"POST",
	
	        url: "../faculties/ajax_autocomplete_teacher_save",
	        
	        data: { text: text }
	    });
	}
	
	//	добавляет объекту классы "unactive" и "padding"
	//	используется в div.subject, чтобы применить стиль текста
	function addUnactivePadding(object)
	{
		object.addClass("unactive");
		object.addClass("padding");
	}
	
	//	убирает у объекта классы "unactive" и "padding"
	//	используется в div.subject, чтобы очистить стиль текста
	function removeUnactivePadding(object)
	{
		object.removeClass("unactive");
		object.removeClass("padding");
	}
	
	//	Скрытие поле имени преподавателя и очистка текста на "пустое" = "Нет преподавателя"
	//		object: input.active
	//		путь: input <- div.subject <- td -> div.name = show
	function showTeacherName(object, groupNum)
	{
		object.parent().parent().find("div.name").css("display", "inline");
	}
	
	//	Скрытие поле имени преподавателя и очистка текста на "пустое" = "Нет преподавателя"
	//		object: input.active
	//		путь: input <- div.subject <- td -> div.name = hide
	function hideTeacherName(object)
	{
		var teacherNameBlock = object.closest("td").find("div.name");
		teacherNameBlock.css("display", "none");
		
		//	очистка видимого текста
		var text = teacherNameBlock.find("span.name");
		text.text("Нет преподавателя");
		//	очистка стиля видимого текста
		text.addClass("unactive");
			
		//	очистка поля ввода
		var input = teacherNameBlock.find("input");
		input.val('');
		//	установление значения аттрибута "name" по-умолчанию ("пустое")
		input.attr("name", changeGroupNum(input.attr("name"), "no"));
	}
	
	//-------------------------------------------------------
	//УПРАВЛЕНИЕ ВИДИМОСТЬЮ ЭЛЕМЕНТОВ УПРАВЛЕНИЯ
	//ПРЕДМЕТЫ
		//ПОЯВЛЕНИЕ
		$(document).on( "mouseenter", 'td.subject', function() {
			
			var combine = $(this).find(".table-subject-combine");
			var del = $(this).find(".table-subject-delete");
			var plus = $(this).find(".table-subject-plus");
			var minus = $(this).find(".table-subject-minus");
				
			combine.stop(true, true).fadeIn();
			del.stop(true, true).fadeIn();
			plus.stop(true, true).fadeIn();
			minus.stop(true, true).fadeIn();
	
		});
		//СКРЫТИЕ
		$(document).on( "mouseleave", 'td.subject', function() {
			
			var combine = $(this).find(".table-subject-combine");
			var del = $(this).find(".table-subject-delete");
			var plus = $(this).find(".table-subject-plus");
			var minus = $(this).find(".table-subject-minus");
			
			combine.fadeOut();
			del.fadeOut();
			plus.fadeOut();
			minus.fadeOut();
	
		});
		
	//ВРЕМЯ
		//ПОЯВЛЕНИЕ
		$(document).on( "mouseenter", 'td.time', function() {
			
			var plus = $(this).find(".table-time-plus");
			var minus = $(this).find(".table-time-minus");
				
			plus.stop(true, true).fadeIn();
			minus.stop(true, true).fadeIn();
	
		});
		//СКРЫТИЕ
		$(document).on( "mouseleave", 'td.time', function() {
			
			var plus = $(this).find(".table-time-plus");
			var minus = $(this).find(".table-time-minus");
			
			plus.fadeOut();
			minus.fadeOut();
	
		});
		
	//ГРУППЫ
		//ПОЯВЛЕНИЕ
		$(document).on( "mouseenter", 'th.subject', function() {
			
			var close = $(this).find(".table-group-close");
				
			close.stop(true, true).fadeIn();
	
		});
		//СКРЫТИЕ
		$(document).on( "mouseleave", 'th.subject', function() {
			
			var close = $(this).find(".table-group-close");
				
			close.fadeOut();
	
		});
		//конец: УПРАВЛЕНИЕ ВИДИМОСТЬЮ ЭЛЕМЕНТОВ УПРАВЛЕНИЯ
		//-------------------------------------------------------
		
	//-------------------------------------------------------
	//  КНОПКИ ПРЕДМЕТА
		//КНОПКА ДОБАВЛЕНИЯ ПРЕДМЕТА ПО ЗНАМЕНАТЕЛЮ
		$(document).on( "click", 'div.table-subject-plus', function() {
			
			//показываем блок предмета "по знаменателю"
			var subject_down = $(this).parent().find(".table-subject-down");
			subject_down.fadeIn();
			
			//меняем назначение этой кнопки на "добавление"
			$(this).removeClass("table-subject-plus");
			$(this).addClass("table-subject-minus");
			$(this).find("i").removeClass("fa-plus-square-o");
			$(this).find("i").addClass("fa-minus-square-o");
	
		});
		
		//КНОПКА УДАЛЕНИЯ ПРЕДМЕТА ПО ЗНАМЕНАТЕЛЮ
		$(document).on( "click", 'div.table-subject-minus', function() {
			
			//скрываем блок предмета "по знаменателю"
			var subject_down = $(this).parent().find(".table-subject-down");
			subject_down.fadeOut();
			
			//меняем назначение этой кнопки на "удаление"
			$(this).removeClass("table-subject-minus");
			$(this).addClass("table-subject-plus");
			$(this).find("i").removeClass("fa-minus-square-o");
			$(this).find("i").addClass("fa-plus-square-o");
			
			//очищаем содержание видимого текста
			subject_down.find("span.subject").text("Нет предмета");
			subject_down.find("span.name").text("Нет преподавателя");
			
			//изменение стиля видимого текста на неактивное
			addUnactivePadding(subject_down.find("div.subject"));
			
			//скрытие имени преподавателя
			hideTeacherName(subject_down.find("input"));
			
			var inputs = subject_down.find("input");
			inputs.each(function(){
				//очистка атрибутов "name" у полей ввода
				$(this).attr("name", changeGroupNum($(this).attr("name"), "no"));
				$(this).val("");
			});
	
		});
	
		//КНОПКА ОБЪЕДИНЕНИЯ ПРЕДМЕТОВ ГРУПП
		$(document).on( "click", 'div.table-subject-combine', function() {
			
			//	"нажимаем" кнопку
			$(this).addClass("pressed");
					
			//	устанавливаем ширину на все столбцы групп
			var curr = $(this).parent();	//	текущий td.subject
			var subjects = curr.parent().find("td.subject");	//	все ячейки занятий этой строки
			curr.attr("colspan", subjects.length);
			
			//	Для текущего занятия ставим  №группы = all
				var groupColIndex = $(this).parent()[0].cellIndex - 2;
				var neededGroupId = $(document).find("table.schedule-table").find("th.subject")[groupColIndex].getAttribute("group");
				
				var lesson_up = curr.find('.table-subject-up div.subject');
				var lesson_down = curr.find('.table-subject-down div.subject');
				
				//	Если занятие знаменателя активно, то мы переназначаем ему значение
				if (!lesson_up.hasClass('unactive')) {
					var input_subject = lesson_up.find("input.subject");
					var input_name = lesson_up.parent().find("input.name");
					var input_id = lesson_up.parent().find("input.id");
					input_subject.attr("name", changeGroupNum(input_subject.attr("name"), "all"));
					input_name.attr("name", changeGroupNum(input_name.attr("name"), "all"));
					input_id.attr("name", changeGroupNum(input_id.attr("name"), "all"));
				}
				
				//	Если занятие знаменателя активно, то мы переназначаем ему значение	
				if (!lesson_down.hasClass('unactive')) {
					var input_subject = lesson_down.find("input.subject");
					var input_name = lesson_down.parent().find("input.name");
					var input_id = lesson_down.parent().find("input.id");
					input_subject.attr("name", changeGroupNum(input_subject.attr("name"), "all"));
					input_name.attr("name", changeGroupNum(input_name.attr("name"), "all"));
					input_id.attr("name", changeGroupNum(input_id.attr("name"), "all"));
				}
			
			//	скрываем остальные предметы этого времени
			//	И изменяем номера групп на "no"		
				subjects.each(function(){
					if (!$(this).is(curr))
					{
						$(this).css("display", "none");
						
						var curr_input_subject = $(this).find("input.subject");
						var curr_input_name = $(this).find("input.name");
						var curr_input_id = $(this).find("input.id");
						curr_input_subject.attr("name", changeGroupNum(curr_input_subject.attr("name"), "no"));
						curr_input_name.attr("name", changeGroupNum(curr_input_name.attr("name"), "no"));
						curr_input_id.attr("name", changeGroupNum(curr_input_id.attr("name"), "no"));
					
					}
				});
			
			//	Удаление идентификатора общего предмета
			$(this).parent().find("input").addClass("global");
			
		});
		
		//КНОПКА РАЗЪЕДИНЕНИЯ ПРЕДМЕТОВ ГРУПП
		$(document).on( "click", 'div.table-subject-combine.pressed', function() {
			
			//	"отжимаем" кнопку
			$(this).removeClass("pressed");
			//	устанавливаем ширину на один столбец
			$(this).parent().attr("colspan", "1");
			//	показываем остальные столбцы
			$(this).parent().parent().find("td.subject").fadeIn();
			
			//	Удаление идентификатора общего предмета
			$(this).parent().find("input").removeClass("global");
			
			//	Ищем все занятия этой строки таблицы
			var curr = $(this).parent();
			var subjects = curr.parent().find("td.subject");
			
			//	Действия для остальных занятий строки
			//	Изменяем номер группы
			subjects.each(function(){
				
				var curr_groupColIndex = $(this)[0].cellIndex - 2;
				var curr_neededGroupId = $(document).find("table.schedule-table").find("th.subject")[curr_groupColIndex].getAttribute("group");
				
				var curr_lesson_up = $(this).find('.table-subject-up div.subject');
				var curr_lesson_down = $(this).find('.table-subject-down div.subject');
				
				//	Если занятие знаменателя активно, то мы переназначаем ему значение
				if (!curr_lesson_up.hasClass('unactive')) {
					var curr_input_subject = curr_lesson_up.find("input.subject");
					var curr_input_name = curr_lesson_up.parent().find("input.name");
					var curr_input_id = curr_lesson_up.parent().find("input.id");
					curr_input_subject.attr("name", changeGroupNum(curr_input_subject.attr("name"), curr_neededGroupId));
					curr_input_name.attr("name", changeGroupNum(curr_input_name.attr("name"), curr_neededGroupId));
					curr_input_id.attr("name", changeGroupNum(curr_input_id.attr("name"), curr_neededGroupId));
				}
				
				//	Если занятие знаменателя активно, то мы переназначаем ему значение	
				if (!curr_lesson_down.hasClass('unactive')) {
					var curr_input_subject = curr_lesson_down.find("input.subject");
					var curr_input_name = curr_lesson_down.parent().find("input.name");
					var curr_input_id = curr_lesson_down.parent().find("input.id");
					curr_input_subject.attr("name", changeGroupNum(curr_input_subject.attr("name"), curr_neededGroupId));
					curr_input_name.attr("name", changeGroupNum(curr_input_name.attr("name"), curr_neededGroupId));
					curr_input_id.attr("name", changeGroupNum(curr_input_id.attr("name"), curr_neededGroupId));
				}
				
			});
	
		});
		
		//	Возвращает №группы в строке string (должна содержать "[g-№группы]")
		function getGroupNum(string) {
			
			var startIndex = string.indexOf("[g-") + 3;
			var num = "";
			while (string.charAt(startIndex) !== ']') {
				num = num+string.charAt(startIndex);
				startIndex++;
			}
			return num;
		}
		
		//	Изменяет №группы в строке string (должна содержать "[g-№группы]")
		function changeGroupNum(string, insertString) {
			
			var toChange = "[g-"+getGroupNum(string)+"]";
			insertString = "[g-"+insertString+"]";
			return string.replace(toChange, insertString);
		}
		//  конец: КНОПКИ ПРЕДМЕТА
		//-------------------------------------------------------
	
	//-------------------------------------------------------
	//	КНОПКИ ВРЕМЕНИ
	
		//	КНОПКА ДОБАВЛЕНИЯ "ЗВОНКА"
		$(document).on( "click", 'div.table-time-plus', function() {
			
			//	true если новый звонок добавляется ДО
			var ringPasteBefore = true;
			
			//	Получение нового id звонка и запоминание старого (для определения возможностей кнопок управления временем нового звонка)
			var prevTimeID = $(this).parent().attr("id").replace("time-", "");
			var insertTimeID = prevTimeID;
			
			//	Определение первый/последний звонок был нажат
			if ($(this).hasClass("top") || $(this).hasClass("top-one")) {
				insertTimeID--;
			} else if ($(this).hasClass("bottom") || $(this).hasClass("bottom-one")) {
				insertTimeID++;
				ringPasteBefore = false;
			}
			
			//	Запоминаем место, где выводить AJAX данные
			var target = $(this).closest("tr");
			//	Запоминаем нажатую кнопку для удаления кнопок управления временем этого звонка
			var thatButton = $(this);
			
			//	Получение id текущего дня
			var day_id = target.attr("class").replace("day-", "");
			
			//	Получение position для кнопок управления временем нового звонка
			var position = "";
			if (ringPasteBefore) {
				position = "top";
			}
			else {
				position = "bottom";
			}
			
			//	Проверка количества строк текущего дня
			var rings_count = target.parent().find("."+target.attr("class")).length - 1;	//	Считается именно кол-во звонков
			//	Если кол-во равно 2 (один - для заголовка дня недели и один - для занятий), то
			//	необходимо ИЗМЕНИТЬ элементы управления текущего звонка, а не удалить
			//	Изменение слудующим образом: удаляется нажатая кнопка +, затем изменяются классы оставшихся кнопок
			
			$.ajax({
		        type:"POST",
		
		        url:"../faculties/ajax_getRing",
		        
		        data:{
			        day_id: day_id,
			        nextTime: insertTimeID,
			        prevTime: prevTimeID,
			        position: position,
			        speciality_id_toSend: $('#speciality_SelectBox').val(),
			        course_toSend: $('#course_SelectBox').val()
			    },
		        
		        success : function(data) {
		           
					//	UPDATE TARGET
					if (ringPasteBefore) {
						target.before(data);
					}
					else {
						target.after(data);
					}
					
					if (thatButton.hasClass("one")) {
						//	Был "Выходной"
						//	Идет добавление первого звонка
						//	Необходимо удалить строку "Выходной"
						thatButton.closest("tr").remove();
					} else {
						//	Увеличиваем rowspan у заглавия этого дня недели
						dayCellRowspan_inc(target);
						
						//	Удаление кнопок управления этого звонка
						if (rings_count > 1)
							deleteTimeButtons(thatButton);
						else {
							var newPos = "";
							if (ringPasteBefore)
								//	Новый звонок добавляется ДО, а значит
								//	кнопки управления текущего дня будут внизу
								newPos = "bottom";
							else
								//	Новый звонок добавляется ПОСЛЕ, а значит
								//	кнопки управления текущего дня будут вверху
								newPos = "top";
							changeTimeButtons(thatButton, newPos);
						}
					}
		
		        },
		        error : function() {
		           alert("Bad AJAX request!");
		        }
		    });
			
		});
		
		//	КНОПКА УДАЛЕНИЯ "ЗВОНКА"
		$(document).on( "click", 'div.table-time-minus', function() {
			
			//	Поиск текущей строки
			var target = $(this).closest("tr");
			//	Кнопки этого "звонка"
			var currButtons = $(this).parent().find("div");
			
			//	Проверка количества строк текущего дня
			var rings_count = target.parent().find("."+target.attr("class")).length - 1;	//	Считается именно кол-во звонков
			//	Если кол-во равно 2 (один - для заголовка дня недели и один - для занятий), то
			//	необходимо переназначить этот день на "выходной"
			if (rings_count === 1) {
				
				var groups_count = target.find("td.subject").length;
				var day_id = target.attr("class").replace("day-", "");
				
				$.ajax({
			        type:"POST",
			
			        url:"../faculties/ajax_makeDayOff",
			        
			        data:{
				        day_id: day_id,
				        groups_count: groups_count
				    },
			        
			        success : function(data) {
				        
				        var day_header = target.parent().find("tr."+target.attr("class")+" td.day-header").parent();
				        day_header.after(data);
				        target.remove();
			
			        },
			        error : function() {
			           alert("Bad AJAX request!");
			        }
			    });
				
			} else {
			
				//	Кнопки звонка, предшествующего этому "звонку"
				var prevButtons;
				var prevButtonsArea;
				
				//	Запоминаем положение кнопок, в которое превратятся предыдущие
				var newPosition = ($(currButtons[0]).hasClass("top")) ? "top" : "bottom";
				
				//	Определение первый/последний "звонок" был нажат
				if ($(this).hasClass("top")) {
					prevButtonsArea = target.next().find("td.time");
					prevButtons = prevButtonsArea.find("div");
				} else if ($(this).hasClass("bottom") || $(this).hasClass("middle-one")) {
					prevButtonsArea = target.prev().find("td.time");
					prevButtons = prevButtonsArea.find("div");
				} else {
					return;
				}
				
				if (currButtons.length === 1) {
					if (prevButtons.length !== 0) {	//	Станет три
						//	[-][- +] -> [+ - +]
						//	Удаление предыдущих кнопок
						deleteTimeButtons($(prevButtons[0]));
						//	Создание [+ - +]
						createTimeButtons_three(prevButtonsArea);
					} else {
						//	[-][ ] -> [+ -]
						//	Создание [+ -] или [- +] в зависимости от положения текущих кнопок
						createTimeButtons_two(prevButtonsArea, newPosition);
					}
				} else {
					if (prevButtons.length !== 2) {	//	Замена
						//	[+ -][-] -> [+ -] || [+ -][ ] -> [+ -]
						//	Удаление предыдущих кнопок
						deleteTimeButtons($(prevButtons[0]));
						//	Перемещение текущих кнопок
						currButtons.each(function(){
							$(this).appendTo(prevButtonsArea);
							$(this).hide();	//	скрываем кнопки
						});
					} else {	//	Станет три
						//	[+ -][- +] -> [+ - +]
						//	Удаление предыдущих кнопок
						deleteTimeButtons($(prevButtons[0]));
						//	Создание [+ - +]
						createTimeButtons_three(prevButtonsArea);
					}
				}
				
				//	Уменьшаем rowspan у заглавия этого дня недели
		        dayCellRowspan_dec(target);
		        
		        target.remove();
	        
	        }
			
		});
		
		//	Создает кнопки времени [+ - +] в контейнере container
		function createTimeButtons_three(container) {
			container.html(container.html()+'<div class="col-md-6 table-time-plus top-one" style="display: none;"><i class="icon-plus"></i></div>');
			container.html(container.html()+'<div class="col-md-6 table-time-minus middle-one" style="display: none;"><i class="icon-minus"></i></div>');
			container.html(container.html()+'<div class="col-md-6 table-time-plus bottom-one" style="display: none;"><i class="icon-plus"></i></div>');
		}
		
		//	Создает кнопки времени [+ -] или [+ -] в зависимости от position в контейнере container
		function createTimeButtons_two(container, position) {
			container.html(container.html()+'<div class="col-md-6 table-time-plus '+position+'" style="display: none;"><i class="icon-plus"></i></div>');
			container.html(container.html()+'<div class="col-md-6 table-time-minus '+position+'" style="display: none;"><i class="icon-minus"></i></div>');
	
		}
		
		//	Уменьшаем rowspan у заглавия дня недели
		//	tr - текущая нажатая строка занятий дня недели (строка "звонка")
		function dayCellRowspan_inc(tr) {
			var day_header = tr.parent().find("tr."+tr.attr("class")+" td.day-header");
	        var rowspan = Number( day_header.attr("rowspan") ) + 1;
	        day_header.attr("rowspan", rowspan);
		}
		
		//	Уменьшаем rowspan у заглавия дня недели
		//	tr - текущая нажатая строка занятий дня недели (строка "звонка")
		function dayCellRowspan_dec(tr) {
			var day_header = tr.parent().find("tr."+tr.attr("class")+" td.day-header");
	        var rowspan = Number( day_header.attr("rowspan") ) - 1;
	        day_header.attr("rowspan", rowspan);
		}
		
		//	удаляет кнопки, находящиеся рядом с нажатой buttonObject
		function deleteTimeButtons(buttonObject) {
			buttonObject.parent().find("div").remove();
		}
		
		//	изменяет кнопки времени, находящиеся рядом с нажатой buttonObject
		//	удаляет текущую кнопку
		//	newPosition - новая позиция кнопок управления в звонках этого дня: "top"/"bottom"
		function changeTimeButtons(buttonObject, newPosition) {
			var buttons = buttonObject.parent();
			//	Удаление нажатой кнопки
			buttonObject.remove();
			//	Поиск остальных кнопок
			buttons = buttons.find("div");
			//	Просмотр кол-ва оставшихся кнопок
			if (buttons.length === 1) {
				//	Значит было: [- +] или [+ -]
				//	Меняем названия классов "top" и "bottom" на будущее их положение и добавляем в конец "one"
				buttons.each(function(){
					$(this).removeClass("top");
					$(this).removeClass("bottom");
					$(this).addClass(newPosition);
					$(this).addClass("one");
					$(this).hide();	//	Скрываем старые кнопки, чтобы избежать их видимость
				});
				
			} else if (buttons.length === 2) {
				//	Значит было: [+ - +]
				//	Меняем названия классов "top-one", "middle-one" и "bottom-one" на "top"/"bottom" в зависимости от будущего их положения
				buttons.each(function(){
					$(this).removeClass("top-one");
					$(this).removeClass("middle-one");
					$(this).removeClass("bottom-one");
					$(this).addClass(newPosition);
					$(this).hide();	//	Скрываем старые кнопки, чтобы избежать их видимость
				});
			}
		}
		//	конец: КНОПКИ ДОБАВЛЕНИЯ/УДАЛЕНИЯ ЗВОНКОВ
		//-------------------------------------------------------
		
	//-------------------------------------------------------
	//	КНОПКИ ГРУПП
	
		//	КНОПКА ДОБАВЛЕНИЯ ГРУППЫ
		//	добавление группы с пустым именем
		$(document).on( "click", 'th.add', function() {
			
			if (confirm('Добавление новой группы приведет к потере несохраненных данных.\nДобавить группу?')) {
				addGroup();
			}
			
		})
		
		//	Добавление новой группы в БД
		function addGroup() {
			$.ajax({
		        type:"POST",
		
		        url:"../faculties/ajax_addGroup",
		        
		        data:{
			        speciality_id_toSend: $('#speciality_SelectBox').val(),
					course_toSend: $('#course_SelectBox').val()
			    },
			    
			    beforeSend: function(){
			        //	Анимация загрузки
					//$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
				},
		        
		        success : function(data) {
			        
			        //	Перезагрузка таблицы
			        speciality_Selected();
		
		        },
		        error : function() {
		           alert("Bad AJAX request!");
		        }
		    });
		}
		
		//	КНОПКА УДАЛЕНИЯ ГРУППЫ
		//	удаляет группу и связанные с ней занятия
		$(document).on( "click", '.table-group-close', function() {
			
			if (confirm('Вы действительно хотите удалить эту группу?\nВсе связанные с этой группой занятия так же будут удалены')) {
				$.ajax({
			        type:"POST",
			
			        url:"../faculties/ajax_delGroup",
			        
			        data:{
				        group_id_toSend: $(this).parent().attr("group")
				    },
				    
				    beforeSend: function(){
				        //	Анимация загрузки
						//$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
					},
			        
			        success : function(data) {
				        
				        //	Перезагрузка таблицы
						speciality_Selected();
			
			        },
			        error : function() {
			           alert("Bad AJAX request!");
			        }
			    });
		    }
			
		})
	
		//	конец: КНОПКИ ГРУПП
		//-------------------------------------------------------
	
	//*************РАБОТА С РАСПИСАНИЕМ*************//
	//**********************************************//	

//------------------------------------------------------------------------------------------------------------------------------------

	//************************************************//
	//*************ВСПОМОГАТЕЛЬНЫЕ МЕТОДЫ*************//
	
	function animateElement(elementName, animationName) {
		
		$(function() {
			setTimeout(function() {
				$(elementName).addClass(animationName + " animated");
			}, 0);
		});
			
		$(function() {
			setTimeout(function() {
				$(elementName).removeClass(animationName + " animated")
			}, 1000);
		});

		// $(elementName).addClass(animationName + " animated").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
		// 	function () {
		// 		$(this).removeClass(animationName + " animated");
		// 	}
		// );
	}
	
	function animateThisIn(elementName, displayStyle, animationName) {
		//анимация
		$(elementName).css('opacity', '0');
		$(elementName).css('display', displayStyle);	
		
		$(elementName).addClass(animationName + " animated").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
			function () {
				$(this).removeClass(animationName + " animated");
				$(this).css('opacity', '1');
			}
		);
	}
	
	function animateThisOut(elementName, animationName) {
		//анимация
		$(elementName).addClass(animationName + " animated").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
			function () {
				$(this).removeClass(animationName + " animated");
				$(elementName).css('display', 'none');
			}
		);
	}
	
	//Подключение маски к элементу
	jQuery(function($){
	   $(".FacultyCodeInput").mask("99.99.99");
	});
	
	//Правка бага №1 - "При скрытии бокового меню (малая ширина окна)
	//и обратном расширении (большая ширина окна) скрывается боковое меню"
	window.onresize = function() {  
	    if (document.body.clientWidth >= 768) {
		    $("#btn-main").removeClass("open");
		    $("#nav").css('display', 'block');
	    } else {
		    $("#nav").css('display', 'none');
	    }
	};
	
	//*************ВСПОМОГАТЕЛЬНЫЕ МЕТОДЫ*************//
	//************************************************//