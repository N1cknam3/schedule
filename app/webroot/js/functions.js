var message_animation_effect = 'flash';
var input_animation_effect = 'shake';
	
	//*************************************//
	//*************ФОРМА ВХОДА*************//
	
	function ajax_action() {
		if ($('#message').text() != 'ok') {
			$('#status').css('display', 'block');
			animateElement1('#message', message_animation_effect);
			
			if ($('#hidden_error_enterLogin').text() == 'true') {
				animateElement1('#enterLogin', input_animation_effect);
			}
			if ($('#hidden_error_enterPass').text() == 'true') {
				animateElement1('#enterPass', input_animation_effect);
			}
			
		}
		else {
			$('#status').css('display', 'none');
			window.location.href = $('#route').text();
		}
	}
	
	function animateElement1(elementName, animationName) {
		$(elementName).removeClass(animationName + " animated");
			
			$(function() {
				setTimeout(function() {
					$(elementName).addClass(animationName + " animated");
				}, 0);
			});
			
		$(elementName).on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {$(elementName).removeClass(animationName + " animated")});
	}	
	
	//*************ФОРМА ВХОДА*************//
	//*************************************//
	
	function ajax_registration() {
		var answer = true;
		
		if ($('#hidden_error_name').text() == 'true') {
			animateElement1('#NameReg', input_animation_effect);
			answer = false;
		}
		
		if ($('#hidden_error_last_name').text() == 'true') {
			animateElement1('#LastNameReg', input_animation_effect);
			answer = false;
		}
		
		if ($('#hidden_error_email').text() == 'true') {
			animateElement1('#EmailReg', input_animation_effect);
			answer = false;
		}		
		
		if ($('#hidden_error_pass').text() == 'true') {
			animateElement1('#PassReg', input_animation_effect);
			answer = false;
		}		
		
		if ($('#hidden_error_pass2').text() == 'true') {
			animateElement1('#Pass2Reg', input_animation_effect);
			answer = false;
		}	
		
		if ($('#hidden_error_captcha').text() == 'true') {
			animateElement1('#GuestCaptcha', input_animation_effect);
			answer = false;
		}
		
		return answer;
	}
	
	function ajax_login() {
		var answer = true;
		
		if ($('#hidden_error_enterLogin').text() == 'true') {
			animateElement1('#UserLogin', input_animation_effect);
			answer = false;
		}
		
		if ($('#hidden_error_enterPass').text() == 'true') {
			animateElement1('#UserPass', input_animation_effect);
			answer = false;
		}
		
		return answer;
	}
    
    function formRegistration_success() {
    	if (!ajax_registration()) {
        	$('#status_reg').show();
			animateElement1('#message_reg', message_animation_effect);
    	} else {
        	$('#status_reg').hide();
        	window.location.href = $('#route_reg').text();
    	}
    }
    
    function formLogin_success() {
    	if (!ajax_login()) {
        	$('#status_login').show();
			animateElement1('#message_login', message_animation_effect);
    	} else {
        	$('#status_login').hide();
			window.location.href = $('#route_login').text();
    	}
    }


//------------------------------------------------------------------------------------------------------------------------------------

	//*****************************************************//
	//*************ФОРМА ВОССТАНОВЛЕНИЯ ПАРОЛЯ*************//
	
	$(document).click(function(e)
	{
		var subject0 = $("#myModal");	
		var container = $(document).find("body");
	    
	    if ((subject0.is(':visible')) && (container).hasClass('login'))
	    {
	        if(!subject0.has(e.target).length)
	        {
	            closePassForm();
	        }
	    }  
	});
	
	function showPassMessage() {
		$('#status1').fadeIn();
		animateElement1('#message1', message_animation_effect);
		animateElement1('#UserLoginToFix', input_animation_effect);
	}
	
	function showPassForm() {
		$('#myModal').slideDown();
		animateElement1('#myModal', 'fadeIn');
		animateElement1('#PassForm', 'fadeInDown');
	}
	
	function closePassForm() {
		animateElement1('#PassForm', 'fadeOutUp');
		animateElement1('#myModal', 'fadeOut');
		$('#myModal').slideUp();
	}
	
	//*************ФОРМА ВОССТАНОВЛЕНИЯ ПАРОЛЯ*************//
	//*****************************************************//

//------------------------------------------------------------------------------------------------------------------------------------

	//********************************************//
	//*************ВХОД ДЛЯ СТУДЕНТОВ*************//
	
	$(document).click(function(e)
	{
		var subject = $("#login_box");
		var showable = subject.find(".showable");
	    //	Проверка через свойство css visibility у дочернего элемента из-за того, что элемент все равно is(':visible')
	    if (showable.css('visibility') !== 'hidden')
	    {
	        if(!subject.has(e.target).length)
	        {
	            subject.addClass("unfocused");
	            $(function() {
		            setTimeout(function() {
						subject.removeClass("unfocused focused");
					}, 500)
				});
	        } else {
			    if(subject.has(e.target).length)
		        {
		            subject.addClass("focused");
		        }
		    }
	    };
	});
	
	function show_login() {
		$('#login_box').addClass("focused");
	}
	
	function hide_login() {
		if(!$('input').is(':focus')) {
			//	Проверка для предотвращения скрытия элемента при выборе автоподстановки
	       $('#login_box').stop(true, true).fadeOut('slow');
	    }
	}
	
	//	ДЕЙСТВИЯ С SELECTBOX'АМИ
		//	Скрываем опции самого первого selectbox'а при загрузке
		$(document).ready(function(){
			$(this).find(".cs-select").find(".cs-options").hide();
		});
		
		//	При выборе любого selectbox'а показываем опции
		$(document).on( "click", '.cs-select', function() {
			$(this).find(".cs-options").show();
		});
		
		//	При закрытии любого selectbox'а скрываем опции
		$(document).on( "blur", '.cs-select', function() {
			$(this).find(".cs-options").hide();
			$("#fullname").html('');
		});
	//	конец: ДЕЙСТВИЯ С SELECTBOX'АМИ
	
	//	ДЕЙСТВИЯ С РАЗМЕРОМ ФОНА COLORBOX'А
		//	При изменении размера экрана изменяем высоту фонов
		$( window ).resize(function() {
			//var targetHeight = Math.max($("#colorbox").height(), $(window).height());
			//$("#cboxOverlay").height(targetHeight);
			//$("#bg-overlay").height(targetHeight);
			//	Из-за изменений размера и поведения colorbox'а необходимо возвращать пользователя на начало страницы,
			//	чтобы избежать появления пробелов на фоне
			//$(document).scrollTop(0);
		});
		
		//	При завершении colorbox'а вызывается изменение размера фона как самого colorbox'а, так и фона страницы
		//	введено из-за возможно большой высоты содержимого colorbox'а (таблица может оказаться велика)
		$(document).on( "cbox_complete", function() {
			var targetHeight = Math.max($("#colorbox").height(), $(window).height());
			if (targetHeight !== $("#cboxOverlay").height()) {
				$("#cboxOverlay").height(targetHeight);
				$("#bg-overlay").height(targetHeight);
			}
		});
		
		//	При закрытии colorbox'а вызывается изменение размера фона на прежнее
		$(document).on( "cbox_closed", function() {
			$("#bg-overlay").height("100%");
		});
	//	конец: ДЕЙСТВИЯ С РАЗМЕРОМ ФОНА COLORBOX'А
	
	/*
		Факультеты -> Специальности + Курс
	*/
	function step2_showSpecialities() {
		$("#fullname").html('');
		
		//	Очистка предыдущей таблицы
		clear_Table();
		
		//	Загрузка и отображение списка специальностей
		$("#specialitiesContainer").hide();
		
		$.ajax({
			type: "POST",
			
			url: "students/select_step2_showSpecialitiesSelectBox",
			
			data: {
				faculty_id: $('#select_FacultyId').val()
			},
			
			success: function(data){
				$("#specialitiesContainer").html(data);
				$("#specialitiesContainer").slideDown();
				$(function() {
					setTimeout(function() {
						$("#specialitiesContainer").css('overflow', '');
					}, 1000)
				});
				
				$(document).find(".cs-select").find(".cs-options").hide();
			},
	        error : function(data) {
	           alert("Bad AJAX request!");
	        }
		});
		
		//	Загрузка и отображение списка курсов
		$("#courseContainer").hide();
		
		$.ajax({
	        type:"POST",
	
	        url:"students/select_step3_showCourseSelectBox",
	        
	        success : function(data) {
		        
		        //	UPDATE TARGET
				$("#courseContainer").html(data);
				$("#courseContainer").slideDown();
				$(function() {
					setTimeout(function() {
						$("#courseContainer").css('overflow', '');
					}, 1000)
				});
				
				$(document).find(".cs-select").find(".cs-options").hide();
	
	        },
	        error : function(data) {
	           alert("Bad AJAX request!");
	        }
	    });		
	}
	
	/*
		Курс + Специальность -> Вывод таблицы
	*/
	function step4_showTable() {
		
		$("#fullname").html('');
		
		var faculty_id = $('#select_FacultyId').val();
		var speciality_id = $('#select_SpecialityId').val();
		var course = $('#select_Course').val();
		
		if ( (faculty_id) && (speciality_id) && (course) ) {
		
			$("#tableContainer").hide();
			
			$.ajax({
		        type:"POST",
		
		        url:"students/select_step4_showTable",
		        
		        data: {
					faculty_id: faculty_id,
					speciality_id: speciality_id,
					course: course
				},
		        
		        success : function(data) {
			        
			        //	UPDATE TARGET
					//	Запуск всплывающего окна - colorBox
					$.colorbox({
						html:data
					});
					
		        },
		        error : function(data) {
		           alert("Bad AJAX request!");
		        }
		    });
		} else {
			clear_Table();
		}
	}
	
/*
	//	ЭФФЕКТ
	$(document).ready(function(){
		$('body').hide();
		$(function() {
			setTimeout(function() {
				$('body').fadeIn(1500);
			}, 0);
		});
	});
*/
	
	function clear_Table() {
		$("#tableContainer").hide();
		$("#tableContainer").html('');
	}
	
	//*************ВХОД ДЛЯ СТУДЕНТОВ*************//
	//********************************************//

// вывод подсказок при наведении на специальность	
$(document).on( "mousemove", '#specialitiesContainer .cs-optgroup li', function() {
//alert($(this).find("span").text());
$("#fullname").html($(this).find("span").text());
});	
//**********************************************************