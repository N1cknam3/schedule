<?php
class StudentCabinetHelper extends AppHelper {
	
	public $helpers = array('Html', 'Form', 'Js', 'Widgets');
	
	public function NewSchedule($schedulesArray, $facultiesArray) {
		$answer = '';
		
		if (empty($schedulesArray)) {
			//	Ошибка в дальнейшей логике
// 			$answer .= $this->NewScheduleRow(null, $facultiesArray, null, null);
		} else {
			foreach($schedulesArray as $schedule) {
				$specialitiesArray = (isset($schedule['Speciality'])) ? $schedule['Speciality'] : null;
				$groupsArray = (isset($schedule['Group'])) ? $schedule['Group'] : null;
				$answer .= $this->NewScheduleRow($schedule['GuestSchedule'], $facultiesArray, $specialitiesArray, $groupsArray);
			}
		}
		
		return $answer;
	}
	
	public function NewScheduleRow($schedule, $facultiesArray, $specialitiesArray, $groupsArray) {
		$answer = '';
		
		$answer .= '<div class="schedule-student-row" style="clear: both; padding: 15px 0px 0px">';
		
			$f_selected = ($schedule['faculty_id'] === null) ? -1 : $schedule['faculty_id'];
			$s_selected = ($schedule['speciality_id'] === null) ? -1 : $schedule['speciality_id'];
			$c_selected = ($schedule['course'] === null) ? -1 : $schedule['course'];
			$g_selected = ($schedule['group_id'] === null) ? -1 : $schedule['group_id'];
			$is_checked_class_add = ($schedule['is_primary']) ? "fa-star" : "fa-star-o";
        	$is_checked_color = ($schedule['is_primary']) ? "#D74E48" : "#A5A5A5";
        	
        	$answer .= '<div class="schedule-student-faculty-box">';
        		$answer .= $this->Form->input('id', array(
					'type' => 'hidden',
					'id' => 'id_SelectBox',
					'label' => false,
					'class' => 'guest-schedule-id',
					'value' => $schedule['id']
				));
        	$answer .= '</div>';
			
			$answer .= '<div class="schedule-student-faculty-box">';
				$answer .= $this->NewScheduleRow_faculties($facultiesArray, $f_selected);
			$answer .= '</div>';
		
			$answer .= '<div class="schedule-student-speciality-box">';
				if ($f_selected !== -1) {
					$answer .= $this->NewScheduleRow_specialities($specialitiesArray, $s_selected);
				}
			$answer .= '</div>';
        	
        	$answer .= '<div class="schedule-student-course-box">';
        		if ($s_selected !== -1) {
					$answer .= $this->NewScheduleRow_courses($c_selected);
				}
        	$answer .= '</div>';
        	
        	$answer .= '<div class="schedule-student-group-box">';
        		if ($c_selected !== -1) {
					$answer .= $this->NewScheduleRow_groups($groupsArray, $g_selected);
				}
        	$answer .= '</div>';

        	$answer .= '<div class="speciality-controls" style="float: left; padding-left: 15px; padding-right: 15px;">';
        	
        		/*
$status = $title = null;
        		if ($schedule['is_ready']) {
	        		$status = '<i class="fa fa-check-square-o" style="color: green"></i>';
	        		$title = __('Расписание заполнено');
        		} else {
	        		$status = '<i class="fa fa-ban" style="color: red"></i>';
	        		$title = __('Расписание заполнено не полностью!');
        		}
        		
        		$status_load = '<i class="fa fa-spinner fa-pulse" style="color: grey"></i>';
        		$title_load = __('Загрузка данных');
        	
        		$answer .= '<div class="schedule-ready speciality-control-button" title="'.$title.'">';
					$answer .= $status	;
				$answer .= '</div>';
*/
				
				$answer .= $this->NewScheduleRow_status ($schedule['is_ready']);
        	
        		$answer .= '<div class="speciality-check speciality-control-button">';
					$answer .= '<i class="fa '.$is_checked_class_add.'" style="color: '.$is_checked_color.'"></i>';
				$answer .= '</div>';
				
				$answer .= '<div class="speciality-minus speciality-control-button">';
					$answer .= '<i class="icon-minus"></i>';
				$answer .= '</div>';
				
			$answer .= '</div>';
			
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function NewScheduleRow_faculties ($facultiesArray, $selected = -1){
		$answer = '';
        	
    	$answer .= '<div class="schedule-student-col col-md-3">';
        	$answer .= $this->Widgets->ChooseFaculty($facultiesArray, "NewSchedule_add_speciality(this)", $selected);
    	$answer .= '</div>';
		
		return $answer;
	}
	
	public function NewScheduleRow_specialities ($specialitiesArray, $selected = -1){
		$answer = '';
        	
    	$answer .= '<div class="schedule-student-col col-md-3">';
        	$answer .= $this->Widgets->ChooseSpeciality($specialitiesArray, "NewSchedule_add_course(this)", $selected);
    	$answer .= '</div>';
		
		return $answer;
	}
	
	public function NewScheduleRow_courses ($selected = -1){
		$answer = '';
        	
    	$answer .= '<div class="schedule-student-col col-md-2">';
        	$answer .= $this->Widgets->ChooseCourse("NewSchedule_add_group(this)", $selected);
    	$answer .= '</div>';
		
		return $answer;
	}
	
	public function NewScheduleRow_groups ($groupsArray, $selected = -1){
		$answer = '';
        	
    	$answer .= '<div class="schedule-student-col col-md-2">';
        	$answer .= $this->Widgets->ChooseGroup($groupsArray, "NewSchedule_updateGroups(this)", $selected);
    	$answer .= '</div>';
		
		return $answer;
	}
	
	public function NewScheduleRow_status ($is_ready){
		$answer = '';
        
        $status = $title = null;
        
    	if ($is_ready) {
    		$status = '<i class="fa fa-check-square-o" style="color: green"></i>';
    		$title = __('Расписание заполнено');
		} else {
    		$status = '<i class="fa fa-ban" style="color: red"></i>';
    		$title = __('Расписание заполнено не полностью!');
		}
		
		$answer .= '<div class="schedule-ready speciality-control-button" title="'.$title.'">';
			$answer .= $status	;
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function NewSchedule_list($schedules) {
		$answer = '';
		
		return $answer;
	}
	
	public function ScheduleList() {
		
	}

}
?>