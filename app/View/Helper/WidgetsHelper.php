<?php
class WidgetsHelper extends AppHelper {
	
	public $helpers = array('Html', 'Form', 'Js');
	
	public function TableSpecialities ($listArray, $modelName, $modelAttributesArray, $type = null){
		
		$answer = '';
				
		$colgroupArray = array(70, 250, 250, 70);
		
		$answer .= $this->TableHeader(array("Код", "Название", "Профиль", ""), $colgroupArray);
		
		$answer .= $this->TableBody($listArray, $modelName, $modelAttributesArray, $colgroupArray, "specialityButtons", $type);
		
		$answer .= $this->TableFooterAdd('javascript:speciality_add_show('.$type.')', '<i class="icon-plus"></i>', "Добавить специальность");
		
		return $answer;
	}
	
	public function TableHeader($textArray, $colsWidthArray = null){
		$answer = '';		
		
		$answer .= '<div class="box-content tickets-new-details dashboard-pinned-project-header bg-purple" style="padding:0px 10px;">';
			$answer .= '<table class="table table_header" style="margin-bottom: 0px; border-top: none;">';
				
				if(!empty($colsWidthArray))
				{
					$answer .= $this->TableColgroup($colsWidthArray);
				}
				
				$answer .= '<tr>';
				
					foreach($textArray as $title)
					{
						$answer .= '<td style="border-top: none">'.$title.'</td>';
					}
					
                $answer .= '</tr>';		
			$answer .= '</table>';
		$answer .= '</div>';		
		
		return $answer;
	}
	
	public function TableColgroup($colsWidthArray){
		$answer = '';
		
		$answer .= '<colgroup>';
		
			foreach($colsWidthArray as $colWidth)
			{
				$answer .= '<col width="'.$colWidth.'" valign="top">';
			}
			
		$answer .= '</colgroup>';
		
		return $answer;
	}
	
	public function TableBody($listArray, $modelName, $modelAttributesArray, $colsWidthArray, $plugin, $type = null){
		$answer = '';		
		
		$answer .= '<div class="box-content tickets-new-details">';
			$answer .= '<div class="slimScrollDiv" style="position: relative; width: auto; height: 300px;">';
				$answer .= '<div class="widget-content slimScrollHomeTasks" style="overflow: scroll; width: auto; height: 300px; padding: 10px">';						
					$answer .= '<table class="table_body table" style="margin-bottom: 5px">';
			
					if(!empty($colsWidthArray))
					{
						$answer .= $this->TableColgroup($colsWidthArray);
					}
					
					foreach($listArray as $element)
					{
						
						$answer .= '<tr>';						
						
							$name_to_message = '\"';
							foreach($modelAttributesArray as $attribute)
							{
								$answer .= '<td><span id="'.$attribute.'-'.$element[$modelName]['id'].'">'.$element[$modelName][$attribute].'</span></td>';
								$name_to_message = $name_to_message . $element[$modelName][$attribute] . " ";
							}	
							
							$name_to_message = substr($name_to_message, 0, -1);
							$name_to_message .= '\"';
							
							//Место для кнопок справа	
							if ($plugin == "specialityButtons")			
								$answer .= $this->TablePluginActionSpecialityButtons($element, $modelName, $type, $name_to_message);
							else if ($plugin == "chairButtons")			
								$answer .= $this->TablePluginActionChairButtons($element, $modelName, $name_to_message);

						$answer .= '</tr>';
					}
			
					$answer .= '</table>';
				$answer .= '</div>';
			$answer .= '</div>';
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function TableFooterAdd($js_function, $icon = null, $text = null, $success = null){
		$answer = '';
		$answer .= '<div class="nav_table_specialities" style="background: rgba(26, 170, 217, 0.75); padding: 0px;">';
			$answer .= '<ul class="nav_table_specialities_add_controls">';
				$answer .= '<li>';
					
					$answer .= $this->Html->link(
												$icon.' '.$text,													
												$js_function,
												array(
													'escape' => false,
													'class' => 'button_add'
												)
											);
					
					if ($success != null) {
						$answer .= '<div class="success">'.$success.'</div>';
					}
					
				$answer .= '</li>';
	    	$answer .= '</ul>';
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function TablePluginActionSpecialityButtons($element, $modelName, $type, $deleteMessage) {
		$answer = "";
		
		$answer .= '<td style="vertical-align: middle; padding:0; overflow: auto;">';
			$answer .= '<div class="nav_table_specialities">';
				$answer .= '<ul class="nav_table_specialities_add_controls" tabindex="-1" data-reactid=".1.0.1.0">';
					$answer .= '<li><a class="button_edit" href="javascript:speciality_edit_show('. $element[$modelName]['id'] .",".$type.')"><i class="icon-edit"></i></a></li>';
					
					$answer .= '<li>';
					$answer .= $this->Js->link(
						'<i class="icon-trash"></i>',
						array(
							'controller'=>'faculties',
							'action'=>'speciality_ajax_delete/'.$element[$modelName]['id']."/".$type
						),
						array(
							'update'=>'#ajax_reload_content',
							'complete' => "speciality_delete()",
							'before' => 'return confirmDelete("'.$deleteMessage.'")',
							'url' => false,
							'escape' => false
						)
					);
					$answer .= '</li>';
					
					$answer .= $this->Js->writeBuffer();
					
		    	$answer .= '</ul>';
			$answer .= '</div>';
		$answer .= '</td>';
		
		return $answer;
	}
	
	public function TablePluginActionChairButtons($element, $modelName, $deleteMessage) {
		$answer = "";
		
		$answer .= '<td style="vertical-align: middle; padding:0; overflow: auto;">';
			$answer .= '<div class="nav_table_specialities">';
				$answer .= '<ul class="nav_table_specialities_add_controls" tabindex="-1" data-reactid=".1.0.1.0">';
					$answer .= '<li><a class="button_edit" href="javascript:chair_edit_show('. $element[$modelName]['id'].')"><i class="icon-edit"></i></a></li>';
					
					$answer .= '<li>';
					$answer .= $this->Js->link(
						'<i class="icon-trash"></i>',
						array(
							'controller'=>'faculties',
							'action'=>'chair_ajax_delete/'.$element[$modelName]['id']
						),
						array(
							'update'=>'#ajax_reload_content',
							'complete' => "chair_delete()",
							'before' => 'return confirmDelete("'.$deleteMessage.'")',
							'url' => false,
							'escape' => false
						)
					);
					$answer .= '</li>';
					
					$answer .= $this->Js->writeBuffer();
					
		    	$answer .= '</ul>';
			$answer .= '</div>';
		$answer .= '</td>';
		
		return $answer;
	}
	
	public function NoResults(){
		$answer = '';
		
		$answer .= '<div class="widget" style="margin-bottom: 0;">';
			$answer .= '<div class="widget-content widget-big-box">Нет результатов</div>';
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function TableChairs ($listArray, $modelName, $modelAttributesArray){
		
		$answer = '';
				
		$colgroupArray = array(570, 70);
		
		$answer .= $this->TableHeader(array("Название", ""), $colgroupArray);
		
		$answer .= $this->TableBody($listArray, $modelName, $modelAttributesArray, $colgroupArray, "chairButtons");
		
		$answer .= $this->TableFooterAdd('javascript:chair_add_show()', '<i class="icon-plus"></i>', "Добавить кафедру");
		
		return $answer;
	}
	
	//	---------------------------------------------------------------------------------------------------------------
	//	РАСПИСАНИЕ ----------------------------------------------------------------------------------------------------
	
	//	Вывод selectbox'а выбора курса с JS функцией изменения: показ selectbox'а выбора специальности
	public function ChooseCourse($onChange = null, $selected = -1) {
		
		$answer = '';
		
		$options = array(
			array(
                'name' => 'Выберите курс',
                'value' => '-1',
                'disabled' => TRUE/*
,
                'selected' => TRUE
*/
            ),
            '1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6'
        );
        
        if ($onChange === null) {
	        $onChange = $this->Js->request(
	            array('action' => 'schedule_step1_showSpecialitySelectbox'),
	            array(
	                    'update' => '#ajax_reload_step1_ChooseSpeciality', // element to update
	                                             			// after form submission
	                    'before' => "schedule_ajax_reload_clearAll()",
	                    'async' => true,
	                    'dataExpression'=>true,
	                    'method' => 'POST'
	                )
	            );
        }
		
		$answer .= $this->Form->input('course', array(
			'type' => 'select',
			'class' => 'form-control guest-schedule-course',
			'id' => 'course_SelectBox',
			'label' => false,
			'options' => $options,
			'selected' => $selected,
			'onchange' => $onChange
		));
		
		return $answer;
	}
	
	//	Вывод selectbox'а выбора группы с JS функцией изменения: вывод таблицы расписания
	public function ChooseGroup($groupsArray = null, $onChange = "", $selected = -1) {
		
		$answer = '';
		
		$options = array(
			array(
                'name' => 'Все группы',
                'value' => '-1'/*
,
                'selected' => TRUE
*/
            )
        );
        
        if ($groupsArray != null)
        	$options += $groupsArray;

		$answer .= $this->Form->input('group', array(
			'type' => 'select',
			'class' => 'form-control guest-schedule-group_id',
			'id' => 'group_SelectBox',
			'label' => false,
			'options' => $options,
			'selected' => $selected,
			'onchange' => $onChange
		));
		
		return $answer;
	}
	
	//	Вывод selectbox'а выбора специальности с JS функцией изменения: вывод таблицы расписания
	public function ChooseSpeciality($specialitiesArray = null, $onChange = "speciality_Selected()", $selected = -1) {
		
		$answer = '';
		
		$options = array(
			array(
                'name' => 'Выберите специальность',
                'value' => '-1',
                'disabled' => TRUE/*
,
                'selected' => TRUE
*/
            )
        );
        
        if ($specialitiesArray != null)
        	$options += $specialitiesArray;

		$answer .= $this->Form->input('speciality', array(
			'type' => 'select',
			'class' => 'form-control guest-schedule-speciality_id',
			'id' => 'speciality_SelectBox',
			'label' => false,
			'options' => $options,
			'selected' => $selected,
			'onchange' => $onChange
		));
		
		return $answer;
	}
	
	//	Вывод selectbox'а выбора специальности с JS функцией изменения: вывод таблицы расписания
	public function ChooseFaculty($facultiesArray = null, $onChange = "", $selected = -1) {
		
		$answer = '';
		
		$options = array(
			array(
                'name' => 'Выберите факультет',
                'value' => '-1',
                'disabled' => TRUE/*
,
                'selected' => TRUE
*/
            )
        );
        
        if ($facultiesArray != null)
        	$options += $facultiesArray;

		$answer .= $this->Form->input('faculty', array(
			'type' => 'select',
			'class' => 'form-control guest-schedule-faculty_id',
			'id' => 'faculty_SelectBox',
			'label' => false,
			'options' => $options,
			'selected' => $selected,
			'onchange' => $onChange
		));
		
		return $answer;
	}
	
	public function ScheduleTable_User_list($days, $rings, $tables_data) {
		$tf = true;
		
			$answer = '';
			
			foreach($tables_data as $currTable) {
				$table = $currTable['table'];
				$groups = $currTable['groups'];
				$speciality = $currTable['speciality'];
				$speciality_type = $currTable['speciality_type'];
				$course = $currTable['course'];
				$group_name = $currTable['group_name'];
				
				if ($group_name)
					$group_name = ", группа " . $group_name;
		      
				$answer .= '<div class="schedule-list-container">';
					$answer .= '<div class="schedule-list">';
						$answer .= '<span class="schedule-list-title">'.$speciality_type.", ".$speciality['Speciality']['name'].", ".$speciality['Speciality']['profile'].", ".$course.' курс'.$group_name.'</span>';
					$answer .= '</div>';
					
					$answer .= '<div class="schedule-list-content">';
				
				
						if ($currTable['is_primary'])
							$answer .= '<div class="schedule-table-list-row-content">';
						else
							$answer .= '<div class="schedule-table-list-row-content">';
						
							$answer .= '<div class="col-md-12" style="overflow-x: scroll;">';	
							
								if ($groups != null) {
									$answer .= $this->ScheduleTable_User($days, $rings, $speciality, $course, $groups, $table);
								} else {
									$answer .= $this->ScheduleTable_noTable();
								}
								
							$answer .= '</div>';
						$answer .= '</div>';
				
					$answer .= '</div>';
				$answer .= '</div>';
					
			}
			
		return $answer;
	}
	
	public function ScheduleTable_noTable() {
	
		$messages = array();
		
		//	0
		array_push($messages, '
			<p style="font-size: 20px;">Мы знаем, как вы любите учиться</p>
			<p style="font-size: 20px;">Но пока что расписание для вас ещё не готово..</p>
			<p style="font-size: 20px;">Но кто-то над этим уже работает :)</p>
		
			<i class="fa fa-cog fa-spin" style="font-size: 50px;"></i>
		');
		
		//	1
		array_push($messages, '
			<p style="font-size: 20px;">Упс!</p>
			<p style="font-size: 20px;">Извините, на данный момент расписание недоступно</p>
			<p style="font-size: 20px;">Но кто-то над этим уже работает</p>
		');
		
		//	2
		array_push($messages, '
			<p style="font-size: 20px;">Оп-па!</p>
			<p style="font-size: 20px;">Похоже, что Вы нас опередили..</p>
			<p style="font-size: 20px;">Вы нас опередили</p>
			<p style="font-size: 20px;">Но кто-то над этим уже работает</p>
		');
		
		//	3
		array_push($messages, '
			<p style="font-size: 20px;">Оп-па!</p>
			<p style="font-size: 20px;">Похоже, что вы оказались оперативнее составителей, и расписание для вас ещё не готово..</p>
			<p style="font-size: 20px;">Вероятно, вы захотите зайти позже? :)</p>
		');
		
		
		//	4
		array_push($messages, '
			<p style="font-size: 20px;">Ох и шустрый же ты, студент!</p>
			<p style="font-size: 20px;">Вот бы твою оперативность, да не только во время сессии :)</p>
			<p style="font-size: 20px;">Приходи чуть позже, у нас для тебя подарок</p>
		');
		
		//	5
		array_push($messages, '
			<p style="font-size: 20px;">Тссс..</p>
			<p style="font-size: 20px;">Скажу по-секрету: для вас готовится настоящий сюрприз!</p>
			<p style="font-size: 20px;">Заходи в гости в другой раз</i></p>
			<p style="font-size: 20px;">P.S. Только чур никому, лады?</p>
		');
		
		//	6
		array_push($messages, '
			<p style="font-size: 20px;">Хьюстон, у нас проблемы!</p>
			<p style="font-size: 20px;">Кажется, кто-то потерял ваше расписание..</p>
			<p style="font-size: 20px;">Но не волнуйтесь, мы его отыщем!</p>
		');
		
		//	7
		array_push($messages, '
			<p style="font-size: 20px;">Стоять!</p>
			<p style="font-size: 20px;">А теперь ти-и-ихо закрой расписание и зайди позже..</p>
			<p style="font-size: 20px;">И пусть это останется между нами</p>
		');
		
		//	8
		array_push($messages, '
			<p style="font-size: 20px;">Бугагашенька!</p>
			<p style="font-size: 20px;">А вообще да, мы потеряли ваше расписание..</p>
			<p style="font-size: 20px;">Но вы же нас простите, если мы исправимся?</p>
		');
		
		//	9
		array_push($messages, '
			<p style="font-size: 20px;">Ох ох..</p>
			<p style="font-size: 20px;">Расписания нет потому, что я его съел!</p>
			<p style="font-size: 20px;">В следующий раз заходи скорей, может и успеешь ;)</p>
		');
		
		//	10
		array_push($messages, '
			<p style="font-size: 20px;">Здесь могло бы быть ваше расписание</p>
		');
		
		//11
		array_push($messages, '
			<p style="font-size: 20px;">Расписания ищешь ты</p>
			<p style="font-size: 20px;">Но только недоступно сейчас оно</p>
		');
		
		//	12
		array_push($messages, '
			<p style="font-size: 20px;">Ох ты ж ёжик!</p>
			<p style="font-size: 20px;">Кто-то забыл составить расписание!</p>
		');
		
		//	13
		array_push($messages, '
			<p style="font-size: 20px;">На первый взгляд, расписания здесь нет..</p>
			<p style="font-size: 20px;">Но стоит зайти чуть позже!..</p>
		');
		
		//	14
		array_push($messages, '
			<p style="font-size: 20px;">Расписания пока ещё нет..</p>
			<p style="font-size: 20px;">Зато есть..</p>
		');
		
		//	15
		array_push($messages, '
			<i class="fa fa-stethoscope" style="font-size: 50px;"></i>
			<i class="fa fa-cog fa-spin" style="font-size: 50px;"></i>
		');

		$answer = '';
		
		$answer .= '<div class="error-not-found">';
		
				$answer .= $messages[0];
			
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function ScheduleTable_User($days, $rings, $speciality, $course, $groups, $table) {
		$answer = '';
		
		$answer .= '<h1 style="text-transform: uppercase">Расписание занятий '.$course.'-го курса</h1>';
		$answer .= '<!-- <h2>'.@$weekType.'</h2> -->';
		
		$answer .= '<table class="schedule-table center user">';
	
			$answer .= '<caption>';
				$answer .= '<div style="text-align: left">';
						$answer .= 'Специальность: '.$speciality['Speciality']['name'];
						$answer .= '<br>';
						$answer .= 'Профиль: '.$speciality['Speciality']['profile'];
				$answer .= '</div>';
			$answer .= '</caption>';
			
			$answer .= '<thead>';
				$answer .= $this->ScheduleTableRowGroups_User(@$groups);
			$answer .= '</thead>';
			
			$answer .= '<tbody>';
				
					$count_groups = count($groups);
					
					/*
						Вывод всех занятий по дням недели, содержащихся в $days
						Если в массиве занятий есть записи такого дня, то происходит их вывод,
						иначе - выходной
					*/
					foreach ($days as $day)
					{
						if ($table && array_key_exists($day['Day']['id'], $table))
						{
							//	Вывод занятий этого дня
							$answer .= $this->ScheduleTableRowDay_User($day, $rings, $table[$day['Day']['id']], $groups);
							
						} else {
							//	Если нет записей этого дня, то вывод графы о том, что этот день - выходной
							$answer .= $this->ScheduleTableRowDayOff_User($day, $count_groups + 1);
						} 
					}
				
			$answer .= '</tbody>';
			
		$answer .= '</table>';
		
		return $answer;
	}
	
	//	$lesson_info используется для назначения поля "name" input'а
	//	поля $lesson_info: ['day_id' => .. , 'ring_id' => .. , 'group_id' => .. , 'type' => ..]
	public function ScheduleTableCellSubject($name = null, $lesson_info = array(), $global_lesson = false) {
		$answer = '';
		
		if ($name == null) {
			$visible = false;	
		} else {
			$visible = true;
		}
		
		$day_id = $ring_id = $group_id = "0";
		$type = "up";
		
		if (!empty($lesson_info)) {
			$day_id = $lesson_info['day_id'];
			$ring_id = $lesson_info['ring_id'];
			$group_id = $lesson_info['group_id'];
			if (strpos($group_id, ",") !== false) {
				$group_id = "all";
			}
			if ($lesson_info['type'] == "0") {
				$type = "up";
			} else if ($lesson_info['type'] == "1") {
				$type = "down";
			}
		}
		
		$class_name_add = "";
		if ($global_lesson)
			$class_name_add = " global";
		
		$visible_name = ($visible) ? $name : "Нет предмета";
		
		$answer .= ($visible) ? '<div class="subject">' : '<div class="subject unactive padding">';
		
			$answer .= '<span class="subject">'.$visible_name.'</span>';
			
			$answer .= $this->Form->input('d-'.$day_id.'.'.'r-'.$ring_id.'.'.'g-'.$group_id.'.'.$type.'.subject', array(
			    'label' => false,
			    'type' => 'text',
			    'placeholder' => 'Название предмета',
			    'value' => $name,
			    'div' => false,
			    'class' => 'form-control subject'.$class_name_add,
			    'style' => 'display: none;'					    
			));
			
			//	Блок для автоподстановки
			$answer .= '<div class="suggestion-box"></div>';
			
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function ScheduleTableCellSubject_User($name = null, $is_withoutTeacher = false) {
		$answer = '';
		
		if ($name == null) {
			$visible = false;	
		} else {
			$visible = true;
		}
		
		$class_added = ($is_withoutTeacher) ? 'padding-one' : '';
		
		$visible_name = ($visible) ? $name : "";
		
		$answer .= ($visible) ? '<div class="subject '.$class_added.'">' : '<div class="subject unactive padding">';
		
			$answer .= '<span class="subject">'.$visible_name.'</span>';
			
		$answer .= '</div>';
		
		return $answer;
	}
	
	//	$lesson_info используется для назначения поля "name" input'а
	//	поля $lesson_info: ['day_id' => .. , 'ring_id' => .. , 'group_id' => .. , 'type' => ..]
	public function ScheduleTableCellTeacherName($name = null, $lesson_info = array(), $global_lesson = false, $visible = false) {
		$answer = '';
		
		if ($name == null) {
			$enable = false;	
		} else {
			$enable = true;
		}
		
		$day_id = $ring_id = $group_id = 0;
		$type = "up";
		
		if (!empty($lesson_info)) {
			$day_id = $lesson_info['day_id'];
			$ring_id = $lesson_info['ring_id'];
			$group_id = $lesson_info['group_id'];
			if (strpos($group_id, ",") !== false) {
				$group_id = "all";
			}
			if ($lesson_info['type'] == "0") {
				$type = "up";
			} else if ($lesson_info['type'] == "1") {
				$type = "down";
			}
		}
		
		$class_name_add = "";
		if ($global_lesson)
			$class_name_add = " global";
		
		$visible_name = ($enable) ? $name : "Нет преподавателя";
		
		$answer .= ($visible) ? '<div class="name">' : '<div class="name" style="display: none;">';
		
			$answer .= ($enable) ? '<span class="name">' : '<span class="name unactive">';
			$answer .= $visible_name.'</span>';
			
			$answer .= $this->Form->input('d-'.$day_id.'.'.'r-'.$ring_id.'.'.'g-'.$group_id.'.'.$type.'.name', array(
			    'label' => false,
			    'type' => 'text',
			    'placeholder' => 'Имя преподавателя',
			    'value' => $name,
			    'div' => false,
			    'class' => 'form-control name'.$class_name_add,
			    'style' => 'display: none;'					    
			));
			
			//	Блок для автоподстановки
			$answer .= '<div class="suggestion-box"></div>';
			
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function ScheduleTableCellTeacherName_User($name = null, $visible = false) {
		$answer = '';
		
		if ($name == null) {
			$enable = false;	
		} else {
			$enable = true;
		}
		
		$visible_name = ($enable) ? $name : "";
		
		$answer .= ($visible) ? '<div class="name">' : '<div class="name" style="display: none;">';
		
			$answer .= ($enable) ? '<span class="name">' : '<span class="name unactive">';
			$answer .= $visible_name.'</span>';
			
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function ScheduleTableCellLessonID($lesson_info = array()) {
		
		$answer = "";
		
		$day_id = $ring_id = $group_id = 0;
		$type = "up";
		$id = "no";
		
		if (!empty($lesson_info)) {
			$day_id = $lesson_info['day_id'];
			$ring_id = $lesson_info['ring_id'];
			$group_id = $lesson_info['group_id'];
			if (strpos($group_id, ",") !== false) {
				$group_id = "all";
			}
			if ($lesson_info['type'] == "0") {
				$type = "up";
			} else if ($lesson_info['type'] == "1") {
				$type = "down";
			}
			$id = $lesson_info['id'];
		}
		
		$answer .= $this->Form->input('d-'.$day_id.'.'.'r-'.$ring_id.'.'.'g-'.$group_id.'.'.$type.'.id', array(
		    'label' => false,
		    'type' => 'text',
		    'placeholder' => false,
		    'value' => $id,
		    'div' => false,
		    'class' => 'id',
		    'style' => 'display: none;'					    
		));
		
		return $answer;
	}
	
	public function ScheduleTableCellGroupName($group = null) {
		$answer = '';
		
		if ($group == null) {
			$visible = false;
		} else {
			$visible = true;
			if ($group['Group']['name'] != null)
				$name = $group['Group']['name'];
			else
				$visible = false;
		}
		
		$visible_name = ($visible) ? $name : "Название группы";
		
		$answer .= '<div class="group-name">';
		
			$answer .= ($visible) ? '<span class="group-name">' : '<span class="group-name unactive">';
			$answer .= $visible_name.'</span>';
			
			$answer .= $this->Form->input('Group.'.$group["Group"]["id"].'.name', array(
			    'label' => false,
			    'type' => 'text',
			    'placeholder' => 'Имя группы',
			    'value' => @$name,
			    'div' => false,
			    'class' => 'form-control group-name',
			    'style' => 'display: none;'
			));
		$answer .= '</div>';
		
		$answer .= '<div class="table-group-close" style="display: none;"><i class="fa fa-times"></i></div>';
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function ScheduleTableCellGroupName_User($group = null) {
		$answer = '';
		
		if ($group == null) {
			$visible = false;
		} else {
			$visible = true;
			if ($group['Group']['name'] != null)
				$name = $group['Group']['name'];
			else
				$visible = false;
		}
		
		$visible_name = ($visible) ? $name : "Без названия";
		
		$answer .= '<div class="group-name">';
		
			$answer .= ($visible) ? '<span class="group-name">' : '<span class="group-name unactive">';
			$answer .= $visible_name.'</span>';
			
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function ScheduleTableRowGroups($groups = null) {
		
		$answer = '';
		
		$answer .= '<tr>';
			$answer .= '<th id="head-day" class="day">День</th>';
			$answer .= '<th id="head-time" class="time">Время</th>';
			
			if ($groups != null)
			{
				foreach ($groups as $group)
				{
					$answer .= '<th id="head-group-'.$group['Group']['id'].'" class="subject" group="'.$group['Group']['id'].'">';
						$answer .= $this->ScheduleTableCellGroupName($group);
					$answer .= '</th>';
				}
			} else {
				$answer .= '<th id="head-group-" class="subject">';
					$answer .= $this->ScheduleTableCellGroupName();
				$answer .= '</th>';
			}
			
			$answer .= '<th id="num-of-groups-'.count($groups).'" class="add"><i class="icon-plus"></i></th>';
		$answer .= '</tr>';
		
		return $answer;	
	}
	
	public function ScheduleTableRowGroups_User($groups = null) {
		
		$answer = '';
		
		$answer .= '<tr>';
			$answer .= '<th id="head-day" class="day">День</th>';
			$answer .= '<th id="head-time" class="time">Время</th>';
			
			if ($groups != null)
			{
				foreach ($groups as $group)
				{
					$answer .= '<th class="subject">';
						$answer .= $this->ScheduleTableCellGroupName_User($group);
					$answer .= '</th>';
				}
			} else {
				$answer .= '<th class="subject">';
					$answer .= $this->ScheduleTableCellGroupName_User();
				$answer .= '</th>';
			}
			
		$answer .= '</tr>';
		
		return $answer;	
	}
	
	//	День
	public function ScheduleTableRowDay($day, $rings, $table_day, $groups) {
		/*
			$day - запись дня (информация о текущем дне)
			$rings - массив всех звонков
			$table_day - массив занятий дня вида: [day_id][ring_id][group_id]
			$groups - массив групп выбранного курса и специальности
		*/
		
		$answer = '';
		$header_answer = '';
		
		// В самой первой строке хранится блок "add" для добавления группы
		// Необходимо изменять её атрибут "rowspan" согласно количеству <tr>
		
		// Блок дня
		// В первой строке блока дня хранится название дня недели
		// В ПЕРВОЙ строке так же необходимо добавлять "+" и "-" для блока времени в НАЧАЛЕ
			// Но необходимо учитывать случай, когда это время - единственное для этого дня
		// Количество <tr> в дне недели определяет количество часов
		// У времени tr.id = "time-" + id записи ring
		
		$group_count = count($groups);
		
		$row_count = 1;
		$day_ring_count = count($table_day);
		//	$rings_types - ассив, соответствующий звонкам этого дня, в котором приведен тип органов управления временем соответствующего звонка
		$rings_types = $this->ScheduleTableCol_Ring_getNeededControlType($rings, $table_day);
		
		/*
			Так как возможны ситуации, когда между двумя звонками могут быть "пробелы":
			{'ring_id = 1' => .. , 'ring_id = 5' => .. , 'ring_id = 8' => ..}
			То необходимо следить за этим и вместо "пробелов" вставлять пустые строки звонком методом $this->ScheduleTableRowLessons_empty()
			Слежение происходит путем сравнения предыдущего id звонка (prev_ring_id) и текущего
			Стоит заметить, что звонки отсортированы по id
		*/
		$prev_ring_id = -1;	//	Идентификация первой записи
				
		foreach ($table_day as $ring_id => $ring) {	// [day_id][ring_id]
			
			/*
				Для случая разрыва между соседними занятиями
				Место для вывода пустых занятий
			*/
			if ($prev_ring_id != -1) {	//	Исключая самую первую запись
				
				//	Разница между соседними ring_id должна быть не более 1
				for($i = 1; $i < $ring_id - $prev_ring_id; $i++) {
					//	$type в данном случае обязательно пустой, так как не должно быть элементов управления
					//	$position в этом случае не имеет значения, так как оно заведомо отлично от первого ("top") и последнего ("bottom")
					$answer .= $this->ScheduleTableRowLessons_empty($day['Day']['id'], $rings[$prev_ring_id + $i], null, "", $groups);
					
					//	Так как произошло вмешательство, то необходимо увеличить размер строк, занимаемых заголовком дня недели
					$day_ring_count++;
				}
				
			}
			//	После этого запоминаем ring_id для следующего сравнения
			$prev_ring_id = $ring_id;
			
			//	ВЫВОД СТРОКИ ТЕКУЩИХ ЗАНЯТИЙ
			$answer .= '<tr class="day-'.$day['Day']['id'].'">';
			
			//	Для первой строки занятий выводится информация о дне недели
			//	В качестве которого берется пустой невидимый столбец дня
				$answer .= '<td style="display: none;"></td>';
			
			//	ВЫВОД ИНФОРМАЦИИ О ВРЕМЕНИ ЗАНЯТИЯ
			//	Для первой записи - top
			$position = "top";
			//	Для последней - bottom
			if ($row_count == $day_ring_count)
				$position = "bottom";
				
			//	Отдельно стоит учитывать, что [+ -] всегда вверху, а [- +] - внизу
			if ($rings_types[$row_count-1] == '1')	//	+ -
				$position = "top";
			else if ($rings_types[$row_count-1] == '3')	//	- +
				$position = "bottom";
				
			$answer .= $this->ScheduleTableCol_Time($rings[$ring_id], $rings_types[$row_count-1], $position);
			
			//	подсчитаем кол-во групп в занятии
			$lesson_count = count($ring);
			
			//	Вывод информации по каждому из занятий
			foreach ($ring as $group_id => $group_lesson) {	//	[day_id][ring_id][group_id]	|| [day_id][ring_id][all]
				
				//	На случай пустых данных
				if (empty($group_lesson['up'])) {
					$group_lesson['up']['Lesson']['id'] = "";
					$group_lesson['up']['Lesson']['day_id'] = $day['Day']['id'];
					$group_lesson['up']['Lesson']['ring_id'] = $ring_id;
					$group_lesson['up']['Lesson']['group_id'] = "no";
					$group_lesson['up']['Lesson']['type'] = "0";
				}
				
				//	Передача массива занятия вида: [ 'up' => .. , 'down' => .. ]
				$answer .= $this->ScheduleTableCol_Lesson($group_lesson, $group_count, $lesson_count);	
				
				if ($lesson_count == 1) {
					//	В СЛУЧАЕ ОДНОГО ОБЩЕГО ПРЕДМЕТА НЕОБХОДИМО ВЫВЕСТИ ПУСТЫЕ ПРЕДМЕТЫ ДЛЯ КАЖДОЙ ИЗ ОСТАВШИХСЯ ГРУПП
					//	И СДЕЛАТЬ ИХ НЕВИДИМЫМИ
					//	Считается, что общий предмет относится к первой группе
					for ($i=1; $i<$group_count; $i++) {
						
						$group_lesson = array();
						$group_lesson['up']['Lesson']['id'] = "";
						$group_lesson['up']['Lesson']['day_id'] = $day['Day']['id'];
						$group_lesson['up']['Lesson']['ring_id'] = $ring_id;
						$group_lesson['up']['Lesson']['group_id'] = "no";
						$group_lesson['up']['Lesson']['type'] = "0";
						
						//	Тут все верно с $lesson_count = $group_count
						//	false значит, что предмет не будет виден
						$answer .= $this->ScheduleTableCol_Lesson($group_lesson, $group_count, $group_count, false);
					}
				}
				
			}
			
			$answer .= '</tr>';
			
			$row_count++;
			
		}
		
		$header_answer = $this->ScheduleTableRowDayHeader($day, $day_ring_count, $group_count);
		
		$answer = $header_answer.$answer;
		//	конец: ВЫВОД ЗАГОЛОВКА ДНЯ
		
		return $answer;
	}
	
	public function ScheduleTableRowDay_User($day, $rings, $table_day, $groups) {
			//		$day - запись дня (информация о текущем дне)
			//$rings - массив всех звонков
			//$table_day - массив занятий дня вида: [day_id][ring_id][group_id]
			//$groups - массив групп выбранного курса и специальности
		
		
		$answer = '';
		$header_answer = '';
		
		// В самой первой строке хранится блок "add" для добавления группы
		// Необходимо изменять её атрибут "rowspan" согласно количеству <tr>
		
		// Блок дня
		// В первой строке блока дня хранится название дня недели
		// В ПЕРВОЙ строке так же необходимо добавлять "+" и "-" для блока времени в НАЧАЛЕ
			// Но необходимо учитывать случай, когда это время - единственное для этого дня
		// Количество <tr> в дне недели определяет количество часов
		// У времени tr.id = "time-" + id записи ring
		
		$group_count = count($groups);
		
		$day_ring_count = count($table_day);
		
		/*
			Так как возможны ситуации, когда между двумя звонками могут быть "пробелы":
			{'ring_id = 1' => .. , 'ring_id = 5' => .. , 'ring_id = 8' => ..}
			То необходимо следить за этим и вместо "пробелов" вставлять пустые строки звонком методом $this->ScheduleTableRowLessons_empty()
			Слежение происходит путем сравнения предыдущего id звонка (prev_ring_id) и текущего
			Стоит заметить, что звонки отсортированы по id
		*/
		
		$prev_ring_id = -1;	//	Идентификация первой записи
				
		foreach ($table_day as $ring_id => $ring) {	// [day_id][ring_id]
			
			
			//	Для случая разрыва между соседними занятиями
			//	Место для вывода пустых занятий
			
			if ($prev_ring_id != -1) {	//	Исключая самую первую запись
				
				//	Разница между соседними ring_id должна быть не более 1
				for($i = 1; $i < $ring_id - $prev_ring_id; $i++) {
					//	$type в данном случае обязательно пустой, так как не должно быть элементов управления
					//	$position в этом случае не имеет значения, так как оно заведомо отлично от первого ("top") и последнего ("bottom")
					$answer .= $this->ScheduleTableRowLessons_empty_User($day['Day']['id'], $rings[$prev_ring_id + $i], $groups);
					
					//	Так как произошло вмешательство, то необходимо увеличить размер строк, занимаемых заголовком дня недели
					$day_ring_count++;
				}
				
			}
			//	После этого запоминаем ring_id для следующего сравнения
			$prev_ring_id = $ring_id;
			
			//	ВЫВОД СТРОКИ ТЕКУЩИХ ЗАНЯТИЙ
			$answer .= '<tr>';
			
			//	Для первой строки занятий выводится информация о дне недели
			//	В качестве которого берется пустой невидимый столбец дня
				$answer .= '<td style="display: none;"></td>';
			
			//	ВЫВОД ИНФОРМАЦИИ О ВРЕМЕНИ ЗАНЯТИЯ
			$answer .= $this->ScheduleTableCol_Time_User($rings[$ring_id]);
			
			//	подсчитаем кол-во групп в занятии
			$lesson_count = count($ring);
			
			//	Вывод информации по каждому из занятий
			foreach ($ring as $group_id => $group_lesson) {	//	[day_id][ring_id][group_id]	|| [day_id][ring_id][all]
				
				//	На случай пустых данных
				if (empty($group_lesson['up'])) {
					$group_lesson['up']['Lesson']['id'] = "";
					$group_lesson['up']['Lesson']['day_id'] = $day['Day']['id'];
					$group_lesson['up']['Lesson']['ring_id'] = $ring_id;
					$group_lesson['up']['Lesson']['group_id'] = "no";
					$group_lesson['up']['Lesson']['type'] = "0";
				}
				
				//	Передача массива занятия вида: [ 'up' => .. , 'down' => .. ]
				$answer .= $this->ScheduleTableCol_Lesson_User($group_lesson, $group_count, $lesson_count);	
				
				if ($lesson_count == 1) {
					//	В СЛУЧАЕ ОДНОГО ОБЩЕГО ПРЕДМЕТА НЕОБХОДИМО ВЫВЕСТИ ПУСТЫЕ ПРЕДМЕТЫ ДЛЯ КАЖДОЙ ИЗ ОСТАВШИХСЯ ГРУПП
					//	И СДЕЛАТЬ ИХ НЕВИДИМЫМИ
					//	Считается, что общий предмет относится к первой группе
					for ($i=1; $i<$group_count; $i++) {
						
						$group_lesson = array();
						$group_lesson['up']['Lesson']['id'] = "";
						$group_lesson['up']['Lesson']['day_id'] = $day['Day']['id'];
						$group_lesson['up']['Lesson']['ring_id'] = $ring_id;
						$group_lesson['up']['Lesson']['group_id'] = "no";
						$group_lesson['up']['Lesson']['type'] = "0";
						
						//	Тут все верно с $lesson_count = $group_count
						//	false значит, что предмет не будет виден
						$answer .= $this->ScheduleTableCol_Lesson_User($group_lesson, $group_count, $group_count, false);
					}
				}
				
			}
			
			$answer .= '</tr>';
			
		}
		
		$header_answer = $this->ScheduleTableRowDayHeader_User($day, $day_ring_count);
		
		$answer = $header_answer.$answer;
		//	конец: ВЫВОД ЗАГОЛОВКА ДНЯ
		
		return $answer;
	}
	
	public function ScheduleTableRowDayHeader($day, $day_ring_count, $count_groups) {
		$header_answer = '';
		
		//	ВЫВОД ЗАГОЛОВКА ДНЯ
		$header_answer .= '<tr class="day-'.$day['Day']['id'].'">';
		$header_answer .= $this->ScheduleTableCol_Day($day, $day_ring_count + 1);	//	+1 так как эта добавочная строка тоже считается
		$header_answer .= '<td style="display: none;"></td>';
		for ($i = 0; $i < $count_groups; $i++) {
			$header_answer .= '<td style="display: none;"></td>';
		}
		$header_answer .= '</tr>';
		
		return $header_answer;
	}
	
	public function ScheduleTableRowDayHeader_User($day, $day_ring_count) {
		$header_answer = '';
		
		//	ВЫВОД ЗАГОЛОВКА ДНЯ
		$header_answer .= '<tr>';
		$header_answer .= $this->ScheduleTableCol_Day_User($day, $day_ring_count + 1);	//	+1 так как эта добавочная строка тоже считается
		$header_answer .= '</tr>';
		
		return $header_answer;
	}
	
	//	Вывод пустой строки предметов при нажатии кнопки "Добавить новый звонок"
	public function ScheduleTableRowLessons_empty($day_id, $ring, $type, $position, $groups) {
	//		$day_id - id дня, которому принадлежит этот звонок
	//		$ring - массив информации о звонке для вставки
	//		$type - тип кнопок управления (см. выше)
	//			null) нет элементов управления
	//			0) -
	//			1) + -
	//			2) + - +
	//			3) - +
	//		$position - позиция кнопок управления временем: top/bottom
	//		$groups - массив всех групп этой таблицы
		
		
		$answer = '';
		
		$group_count = count($groups);
		
		$answer .= '<tr class="day-'.$day_id.'">';
			
			//	Пустая колонка дня для приведения одинакового кол-ва столбцов строк таблицы
			$answer .= '<td style="display: none;"></td>';
		
			$answer .= $this->ScheduleTableCol_Time($ring, $type, $position);
			
			//	Создание пустого массива необходимой информации для занятия
			$group_lesson = array();
			$group_lesson['up']['Lesson']['id'] = "";
			$group_lesson['up']['Lesson']['day_id'] = "";
			$group_lesson['up']['Lesson']['day_id'] = $day_id;
			$group_lesson['up']['Lesson']['ring_id'] = $ring['Ring']['id'];
			$group_lesson['up']['Lesson']['group_id'] = "no";
			$group_lesson['up']['Lesson']['type'] = "0";
			
			foreach ($groups as $group) {
						
				$answer .= $this->ScheduleTableCol_Lesson($group_lesson, $group_count, $group_count);
				
			}
		
		$answer .= '</tr>';
		
		return $answer;
	}
	
	public function ScheduleTableRowLessons_empty_User($day_id, $ring, $groups) {
	//		$day_id - id дня, которому принадлежит этот звонок
	//		$ring - массив информации о звонке для вставки
	//		$type - тип кнопок управления (см. выше)
	//			null) нет элементов управления
	//			0) -
	//			1) + -
	//			2) + - +
	//			3) - +
	//		$position - позиция кнопок управления временем: top/bottom
	//		$groups - массив всех групп этой таблицы
		
		
		$answer = '';
		
		$group_count = count($groups);
		
		$answer .= '<tr>';
		
			$answer .= $this->ScheduleTableCol_Time_User($ring);
			
			//	Создание пустого массива необходимой информации для занятия
			$group_lesson = array();
			$group_lesson['up']['Lesson']['id'] = "";
			$group_lesson['up']['Lesson']['day_id'] = "";
			$group_lesson['up']['Lesson']['day_id'] = $day_id;
			$group_lesson['up']['Lesson']['ring_id'] = $ring['Ring']['id'];
			$group_lesson['up']['Lesson']['group_id'] = "all";
			$group_lesson['up']['Lesson']['type'] = "0";
			
			//foreach ($groups as $group) {
				$answer .= $this->ScheduleTableCol_Lesson_User($group_lesson, $group_count, $group_count + 1);
			//}
		
		$answer .= '</tr>';
		
		return $answer;
	}
	
	//	Выходной день
	public function ScheduleTableRowDayOff($day, $count_groups) {
		
		$answer = '';
		
		$answer .= $this->ScheduleTableRowDayHeader($day, 1, $count_groups);
		
		$answer .= $this->ScheduleTableRowDayOff_blockOnly($day["Day"]["id"], $count_groups);
		
		return $answer;	
	}
	
	public function ScheduleTableRowDayOff_User($day, $count_groups) {
		
		$answer = '';
		
		$answer .= $this->ScheduleTableRowDayHeader_User($day, 1);
		
		$answer .= $this->ScheduleTableRowDayOff_blockOnly_User($count_groups);
		
		return $answer;	
	}
	
	//	Выходной день без заголовка дня
	public function ScheduleTableRowDayOff_blockOnly($day_id, $count_groups) {
		
		$answer = '';
		
		$answer .= '<tr class="day-'.$day_id.'">';
		
		//	День
			$answer .= '<td style="display: none;"></td>';
		//	Время
			$answer .= '<td rowspan="1" colspan="1" id="time-0"	class="time">';	//	ring_id = 0 => добавлять нужно снизу => bottom
				$answer .= '<div class="col-md-6 table-time-plus bottom one" style="display: none;"><i class="icon-plus"></i></div>';
			$answer .= '</td>';
		//	Занятия
			$answer .= '<td rowspan="1" colspan="'.$count_groups.'" class="subject-none unactive">';
				$answer .= 'День самостоятельной работы';
			$answer .= '</td>';
			
		$answer .= '</tr>';
		
		return $answer;	
	}
	
	public function ScheduleTableRowDayOff_blockOnly_User($count_groups) {
		
		$answer = '';
		
		$answer .= '<tr>';
		
		//	День
			//	здесь совершенно ничего не нужно
		//	Время
			//	его место занимает надпись "Выходной"
		//	Занятия
			$answer .= '<td rowspan="1" colspan="'.($count_groups + 1).'" class="subject-none unactive">';
				$answer .= 'День самостоятельной работы';
			$answer .= '</td>';
			
		$answer .= '</tr>';
		
		return $answer;
	}
	
	//	Выдает список типов меню управления согласно расписанию текущего дня $rings_current_day
	public function ScheduleTableCol_Ring_getNeededControlType($rings_all, $rings_current_day) {
	//		null) нет элементов управления
	//		0) -
	//		1) + -
	//		2) + - +
	//		3) - +
	
		$answer = array();
		$rings_curr_count = count($rings_current_day);
		$rings_all_count = count($rings_all);
		
		
		$rings_all_first_id = array_shift($rings_all);
		$rings_all_first_id = $rings_all_first_id['Ring']['id'];
		reset($rings_all);
		$rings_all_last_id = end($rings_all);
		$rings_all_last_id = $rings_all_last_id['Ring']['id'];
		reset($rings_all);
		
		if ($rings_curr_count == 1) {
			
			foreach($rings_current_day as $ring_key => $ring_val) {
				
				if ($ring_key == $rings_all_first_id)
					$answer = array(3);
				else if ($ring_key == $rings_all_last_id)
					$answer = array(1);
				else
					$answer = array(2);
			}
			
		} else {
			
			$count = 0;
			
			foreach($rings_current_day as $ring_key => $ring_val) {
				$count++;
				if ($count == 1) {
					if ($ring_key == $rings_all_first_id)
						array_push($answer, 0);
					else
						array_push($answer, 1);
				} else if ($count == $rings_curr_count) {
					if ($ring_key == $rings_all_last_id)
						array_push($answer, 0);
					else
						array_push($answer, 3);
				} else {
					array_push($answer, null);
				}
			}
			
		}
		
		return $answer;
		
	}
	
	//	Элемент строки дня - День
	public function ScheduleTableCol_Day($day, $ring_count) {
		$answer = '';
		
		$answer .= '<td rowspan="'.$ring_count.'" colspan="1" id="day-'.$day['Day']['id'].'" class="day day-header">';
			$answer .= '<div class="vert">'.$day['Day']['name'].'</div>';
		$answer .= '</td>';
		
		return $answer;
	}
	
	//	Элемент строки дня - День
	public function ScheduleTableCol_Day_User($day, $ring_count) {
		$answer = '';
		
		$answer .= '<td rowspan="'.$ring_count.'" colspan="1" class="day day-header">';
			$answer .= '<div class="vert">'.$day['Day']['name'].'</div>';
		$answer .= '</td>';
		
		return $answer;
	}
	
	//	Элемент строки дня - Время
	public function ScheduleTableCol_Time($time, $type, $position) {
	//	$type - тип элементов управления:
	//		null) нет элементов управления
	//		0) -
	//		1) + -
	//		2) + - +
	//		3) - +
	//	$position - позиция среди звонков дня:
	//		top) вверху
	//		bottom) внизу
		
		$answer = '';
		
		$showTime = $time['Ring']['start'].'<br>-<br>'.$time['Ring']['end'];
		
		$answer .= '<td rowspan="1" colspan="1" id="time-'.$time['Ring']['id'].'" class="time">';
		
			if ($type == "0") {	//	-
				
				$answer .= $showTime;
				$answer .= '<div class="col-md-6 table-time-minus '.$position.' one" style="display: none;" title="Удалить занятия"><i class="icon-minus"></i></div>';
				
			} else if ($type == "1") {	//	+ -
				
				$answer .= $showTime;
				$answer .= '<div class="col-md-6 table-time-plus '.$position.'" style="display: none;"><i class="icon-plus"></i></div>';
				$answer .= '<div class="col-md-6 table-time-minus '.$position.'" style="display: none;"><i class="icon-minus"></i></div>';
		
			} else if ($type == "2") {	//	+ - +
	
				$answer .= $showTime;
				$answer .= '<div class="col-md-6 table-time-plus top-one" style="display: none;"><i class="icon-plus"></i></div>';
				$answer .= '<div class="col-md-6 table-time-minus middle-one" style="display: none;"><i class="icon-minus"></i></div>';
				$answer .= '<div class="col-md-6 table-time-plus bottom-one" style="display: none;"><i class="icon-plus"></i></div>';
				
			} else if ($type == "3") {	//	- +
				
				$answer .= $showTime;
				$answer .= '<div class="col-md-6 table-time-minus '.$position.'" style="display: none;"><i class="icon-minus"></i></div>';
				$answer .= '<div class="col-md-6 table-time-plus '.$position.'" style="display: none;"><i class="icon-plus"></i></div>';
				
			} else if ($type == null) {	//	нет элементов управления
				
				$answer .= $showTime;
				
			} 
		
		$answer .= '</td>';
		
		return $answer;
	}
	
	public function ScheduleTableCol_Time_User($time) {
	//	$type - тип элементов управления:
	//		null) нет элементов управления
	//		0) -
	//		1) + -
	//		2) + - +
	//		3) - +
	//	$position - позиция среди звонков дня:
	//		top) вверху
	//		bottom) внизу
		
		$answer = '';
		
		$showTime = $time['Ring']['start'].'<br>-<br>'.$time['Ring']['end'];
		
		$answer .= '<td rowspan="1" colspan="1" class="time">';
		
			$answer .= $showTime;
		
		$answer .= '</td>';
		
		return $answer;
	}
	
	//	Элемент строки дня - Время (пустое)
	public function ScheduleTableCol_Time_empty() {
		$answer = '';
			
		$answer .= '<td rowspan="1" colspan="1" id="time-no"	class="time">';
			$answer .= '<div class="col-md-6 table-time-plus top one" style="display: none;"><i class="icon-plus"></i></div>';
		$answer .= '</td>';
		
		return $answer;
	}
	
	public function ScheduleTableCol_Lesson($lesson, $group_count, $lesson_count, $isVisible = true) {
		$answer = '';
		
		$subject_up = '';
		$teacher_up = '';
		
		$subject_down = '';
		$teacher_down = '';
		
		$group_id = null;
		
		$lesson_up_info = array();
		$lesson_down_info = array();
		
		if (!empty($lesson['up'])) {	//	Вывод занятия для группы по числителю
			
			$subject_up = @$lesson['up']['Lesson']['Subject']['name'];
			$teacher_up = @$lesson['up']['Lesson']['Teacher']['name'];
			
			if (is_numeric($lesson['up']['Lesson']['group_id'])) {
				$group_id = $lesson['up']['Lesson']['group_id'];
			}
			
			$lesson_up_info = $lesson['up']['Lesson'];
			$lesson_down_info = $lesson_up_info;
			$lesson_down_info['id'] = "";
		} else {
			$lesson_up_info['group_id'] = "no";
		}
		
		if (!empty($lesson['down'])) {	//	Вывод занятия для группы по знаменателю
			
			$subject_down = @$lesson['down']['Lesson']['Subject']['name'];
			$teacher_down = @$lesson['down']['Lesson']['Teacher']['name'];
			
			if (is_numeric($lesson['down']['Lesson']['group_id'])) {
				$group_id = $lesson['down']['Lesson']['group_id'];
			}
			
			$lesson_down_info = $lesson['down']['Lesson'];
			if (empty($lesson_up_info)) {
				$lesson_up_info = $lesson_down_info;
			}
			
		} else {
			$lesson_down_info['group_id'] = "no";
		}
		
		$lesson_up_info['type'] = "0";
		$lesson_down_info['type'] = "1";
		
		$global_lesson = false;
		if ($group_count == $lesson_count) {
			
			if ($isVisible)
				$answer .= '<td rowspan="1" colspan="1" group="'.$group_id.'" class="subject">';
			else
				$answer .= '<td rowspan="1" colspan="1" group="'.$group_id.'" class="subject" style="display: none;">';
		} else {
			$global_lesson = true;
			
			if ($isVisible)
				$answer .= '<td rowspan="1" colspan="'.$group_count.'" group="'.$group_id.'" class="subject">';
			else
				$answer .= '<td rowspan="1" colspan="'.$group_count.'" group="'.$group_id.'" class="subject" style="display: none;">';
		}

			//	Числитель/Знаменатель
			$answer .= '<table class="inner-table">';					
				$answer .= '<tr class="table-subject-up">';
					$answer .= '<td>';
						$answer .= $this->ScheduleTableCellSubject($subject_up, $lesson_up_info, $global_lesson);
						
						if ($subject_up != "")
							$answer .= $this->ScheduleTableCellTeacherName($teacher_up, $lesson_up_info, $global_lesson, true);
						else
							$answer .= $this->ScheduleTableCellTeacherName($teacher_up, $lesson_up_info, $global_lesson, false);
							
						$answer .= $this->ScheduleTableCellLessonID($lesson_up_info);
					$answer .= '</td>';
				$answer .= '</tr>';
				
				
				if ($subject_down == '')
					$answer .= '<tr class="table-subject-down" style="display: none;">';
				else 
					$answer .= '<tr class="table-subject-down">';
					
					
					$answer .= '<td>';
						$answer .= $this->ScheduleTableCellSubject($subject_down, $lesson_down_info, $global_lesson);
						
						if ($subject_down != "")
							$answer .= $this->ScheduleTableCellTeacherName($teacher_down, $lesson_down_info, $global_lesson, true);
						else
							$answer .= $this->ScheduleTableCellTeacherName($teacher_down, $lesson_down_info, $global_lesson, false);
						
						$answer .= $this->ScheduleTableCellLessonID($lesson_down_info);
					$answer .= '</td>';
				$answer .= '</tr>';
			$answer .= '</table>';
			//	конец: Числитель/Знаменатель
			
			//	Кнопки управления
			//	$answer .= '<div class="table-subject-delete" style="display: none;"><i class="fa fa-trash"></i></i></div> -->
			
			//	ОБЪЕДИНЕНИЕ ПРЕДМЕТОВ
			if ($group_count == $lesson_count)
				$answer .= '<div class="table-subject-combine" style="display: none;"><i class="fa fa-arrows-h"></i></i></div>';
			else
				$answer .= '<div class="table-subject-combine pressed" style="display: none;"><i class="fa fa-arrows-h"></i></i></div>';
				
			//	ДОБАВЛЕНИЕ ЗНАМЕНАТЕЛЯ
			if ($subject_down == '')
				$answer .= '<div class="table-subject-plus" style="display: none;"><i class="fa fa-plus-square-o"></i></div>';
			else
				$answer .= '<div class="table-subject-minus" style="display: none;"><i class="fa fa-minus-square-o"></i></div>';
			//	конец: Кнопки управления
			
		$answer .= '</td>';
		
		
		return $answer;
	}
	
	public function ScheduleTableCol_Lesson_User($lesson, $group_count, $lesson_count, $isVisible = true) {
		$answer = '';
		
		$subject_up = '';
		$teacher_up = '';
		
		$subject_down = '';
		$teacher_down = '';
		
		$group_id = null;
		
		if (!empty($lesson['up'])) {	//	Вывод занятия для группы по числителю
			
			$subject_up = @$lesson['up']['Lesson']['Subject']['name'];
			$teacher_up = @$lesson['up']['Lesson']['Teacher']['name'];
			
			if (is_numeric($lesson['up']['Lesson']['group_id'])) {
				$group_id = $lesson['up']['Lesson']['group_id'];
			}
		}
		
		if (!empty($lesson['down'])) {	//	Вывод занятия для группы по знаменателю
			
			$subject_down = @$lesson['down']['Lesson']['Subject']['name'];
			$teacher_down = @$lesson['down']['Lesson']['Teacher']['name'];
			
			if (is_numeric($lesson['down']['Lesson']['group_id'])) {
				$group_id = $lesson['down']['Lesson']['group_id'];
			}
			
		}
		
		if ($group_count == $lesson_count) {
			
			if ($isVisible)
				$answer .= '<td rowspan="1" colspan="1" class="subject">';
			else
				$answer .= '<td rowspan="1" colspan="1" class="subject" style="display: none;">';
		} else {
			
			if ($isVisible)
				$answer .= '<td rowspan="1" colspan="'.$group_count.'" class="subject">';
			else
				$answer .= '<td rowspan="1" colspan="'.$group_count.'" class="subject" style="display: none;">';
		}

			//	Числитель/Знаменатель
			$answer .= '<table class="inner-table">';					
				$answer .= '<tr class="table-subject-up">';
					$answer .= '<td>';
						$answer .= $this->ScheduleTableCellSubject_User($subject_up, $teacher_up == null);
						
						if ($subject_up != "")
							$answer .= $this->ScheduleTableCellTeacherName_User($teacher_up, true);
						else
							$answer .= $this->ScheduleTableCellTeacherName_User($teacher_up, false);
							
					$answer .= '</td>';
				$answer .= '</tr>';
				
				
				if ($subject_down == '')
					$answer .= '<tr class="table-subject-down" style="display: none;">';
				else 
					$answer .= '<tr class="table-subject-down">';
					
					
					$answer .= '<td>';
						$answer .= $this->ScheduleTableCellSubject_User($subject_down, $teacher_down == null);
						
						if ($subject_down != "")
							$answer .= $this->ScheduleTableCellTeacherName_User($teacher_down, true);
						else
							$answer .= $this->ScheduleTableCellTeacherName_User($teacher_down, false);
						
					$answer .= '</td>';
				$answer .= '</tr>';
			$answer .= '</table>';
			//	конец: Числитель/Знаменатель
			
		$answer .= '</td>';
		
		
		return $answer;
	}
	
	//	конец: РАСПИСАНИЕ ---------------------------------------------------------------------------------------------
	//	---------------------------------------------------------------------------------------------------------------
}
?>