<?php
	$options = array(
		array(
            'name' => 'Выберите курс',
            'value' => '',
            'disabled' => TRUE,
            'selected' => TRUE
        ),
        '1' => '1',
		'2' => '2',
		'3' => '3',
		'4' => '4',
		'5' => '5',
		'6' => '6'
    );
	
	echo $this->Form->input('course', array(
		'type'=>'select',
		'options' => $options,
		'label'=>false,
		'class'=>'cs-select cs-skin-border select-course',
		'id' => 'select_Course'
	));
	
?>

<script>
	(function() {
		[].slice.call( document.querySelectorAll( 'select.cs-select.select-course' ) ).forEach( function(el) {	
			new SelectFx(el, {onChange: step4_showTable });
		} );
	})();
</script>