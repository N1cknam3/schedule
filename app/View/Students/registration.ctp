<?php 
	
	$data1 = $this->Js->get('#RegistrationForm')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
    $this->Js->get('#RegistrationForm')->event(
          'submit',
          $this->Js->request(
            array(
	            'controller' => 'students',
            	'action' => 'reg_ajax'
            ),
            array(
                    'update' => '#status_reg', // element to update
                                             			// after form submission
                    'complete' => "formRegistration_success()",
                    //'success' => $this->Js->redirect('/admin'),
                    'data' => $data1,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );	
	
?>

<h1 class="registration">Регистрация</h1>

<div id="registration">
	
	 <?php echo $this->Form->create('Guest', array('url'=>array('controller'=>'students', 'action'=>'cabinet'), 'id' => 'RegistrationForm')); ?>
	 <?php echo $this->Form->input('name', array('label'=>'Ваше имя', 'placeholder' => 'Иван', 'class' => 'form-control', 'autocomplete'=>'off', 'id' => 'NameReg')); ?>
	 	 <?php echo $this->Form->input('last_name', array('label'=>'Ваша фамилия', 'placeholder' => 'Иванов', 'class' => 'form-control', 'autocomplete'=>'off', 'id' => 'LastNameReg')); ?>
	 	 <?php echo $this->Form->input('email', array('label'=>'Электронная почта', 'placeholder' => 'ivanov@mail.com', 'class' => 'form-control', 'autocomplete'=>'off', 'id' => 'EmailReg')); ?>
	 	 <?php echo $this->Form->input('pass', array('type'=>'password', 'label'=>'Пароль', 'placeholder' => '***', 'class' => 'form-control', 'autocomplete'=>'off', 'id' => 'PassReg')); ?>
	 	 <?php echo $this->Form->input('pass2', array('type'=>'password', 'label'=>'Пароль еще раз', 'placeholder' => '***', 'class' => 'form-control', 'autocomplete'=>'off', 'id' => 'Pass2Reg')); ?>
	 	 <?php echo $this->Form->input('captcha', array('label' => $captcha, 'class' => 'form-control')); ?>
	 	 <?php echo $this->Form->submit('Зарегистрироваться', array('class'=>'btn btn-info btn-lg', 'div'=>false, 'default' => false)); ?>
	 	 <div class="clear"></div>
	 	 <!-- <?php echo $this->Form->submit('Зарегистрироваться', array('class'=>'btn btn-info btn-lg', 'div'=>false)); ?> -->
	 	 <?php /* echo $this->Form->button('Зарегистрироваться', array('class'=>'btn btn-info btn-lg', 'div'=>false, 'type' => 'button', 'id' => 'customSubmitButton'));  */?>
	 <?php echo $this->Form->end(); ?>
	 <div class="clear"></div>
	 <div>*Все поля обязательны для заполнения</div>
	 <?php echo $this->Js->writeBuffer(); ?>
	 <div id="status_reg" class="status"></div>
</div>