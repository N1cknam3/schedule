<div id="data">
	<div id="facultiesContainer">
		<!-- Место вывода selectBox'а выбора факультета -->
		<?php
			$options = array(
				array(
		            'name' => 'Выберите факультет',
		            'value' => '',
		            'disabled' => TRUE,
		            'selected' => TRUE
		        )
		    );
		    $options += $faculties;
		    
			echo $this->Form->input('faculty_id', array(
				'type'=>'select',
				'options' => $options,
				'label'=>false,
				'class'=>'cs-select cs-skin-border select-faculty',
				'id' => 'select_FacultyId',
				'style' => 'display: none;'
			));
		?>
		<script>
			(function() {
				[].slice.call( document.querySelectorAll( 'select.cs-select.select-faculty' ) ).forEach( function(el) {	
					new SelectFx(el, {onChange: step2_showSpecialities });
					$("#fullname").html('');
				} );
			})();
		</script>
	</div>
	
	<div id="specialitiesContainer" style="display: none; margin-top: 15px;">
		<!-- Место вывода selectBox'а выбора специальности -->
	</div>
	
	<div id="courseContainer" style="display: none; margin-top: 15px;">
		<!-- Место вывода selectBox'а выбора курса -->
	</div>
	
	<div id="tableContainer" style="display: none; margin-top: 15px;">
		<!-- Место вывода таблицы -->
	</div>
	
	<div id="fullname"></div>
</div>