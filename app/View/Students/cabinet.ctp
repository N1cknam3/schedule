<?php
// 	debug($schedules);

//	Логика выбора открытой вкладки
$openSchedule = false;
if (!empty($tables_data)) {
	$openSchedule = true;
}

?>

<div class="container student-cabinet-content footer-padding">
  
  <!--LOGO-->
  <div class="login-form-logo title-buttons">
	  <?php
		  if ($openSchedule) {
		  	echo '<a href="#" class="title-button btn hvr-underline-from-center student-cabinet-tab_1 active">Расписание</a>';
		  	echo '<a href="#" class="title-button btn hvr-underline-from-center student-cabinet-tab_2">Управление</a>';
		  } else {
			echo '<a href="#" class="title-button btn hvr-underline-from-center student-cabinet-tab_1">Расписание</a>';
		  	echo '<a href="#" class="title-button btn hvr-underline-from-center student-cabinet-tab_2 active">Управление</a>';
		  }
	  ?>
	 
  </div>
  <!--LOGO-->
  <div id="title-content-container">
	  
	  
	  <?php
		  if ($openSchedule)
		  	echo '<div class="title-content student-cabinet-content_1">';
		  else
		  	echo '<div class="title-content student-cabinet-content_1" style="display: none">';
	  ?>
	      <div class="widget student-schedule-list">
		      
		    <!-- empty Widget content for list content -->
		    <div class="widget-content col-md-12 row">
	        	
	        </div>
		      
			<?php
				echo $this->Widgets->ScheduleTable_User_list($days, $rings, $tables_data);
			?>
		      
	        <!-- empty Widget content for list content -->
	        <div class="widget-content col-md-12 row">
	        	
	        </div>
	        
	      </div>
	  </div>
	  
	  <?php
		  if (!$openSchedule)
		  	echo '<div class="title-content student-cabinet-content_2">';
		  else
		  	echo '<div class="title-content student-cabinet-content_2" style="display: none">';
	  ?>
	      <div class="widget">
		      
	        <!-- Widget content -->
	        <div class="widget-content col-md-12 row title-content student-cabinet-content_2">
	
		        <?php
		        	echo $this->StudentCabinet->NewSchedule($schedules, $faculties);
		        ?>
	
		        <div class="schedule-student-row-footer" style="clear: both; padding: 15px 0px 0px">
	
		        	<div id="speciality-controls" style="float: left; padding-left: 15px; padding-right: 15px;">
						<div class="speciality-add speciality-control-button">
							<i class="icon-plus"></i>	
						</div>
					</div>
	        	
		        </div>
	        	
	        </div>
	        
	        <!-- Widget footer -->
	        <!-- <div class="widget-foot" align="right">Powered by SupportHub</div> -->
	        
	      </div>
	  </div>
	  
	  
  
  </div>
</div>