<?php
	$options = array(
		array(
            'name' => 'Выберите специальность',
            'value' => '',
            'disabled' => TRUE,
            'selected' => TRUE
        )
    );
    
    if (isset($specialitiesArray))
    	$options += $specialitiesArray;
	
	echo $this->Form->input('course', array(
		'type'=>'select',
		'options' => $options,
		'label'=>false,
		'class'=>'cs-select cs-skin-border select-specialities',
		'id' => 'select_SpecialityId'
	));
?>

<script>
	(function() {
		[].slice.call( document.querySelectorAll( 'select.cs-select.select-specialities' ) ).forEach( function(el) {	
			new SelectFx(el, {onChange: step4_showTable });
		} );
	})();
</script>