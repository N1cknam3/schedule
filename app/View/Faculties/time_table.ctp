<div class="col-md-12">
	
	<div class="row fullscreen-mode widget-content dashboard-widget schedule-widget">
		
		<?php
			echo $this->Form->create('Faculty', array('action' => 'choose_schedule_group', 'default' => false, 'id' => 'ScheduleChooseForm'));
		?>
		
		<div class="col-md-2">
			<?php						
				echo '<div id="ajax_reload_step0_ChooseCourse">' . $this->Widgets->ChooseCourse() . '</div>';
			?>			
		</div>
		
		<div class="col-md-2">
			<?php		
				echo '<div id="ajax_reload_step1_ChooseSpeciality"></div>';		
			?>			
		</div>
		
		<?php
			echo $this->Form->end();
		?>
		
	</div>
	
	<div id="schedule_ajax_table" class="row fullscreen-mode widget-content dashboard-widget schedule-widget" style="display: none;">
		
		<?php
			echo $this->Form->create('Faculty', array('action' => 'set_schedule_table', 'default' => false, 'id' => 'ScheduleTableForm'));
		?>
		
		<div class="col-md-12">	
			<?php	
				//	Сюда выводится таблица через ajax
				//	faculties/schedule_step2_showTable
				echo '<div id="ajax_reload_step2_GetTable"></div>';
			?>
		</div>
		
		<?php
			echo $this->Form->end();
		?>
		
	</div>
	
	<?php
		echo $this->Js->writeBuffer();
	?>
	
	<div id="table-result">
		
	</div>

</div>