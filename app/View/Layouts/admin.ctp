
<!DOCTYPE html>
<html lang="en">

<!--HEAD - ONLOAD-->
<head>
	
<!--Адаптация разрешения под разрешение мобильного устройства-->	
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php echo $this->Html->charset(); ?>

<title><?php echo $this->fetch('title'); ?></title>

<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css('all-vendors');
	echo $this->Html->css('style-admin');
	echo $this->Html->css('responsive');

	echo $this->Html->css('font-awesome');
	echo $this->Html->css('animate');
	
	echo $this->Html->css('style-schedule-table');
	
	echo $this->Html->scriptBlock('var jsVars = '.$this->Js->object($jsVars).';');
	
	echo $this->Html->script('jquery-1.7.1.min');
	echo $this->Html->script('jquery.form');
	echo $this->Html->script('functions');
	echo $this->Html->script('functions_admin');	
	//скрипт для маски ввода
	//информация: http://digitalbush.com/projects/masked-input-plugin/
	echo $this->Html->script('masked-input-plugin');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	
?>

</head>


<body class="admin">
	
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32389445 = new Ya.Metrika({
                    id:32389445,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32389445" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->	
	
<?php 
	
	$data1 = $this->Js->get('#SpecialityFormEdit')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
    $this->Js->get('#SpecialityFormEdit')->event(
          'submit',
          $this->Js->request(
            array('action' => 'speciality_ajax_edit'),
            array(
                    'update' => '#ajax_reload_content', // element to update
                                             			// after form submission
                    'complete' => "ajax_speciality_edit_check()",
                    //'success' => $this->Js->redirect('/admin'),
                    'data' => $data1,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );
        
    $data2 = $this->Js->get('#SpecialityFormAdd')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
    $this->Js->get('#SpecialityFormAdd')->event(
          'submit',
          $this->Js->request(
            array('action' => 'speciality_ajax_add'),
            array(
                    'update' => '#ajax_reload_content', // element to update
                                             			// after form submission
                    'complete' => "ajax_speciality_add_check()",
                    //'success' => $this->Js->redirect('/admin'),
                    'data' => $data2,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );
        
    $data3 = $this->Js->get('#ChairAddForm')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
    $this->Js->get('#ChairAddForm')->event(
          'submit',
          $this->Js->request(
            array('action' => 'chair_ajax_add'),
            array(
                    'update' => '#ajax_reload_content', // element to update
                                             			// after form submission
                    'complete' => "ajax_chair_add_check()",
                    'data' => $data3,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );
        
    $data4 = $this->Js->get('#ChairEditForm')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
    $this->Js->get('#ChairEditForm')->event(
          'submit',
          $this->Js->request(
            array('action' => 'chair_ajax_edit'),
            array(
                    'update' => '#ajax_reload_content', // element to update
                                             			// after form submission
                    'complete' => "ajax_chair_edit_check()",
                    'data' => $data4,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );
	
?>	

<?php echo $this->Session->flash(); ?>

	<div class="content">
		
	<!-- СПЕЦИАЛЬНОСТИ -->
	<div id="myModalEdit" class="modal fade login in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
	    <div class="modal-dialog">
		    
		    <?php echo $this->Form->create('Faculty', array('action' => 'speciality_ajax_edit', 'default' => false, 'id' => 'SpecialityFormEdit')); ?>
			    	
			    <div class="modal-content">
			    	
			        <div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:closeEditForm()">x</button>
				        <h4 class="modal-title">
				        	Форма изменения</h4>
			        </div>
			        
			        <div class="modal-body">
						
						<div class="box">
							<?php
							echo $this->Form->input('code', array(
							    'label' => false,
							    'type' => 'text',
							    'placeholder' => 'Код',
							    'value' => @$FormCodeValue,
							    'class' => 'form-control FacultyCodeInput',
							    'id' => 'editSpecialityCode'
							    
							));
							?>
						</div>
						<div class="box">
							<?php
							echo $this->Form->input('name', array(
							    'label' => false,
							    'type' => 'text',
							    'placeholder' => 'Название',
							    'value' => @$FormNameValue,
							    'class' => 'form-control',
							    'id' => 'editSpecialityName'
							));
							?>
						</div>
						<div class="box">
							<?php
							echo $this->Form->input('profile', array(
							    'label' => false,
							    'type' => 'text',
							    'placeholder' => 'Профиль',
							    'value' => @$FormProfileValue,
							    'class' => 'form-control',
							    'id' => 'editSpecialityProfile'
							));
							?>
						</div>
						
						<?php echo $this->Form->input('id', array('type'=>'hidden')); ?>
						<?php echo $this->Form->input('type', array('type'=>'hidden', 'id' => 'editSpecialityType')); ?>
					
						<div id="formAddEditContent"></div>
						
						<div id="status_edit" class="status"><div id="message_edit" class="message"></div></div>
						
			        </div>
			        
			        <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" onclick="javascript:closeEditForm()">
						Закрыть
						</button>
						<!-- <input class="btn btn-primary" type="submit" value="Восстановить" id="" name="submit"> -->
						<?php 
							echo $this->Form->submit('Сохранить', array('class'=>'btn btn-primary', 'div'=>false));
							//echo $this->Js->writeBuffer();
							?>
			        </div>
		        
			    </div>
			        
		    <!-- </form> -->
		    <?php echo $this->Form->end(); ?>
	    </div>    
	</div>
	
	<div id="myModalAdd" class="modal fade login in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
	    <div class="modal-dialog">
		    
		    <?php echo $this->Form->create('Faculty', array('action' => 'speciality_ajax_add', 'default' => false, 'id' => 'SpecialityFormAdd')); ?>
			    	
			    <div class="modal-content">	
			    
			        <div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:closeAddForm()">x</button>
				        <h4 class="modal-title">
				        	Форма изменения</h4>
			        </div>
			        
			        <div class="modal-body">
						
						<div class="box">
							<?php
							echo $this->Form->input('code', array(
							    'label' => false,
							    'type' => 'text',
							    'placeholder' => 'Код',
							    'value' => @$FormCodeValue,
							    'class' => 'form-control FacultyCodeInput',
							    'id' => 'addSpecialityCode'
							));
							?>
						</div>
						<div class="box">
							<?php
							echo $this->Form->input('name', array(
							    'label' => false,
							    'type' => 'text',
							    'placeholder' => 'Название',
							    'value' => @$FormNameValue,
							    'class' => 'form-control',
							    'id' => 'addSpecialityName'
							));
							?>
						</div>
						<div class="box">
							<?php
							echo $this->Form->input('profile', array(
							    'label' => false,
							    'type' => 'text',
							    'placeholder' => 'Профиль',
							    'value' => @$FormProfileValue,
							    'class' => 'form-control',
							    'id' => 'addSpecialityProfile'
							));
							?>
						</div>
						
						<?php echo $this->Form->input('type', array('type'=>'hidden', 'id' => 'addSpecialityType')); ?>
						
						<div id="status_add" class="status"><div id="message_add" class=""></div></div>
						
			        </div>
			        
			        <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" onclick="javascript:closeAddForm()">
						Закрыть
						</button>
						<!-- <input class="btn btn-primary" type="submit" value="Восстановить" id="" name="submit"> -->
						<?php 
							echo $this->Form->submit('Сохранить', array('class'=>'btn btn-primary', 'div'=>false));
							//echo $this->Js->writeBuffer();
			        	?>
			        </div>
		        
			    </div>
			        
		    <!-- </form> -->
		    <?php echo $this->Form->end(); ?>
	    </div>    
	</div>
	
	<!-- КАФЕДРЫ -->
	<div id="myModalChairAdd" class="modal fade login in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
	    <div class="modal-dialog">
		    
		    <?php echo $this->Form->create('Chair', array('action' => 'chair_ajax_add', 'default' => false, 'id' => 'ChairAddForm')); ?>
			    	
			    <div class="modal-content">
				    
			        <div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:closeChairAddForm()">x</button>
				        <h4 class="modal-title">
				        	Форма добавления</h4>
			        </div>
			        
			        <div class="modal-body">
						
						<div class="box">
							<?php
							echo $this->Form->input('name', array(
							    'label' => false,
							    'type' => 'text',
							    'placeholder' => 'Название',
							    'class' => 'form-control',
							    'id' => 'addChairName'
							));
							?>
						</div>
						
						<div id="status_chair_add" class="status"><div id="message_chair_add" class=""></div></div>
						
			        </div>
			        
			        <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" onclick="javascript:closeChairAddForm()">
						Закрыть
						</button>
						<!-- <input class="btn btn-primary" type="submit" value="Восстановить" id="" name="submit"> -->
						<?php 
							echo $this->Form->submit('Сохранить', array('class'=>'btn btn-primary', 'div'=>false));
							?>
			        </div>
		        
			    </div>
			        
		    <!-- </form> -->
		    <?php echo $this->Form->end(); ?>
	    </div>    
	</div>

	<div id="myModalChairEdit" class="modal fade login in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
	    <div class="modal-dialog">
		    
		    <?php echo $this->Form->create('Chair', array('action' => 'chair_ajax_edit', 'default' => false, 'id' => 'ChairEditForm')); ?>
		    
		    	<div class="modal-content">
			    	
			        <div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:closeChairEditForm()">x</button>
				        <h4 class="modal-title">
				        	Форма изменения кафедры</h4>
			        </div>
			        
			        <div class="modal-body">
						
						<div class="box">
							<?php
							echo $this->Form->input('name', array(
							    'label' => false,
							    'type' => 'text',
							    'placeholder' => 'Название',
							    'class' => 'form-control',
							    'id' => 'editChairName'
							));
							?>
						</div>
						
						<?php echo $this->Form->input('id', array('type'=>'hidden')); ?>
						<div id="status_chair_edit" class="status"><div id="message_chair_edit" class=""></div></div>
						
			        </div>
			        
			        <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" onclick="javascript:closeChairEditForm()">
						Закрыть
						</button>
						<?php 
							echo $this->Form->submit('Сохранить', array('class'=>'btn btn-primary', 'div'=>false));
						?>
			        </div>
			        
		    	</div>
		    <!-- </form> -->
		    <?php echo $this->Form->end(); ?>
	    </div>    
	</div>
	
	<div class="sidebar">
		<div class="sidebar-dropdown">
			<a id="btn-main" href="javascript:dropDownMenu()" class="">Расписание</a>
			
		</div>
		<ul id="nav" class="main-nav" style="">
			<div class="nav_logo">
				<img class="img-responsive" src="<?php echo $this->webroot; ?>img/logo.png" alt="Расписание КГУ">
			</div>
			
			<li class="nav_alternative"> 
<!--
		    	<ul class="nav_alternative_controls" tabindex="-1" data-reactid=".1.0.1.0">
					<li class="url-link " data-link="#"><i class="icon-file-text"></i></li>
					<li class="url-link " data-link="#"><i class="icon-list-ul"></i> </li>
					<li class="url-link " data-link="#"><i class="icon-time"></i></li>
					<li class="url-link " data-link="#"><i class="icon-sitemap"></i></li>
					<li class="url-link " data-link="#"><i class="icon-wrench"></i></li>
		    	</ul>
-->
			</li>
			
			<li>
				<?php					
					echo $this->Html->link(
						'<i class="icon-home"></i> Главная',
						array(
							'controller' => 'faculties',
							'action' => 'admin'
						),
						array(
							'escape' => false,
							'class' => @$mainClass
						)
					);
				?>
			</li>
			
			<li>
				<?php					
					echo $this->Html->link(
						'<i class="icon-calendar"></i> Расписание',
						array(
							'controller' => 'faculties',
							'action' => 'timeTable'
						),
						array(
							'escape' => false,
							'class' => @$timeTableClass
						)
					);
				?>
			</li>
			
			<li class="has_sub"> <a id="dropdownButton1" class="<?php echo @$dropdownButton1Class; ?>" href="javascript:dropdownButton()">  
			    <i class="icon-list"></i> Специальности<span class="pull-right"><i id="dropdownIcon1" class="<?php echo @$dropdownIcon1Class; ?>" style="font-size:12px"></i></span></a>
			    <ul id="dropdownList11" style="<?php echo @$dropdownList1Style; ?>">
				    
				    <li>
					<?php
					echo $this->Html->link(
						' Бакалавр',
						array(
							'controller' => 'faculties',
							'action' => 'specialities/0'
						),
						array(
							'escape' => false,
							'class' => @$specialityClass
						)
					);
					?>
				    </li>
				    
				    <li>
					<?php
					echo $this->Html->link(
						' Магистр',
						array(
							'controller' => 'faculties',
							'action' => 'specialities/1'
						),
						array(
							'escape' => false,
							'class' => @$specialityClass
						)
					);
					?>
				    </li>
				    
				    <li>
					<?php
					echo $this->Html->link(
						' Специалитет',
						array(
							'controller' => 'faculties',
							'action' => 'specialities/2'
						),
						array(
							'escape' => false,
							'class' => @$specialityClass
						)
					);
					?>
				    </li>
				    
			    </ul>
			  </li>
			
			<li>
				<?php					
					echo $this->Html->link(
						'<i class="icon-group"></i> Кафедры',
						array(
							'controller' => 'faculties',
							'action' => 'chairs'
						),
						array(
							'escape' => false,
							'class' => @$chairsClass
						)
					);
				?>
			</li>
			
		</ul>
	</div>
	
	<div class="mainbar">
		<?php echo $this->element('admin-header'); ?>
		<div class="matter">
	
			<?php echo $this->element('admin-title'); ?>
			
			<div class="container" id="ajax_reload_content">
				<?php echo $this->fetch('content'); ?>
			</div>
			
			<?php
				echo $this->Form->create('Hidden', array('default' => false, 'id' => 'HiddenFormDelete', 'style' => 'display: none;'));
				echo $this->Form->input('elementIdToDelete', array('type'=>'hidden', 'id' => 'elementIdToDelete', 'style' => 'display: none;'));
				echo $this->Form->input('elementTypeToDelete', array('type'=>'hidden', 'id' => 'elementTypeToDelete', 'style' => 'display: none;'));
				echo $this->Form->end();
			?>
			
		</div>
	</div>
</div>

<div class="clearfix"></div>

<?php
	
	echo $this->element('footer');

	echo $this->Js->writeBuffer();

 ?>

</body>
</html>
