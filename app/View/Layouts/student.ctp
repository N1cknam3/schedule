
<!DOCTYPE html>
<html lang="en">

<!--HEAD - ONLOAD-->
<head>
	
<!--Адаптация разрешения под разрешение мобильного устройства-->	
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php echo $this->Html->charset(); ?>

<title><?php echo $this->fetch('title'); ?></title>

<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css('all-vendors');
	echo $this->Html->css('style');
	echo $this->Html->css('style2');
	echo $this->Html->css('responsive');
	echo $this->Html->css('normalize');
	echo $this->Html->css('cs-select');
	echo $this->Html->css('cs-skin-border');
	echo $this->Html->css('font-awesome');
	echo $this->Html->css('animate');
	echo $this->Html->css('colorbox');
	
	echo $this->Html->css('style-schedule-table');
	echo $this->Html->css('table-print');
	
	echo $this->Html->script('jquery-1.7.1.min');
	echo $this->Html->script('jquery.form');
	echo $this->Html->script('functions');
	echo $this->Html->script('classie');
	echo $this->Html->script('selectFx');
	echo $this->Html->script('jquery.colorbox');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
?>

<!--[if lt IE 9]>
<script>
document.createElement('video');
</script>
<![endif]-->

</head>

<!--HEAD - ONLOAD-->
<body class="login body-login">
	
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32389445 = new Ya.Metrika({
                    id:32389445,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32389445" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->	

<?php
    $data3 = $this->Js->get('#LoginForm')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
    $this->Js->get('#LoginForm')->event(
          'submit',
          $this->Js->request(
            array(
	            'controller' => 'students',
            	'action' => 'ajax_login'
            ),
            array(
                    'update' => '#status_login', // element to update
                                             			// after form submission
                    'complete' => "formLogin_success()",
                    'data' => $data3,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );
?>

<video muted autoplay loop id="bgvid">
<source src="<?php echo $this->html->url('/', true); ?>/video_bg.mp4" type="video/mp4">
<source src="<?php echo $this->html->url('/', true); ?>/video_bg.webm" type="video/webm">
</video>

<div id="bg-overlay"></div>

<div class="page-head">	

	<div class="reg_link">
		<i class="fa fa-user"></i> 
		<!-- <a href="#" onmouseenter="show_login()" onclick="return false;">Войти</a> -->
		
		<!-- <a href="#" class="tooltip">Войти<span>Подсказочка</span></a> -->
		
		<span id="login_box" class="my-thumbnail anim" tabindex="0" onclick="show_login()">
			<span href="#" style="text-decoration: underline; cursor: pointer; ">Войти</span>
			<div class="showable">
				<?php echo $this->Form->create('User', array('url'=>array('controller'=>'students', 'action'=>'cabinet'), 'default' => false, 'id' => 'LoginForm')); ?>
				<?php echo $this->Form->input('login', array('label'=>false, 'autocomplete' => 'off', 'placeholder' => 'happystudent@mail.ru', 'style' => 'width: 100%; padding: 0 10px 0;', 'div'=>'row my-form-input-div')); ?>
				<?php echo $this->Form->input('pass', array('type'=>'password', 'label'=>false, 'autocomplete' => 'off', 'placeholder' => '* * *', 'style' => 'width: 100%; height: 30px !important; padding: 0 10px 0;', 'div'=>'row my-form-input-div')); ?>
				<?php echo $this->Form->submit('Войти', array('class'=>'btn btn-info btn-lg', 'div'=>'row my-form-input-div', 'style' => 'width: 100%; text-transform: uppercase;')); ?>
				<?php echo $this->Form->end(); ?>
				<div id="status_login" class="status main_status_login"></div>
				<div class="my-after-divider-line"></div>
				<div class="row my-form-input-div no-margin" style="color: black;">
					<!-- <div class="col-md-12 no-padding">Войти с помощью</div> -->
					<div class="col-md-12 no-padding">
						<?php
							echo $vk_link = '<div style="width: 50%; float: left; "><a class="social vk_login" href="' . $vk_href . '" title="Войти используя аккаунт Вконтакте"><i class="fa fa-vk" style="width: 85%"></i></a></div>';
							echo $fb_link = '<div style="width: 50%; float: left; "><a class="social fb_login" href="' . $fb_href . '" title="Войти используя аккаунт Facebook"><i class="fa fa-facebook-official" style="width: 85%"></i></a></div>';
						?>
					</div>
				</div>
			</div>
		</span>
				
				<?php					
					echo $this->Html->link(
						'Регистрация',
						array(
							'controller' => 'students',
							'action' => 'registration'
						),
						array(
							'escape' => false,
							'class' => @$mainClass
						)
					);
				?>
	</div>
	
	
</div>

<div id="content">

<?php echo $this->Session->flash(); ?>

<?php echo $this->fetch('content'); ?>

<?php echo $this->element('footer'); ?>

<?php echo $this->Js->writeBuffer(); ?>
</div>

</body>
</html>