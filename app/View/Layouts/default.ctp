
<!DOCTYPE html>
<html lang="en">

<!--HEAD - ONLOAD-->
<head>
	
<!--Адаптация разрешения под разрешение мобильного устройства-->	
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php echo $this->Html->charset(); ?>

<title><?php echo $this->fetch('title'); ?></title>

<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css('all-vendors');
	echo $this->Html->css('style');
	echo $this->Html->css('responsive');
	
	echo $this->Html->css('font-awesome');
	echo $this->Html->css('animate');
	
	echo $this->Html->script('jquery-1.7.1.min');
	echo $this->Html->script('jquery.form');
	echo $this->Html->script('functions');
	echo $this->Html->script('functions_admin');
	//скрипт для маски ввода
	//информация: http://digitalbush.com/projects/masked-input-plugin/
	echo $this->Html->script('masked-input-plugin');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
?>

</head>

<!--HEAD - ONLOAD-->
<body class="login body-login">
	
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32389445 = new Ya.Metrika({
                    id:32389445,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32389445" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->	

<!--DEBUG SECTION - ONLOAD-->
<!-- WI_LOGIN_FORM -->
<div class="container login-form login-form-small">
  <!----------------------COMMON NOTICES--------------------------------->
        <!--WI_NOTICE_ERROR-->
      
      <!--WI_NOTICE_ERROR-->
      <!--WI_NOTICE_SUCCESS-->
      
      <!--WI_NOTICE_SUCCESS-->
      <!--WI_NOTICE_ERROR_NOTY-->
      
      <!--WI_NOTICE_ERROR_NOTY-->
      <!--WI_NOTICE_SUCCESS_NOTY-->
      
      <!--WI_NOTICE_SUCCESS_NOTY-->
  <!----------------------COMMON NOTICES--------------------------------->
</div>
<div class="login-background">
<div class="login-overlay"></div>

</div>

<?php echo $this->Session->flash(); ?>

<?php echo $this->fetch('content'); ?>

</body>
</html>
