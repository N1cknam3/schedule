<?php 

	$data = $this->Js->get('#UserLoginForm')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
 
    $this->Js->get('#UserLoginForm')->event(
          'submit',
          $this->Js->request(
            array('action' => 'ajax_login'),
            array(
                    'update' => '#status', // element to update
                                             // after form submission
                    'complete' => "ajax_action()",
                    //'success' => $this->Js->redirect('/admin'),
                    'data' => $data,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );
        
        
    // форма восстановления пароля    
	$data1 = $this->Js->get('#UserPassForm')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
 
    $this->Js->get('#UserPassForm')->event(
          'submit',
          $this->Js->request(
            array('action' => 'reminder_pass'),
            array(
                    'update' => '#status1', // element to update
                                             // after form submission
                    'complete' => "showPassMessage()",
                    'data' => $data1,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                )
            )
        );        

?>

<div id="myModal" class="modal fade login in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
	    <!-- <form class="form-horizontal" action="http://freelancedemo.nextloop.net/admin/login/reminder" method="post" id="ResetPassword" novalidate="novalidate"> -->
	    <?php echo $this->Form->create('User', array('action' => 'reminder_pass', 'default' => false, 'id' => 'UserPassForm')); ?>
	    	<div class="modal-content" id="PassForm">
		    	
		        <div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:closePassForm()">x</button>
			        <h4 class="modal-title">
			        	Восстановить пароль</h4>
		        </div>
		        
		        <div class="modal-body">
					<!-- Email -->
					<div class="form-group" style="margin-bottom:10px; text-align: left;">
						
						<div class="col-lg-12">
							<!-- <input type="text" class="form-control" id="my_emailaddress" placeholder="Email факультета" name="my_emailaddress" autocomplete="off"> -->
							<?php echo $this->Form->input('login', array('label'=>'Email', 'placeholder' => 'Email факультета', 'class' => 'form-control', 'autocomplete'=>'off', 'id' => 'UserLoginToFix')); ?>
						</div>
						<div id="status1" class="status"></div>
					</div>
		        </div>
		        
		        <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" onclick="javascript:closePassForm()">
					Закрыть
					</button>
					<!-- <input class="btn btn-primary" type="submit" value="Восстановить" id="" name="submit"> -->
					<?php 
						echo $this->Form->submit('Восстановить', array('class'=>'btn btn-primary', 'div'=>false));
						echo $this->Js->writeBuffer();
	          		?>
		        </div>
		        
	    	</div>
	    <!-- </form> -->
	    <?php echo $this->Form->end(); ?>
    </div>    
 </div>

<div class="container login-form login-form-small">
  
  <!--LOGO-->
  <div class="login-form-logo"> <?php echo $this->Html->image('logo.png', array('alt' => '', 'border' => '0')) ?></div>
  <!--LOGO-->
  <div class="login-form">
	  <?php echo $this->Form->create('User', array('action' => 'ajax_login', 'default' => false, 'id' => 'UserLoginForm')); ?>

      <div class="widget">
        <!-- Widget head -->
        <div class="widget-content">
        <div class="login-title">Официальное расписание факультетов</div>
          <!-- Login form -->
          <div class="form-group" style="margin-bottom:13px;">
            <div class="col-lg-12">
              <div class="input-group" id="enterLogin"> <span class="input-group-addon">@</span>
                <?php echo $this->Form->input('login', array('label'=>false, 'placeholder' => 'fivt@kursksu.ru', 'class' => 'form-control')); ?>
              </div>
            </div>
          </div>
          <!-- Password -->
          <div class="form-group">
            <div class="col-lg-12">
              <div class="input-group" id="enterPass"> <span class="input-group-addon"><i class="icon-key"></i></span>

                <?php echo $this->Form->input('pass', array('type'=>'password', 'label'=>false, 'placeholder' => '***', 'class' => 'form-control')); ?>
              </div>
            </div>
          </div>
          <div class="form-group" style="padding:12px 0px;">
            <div class="col-lg-12">
	            <a class="btn btn-information btn-lg" href="javascript:showPassForm()" data-toggle="modal">Восстановить пароль</a>
              <?php 
	              echo $this->Form->submit('Войти', array('class'=>'btn btn-info btn-lg', 'div'=>false));
	              echo $this->Js->writeBuffer();
	          ?>
	          <div id="status" class="status"></div>
            </div>
          </div>
        </div>
        <?php echo $this->Form->end(); ?>
        <div class="widget-foot" align="right">Powered by SupportHub</div>
      </div>
    </form>
  </div>
</div>