<div class="page-head">
	<h2 class="pull-left heading-menu">Учебный год: <?php echo $year; ?></h2>
	
	<div class="heading-menu pull-right responsive-heading-tabs">
		<ul>
			<li>
				<div class="btn-group dropup">
					<?php
						echo $this->Html->link(
							'',
							array(
								'controller' => 'students',
								'action' => 'logout'
							),
							array(
								'confirm' => 'Покинуть систему?',
								'class'=>'icon-off'
							)
						);
					?>
				</div>
			</li>
		</ul>
	</div>
</div>