<?php
	$messages = array();
	
	//	0
	array_push($messages, '
		<p style="font-size: 20px;">Мы знаем, как вы любите учиться</p>
		<p style="font-size: 20px;">Но пока что расписание для вас ещё не готово..</p>
		<p style="font-size: 20px;">Но кто-то над этим уже работает :)</p>
	
		<i class="fa fa-cog fa-spin" style="font-size: 50px;"></i>
	');
	
	//	1
	array_push($messages, '
		<p style="font-size: 20px;">Упс!</p>
		<p style="font-size: 20px;">Извините, на данный момент расписание недоступно</p>
		<p style="font-size: 20px;">Но кто-то над этим уже работает</p>
	');
	
	//	2
	array_push($messages, '
		<p style="font-size: 20px;">Оп-па!</p>
		<p style="font-size: 20px;">Похоже, что Вы нас опередили..</p>
		<p style="font-size: 20px;">Вы нас опередили</p>
		<p style="font-size: 20px;">Но кто-то над этим уже работает</p>
	');
	
	//	3
	array_push($messages, '
		<p style="font-size: 20px;">Оп-па!</p>
		<p style="font-size: 20px;">Похоже, что вы оказались оперативнее составителей, и расписание для вас ещё не готово..</p>
		<p style="font-size: 20px;">Вероятно, вы захотите зайти позже? :)</p>
	');
	
	
	//	4
	array_push($messages, '
		<p style="font-size: 20px;">Ох и шустрый же ты, студент!</p>
		<p style="font-size: 20px;">Вот бы твою оперативность, да не только во время сессии :)</p>
		<p style="font-size: 20px;">Приходи чуть позже, у нас для тебя подарок</p>
	');
	
	//	5
	array_push($messages, '
		<p style="font-size: 20px;">Тссс..</p>
		<p style="font-size: 20px;">Скажу по-секрету: для вас готовится настоящий сюрприз!</p>
		<p style="font-size: 20px;">Заходи в гости в другой раз</i></p>
		<p style="font-size: 20px;">P.S. Только чур никому, лады?</p>
	');
	
	//	6
	array_push($messages, '
		<p style="font-size: 20px;">Хьюстон, у нас проблемы!</p>
		<p style="font-size: 20px;">Кажется, кто-то потерял ваше расписание..</p>
		<p style="font-size: 20px;">Но не волнуйтесь, мы его отыщем!</p>
	');
	
	//	7
	array_push($messages, '
		<p style="font-size: 20px;">Стоять!</p>
		<p style="font-size: 20px;">А теперь ти-и-ихо закрой расписание и зайди позже..</p>
		<p style="font-size: 20px;">И пусть это останется между нами</p>
	');
	
	//	8
	array_push($messages, '
		<p style="font-size: 20px;">Бугагашенька!</p>
		<p style="font-size: 20px;">А вообще да, мы потеряли ваше расписание..</p>
		<p style="font-size: 20px;">Но вы же нас простите, если мы исправимся?</p>
	');
	
	//	9
	array_push($messages, '
		<p style="font-size: 20px;">Ох ох..</p>
		<p style="font-size: 20px;">Расписания нет потому, что я его съел!</p>
		<p style="font-size: 20px;">В следующий раз заходи скорей, может и успеешь ;)</p>
	');
	
	//	10
	array_push($messages, '
		<p style="font-size: 20px;">Здесь могло бы быть ваше расписание</p>
	');
	
	//11
	array_push($messages, '
		<p style="font-size: 20px;">Расписания ищешь ты</p>
		<p style="font-size: 20px;">Но только недоступно сейчас оно</p>
	');
	
	//	12
	array_push($messages, '
		<p style="font-size: 20px;">Ох ты ж ёжик!</p>
		<p style="font-size: 20px;">Кто-то забыл составить расписание!</p>
	');
	
	//	13
	array_push($messages, '
		<p style="font-size: 20px;">На первый взгляд, расписания здесь нет..</p>
		<p style="font-size: 20px;">Но стоит зайти чуть позже!..</p>
	');
	
	//	14
	array_push($messages, '
		<p style="font-size: 20px;">Расписания пока ещё нет..</p>
		<p style="font-size: 20px;">Зато есть..</p>
	');
	
	//	15
	array_push($messages, '
		<i class="fa fa-stethoscope" style="font-size: 50px;"></i>
		<i class="fa fa-cog fa-spin" style="font-size: 50px;"></i>
	');
?>

<div class="error-not-found">

	<?php
		echo $messages[0];
	?>
	
</div>