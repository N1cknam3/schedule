<table class="schedule-table">
	
	<thead>
		<?php echo $this->Widgets->ScheduleTableRowGroups($groups); ?>
	</thead>
	
	<tbody>
		<?php
			$count_groups = count($groups);
			
			/*
				Вывод всех занятий по дням недели, содержащихся в $days
				Если в массиве занятий есть записи такого дня, то происходит их вывод,
				иначе - выходной
			*/
			foreach ($days as $day)
			{
				if (array_key_exists($day['Day']['id'], $table))
				{
					//	Вывод занятий этого дня
					echo $this->Widgets->ScheduleTableRowDay($day, $rings, $table[$day['Day']['id']], $groups);
					
				} else {
					//	Если нет записей этого дня, то вывод графы о том, что этот день - выходной
					echo $this->Widgets->ScheduleTableRowDayOff($day, $count_groups);
				} 
			}
		?>
	</tbody>
	
</table>

<div class="schedule-table-button">
	<?php
		//echo $this->Widgets->TableFooterAdd("javascript:saveTable()", '', "Сохранить", '<i class="fa fa-graduation-cap"></i>');
		echo $this->Widgets->TableFooterAdd("javascript:saveTable()", '', "Сохранить");
	?>
</div>