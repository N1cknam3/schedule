<?php
App::uses('AppModel', 'Model');
/**
 * Speciality Model
 *
 *
 *
 */
class Speciality extends AppModel {
	var $virtualFields = array(
    	'full_name' => 'CONCAT(Speciality.name, " (", Speciality.profile, ")")'
	);
	
	var $displayField = 'full_name';
}